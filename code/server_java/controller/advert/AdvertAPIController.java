package com.jzl.controller.api.advert;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.advert.ApplyHomeAdvertisingRequestTO;
import com.jzl.to.api.advert.ApplyHomeAdvertisingResponseTO;
import com.jzl.to.api.advert.GetAdvertCostListRequestTO;
import com.jzl.to.api.advert.GetAdvertCostListResponseTO;
import com.jzl.to.api.advert.GetAdvertBannerListRequestTO;
import com.jzl.to.api.advert.GetAdvertBannerListResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【广告】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "advertAPIController")
@RequestMapping("api/advert")
public class AdvertAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdvertAPIController.class);


	/**
	 * 申请首页推广
     * @param request
     * @param applyHomeAdvertisingRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "applyHomeAdvertising")
    @ResponseBody
    public Object applyHomeAdvertising(HttpServletRequest request, ApplyHomeAdvertisingRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            ApplyHomeAdvertisingResponseTO responseTO = new ApplyHomeAdvertisingResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("申请首页推广出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取广告推广类型列表
     * @param request
     * @param getAdvertCostListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getAdvertCostList")
    @ResponseBody
    public Object getAdvertCostList(HttpServletRequest request, GetAdvertCostListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetAdvertCostListResponseTO responseTO = new GetAdvertCostListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取广告推广类型列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得首页banner轮播图
     * @param request
     * @param getAdvertBannerListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getAdvertBannerList")
    @ResponseBody
    public Object getAdvertBannerList(HttpServletRequest request, GetAdvertBannerListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetAdvertBannerListResponseTO responseTO = new GetAdvertBannerListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得首页banner轮播图出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
