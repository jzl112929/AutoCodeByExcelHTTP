package com.jzl.controller.api.courseware;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.courseware.GetCoursewareListRequestTO;
import com.jzl.to.api.courseware.GetCoursewareListResponseTO;
import com.jzl.to.api.courseware.DownloadCoursewareRequestTO;
import com.jzl.to.api.courseware.DownloadCoursewareResponseTO;
import com.jzl.to.api.courseware.CreateCoursewareRequestTO;
import com.jzl.to.api.courseware.CreateCoursewareResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【课件】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "coursewareAPIController")
@RequestMapping("api/courseware")
public class CoursewareAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CoursewareAPIController.class);


	/**
	 * 获取课件列表
     * @param request
     * @param getCoursewareListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCoursewareList")
    @ResponseBody
    public Object getCoursewareList(HttpServletRequest request, GetCoursewareListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCoursewareListResponseTO responseTO = new GetCoursewareListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取课件列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 下载课件
     * @param request
     * @param downloadCoursewareRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "downloadCourseware")
    @ResponseBody
    public Object downloadCourseware(HttpServletRequest request, DownloadCoursewareRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            DownloadCoursewareResponseTO responseTO = new DownloadCoursewareResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("下载课件出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 创建课件
     * @param request
     * @param createCoursewareRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "createCourseware")
    @ResponseBody
    public Object createCourseware(HttpServletRequest request, CreateCoursewareRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CreateCoursewareResponseTO responseTO = new CreateCoursewareResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("创建课件出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
