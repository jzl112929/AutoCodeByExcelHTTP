package com.jzl.controller.api.common;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.common.GetSubjectListRequestTO;
import com.jzl.to.api.common.GetSubjectListResponseTO;
import com.jzl.to.api.common.GetGradeListRequestTO;
import com.jzl.to.api.common.GetGradeListResponseTO;
import com.jzl.to.api.common.GetRegionListRequestTO;
import com.jzl.to.api.common.GetRegionListResponseTO;
import com.jzl.to.api.common.GetRegionByLngLatRequestTO;
import com.jzl.to.api.common.GetRegionByLngLatResponseTO;
import com.jzl.to.api.common.GetRegionByPidRequestTO;
import com.jzl.to.api.common.GetRegionByPidResponseTO;
import com.jzl.to.api.common.SignAppRequestTO;
import com.jzl.to.api.common.SignAppResponseTO;
import com.jzl.to.api.common.GetVersionRequestTO;
import com.jzl.to.api.common.GetVersionResponseTO;
import com.jzl.to.api.common.GetSystemConfigRequestTO;
import com.jzl.to.api.common.GetSystemConfigResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【公共常用】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "commonAPIController")
@RequestMapping("api/common")
public class CommonAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommonAPIController.class);


	/**
	 * 获取科目
     * @param request
     * @param getSubjectListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getSubjectList")
    @ResponseBody
    public Object getSubjectList(HttpServletRequest request, GetSubjectListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetSubjectListResponseTO responseTO = new GetSubjectListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取科目出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取年级
     * @param request
     * @param getGradeListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getGradeList")
    @ResponseBody
    public Object getGradeList(HttpServletRequest request, GetGradeListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetGradeListResponseTO responseTO = new GetGradeListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取年级出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取城市
     * @param request
     * @param getRegionListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getRegionList")
    @ResponseBody
    public Object getRegionList(HttpServletRequest request, GetRegionListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetRegionListResponseTO responseTO = new GetRegionListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取城市出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 根据经纬度获得城市
     * @param request
     * @param getRegionByLngLatRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getRegionByLngLat")
    @ResponseBody
    public Object getRegionByLngLat(HttpServletRequest request, GetRegionByLngLatRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetRegionByLngLatResponseTO responseTO = new GetRegionByLngLatResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("根据经纬度获得城市出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 根据Pid获得地区信息
     * @param request
     * @param GetRegionByPidRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "GetRegionByPid")
    @ResponseBody
    public Object GetRegionByPid(HttpServletRequest request, GetRegionByPidRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetRegionByPidResponseTO responseTO = new GetRegionByPidResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("根据Pid获得地区信息出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 签到
     * @param request
     * @param signAppRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "signApp")
    @ResponseBody
    public Object signApp(HttpServletRequest request, SignAppRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            SignAppResponseTO responseTO = new SignAppResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("签到出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取版本信息
     * @param request
     * @param getVersionRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getVersion")
    @ResponseBody
    public Object getVersion(HttpServletRequest request, GetVersionRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetVersionResponseTO responseTO = new GetVersionResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取版本信息出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取系统相关参数
     * @param request
     * @param getSystemConfigRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getSystemConfig")
    @ResponseBody
    public Object getSystemConfig(HttpServletRequest request, GetSystemConfigRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetSystemConfigResponseTO responseTO = new GetSystemConfigResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取系统相关参数出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
