package com.jzl.controller.api.cas;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.cas.EnterLiveCourseRequestTO;
import com.jzl.to.api.cas.EnterLiveCourseResponseTO;
import com.jzl.to.api.cas.GetStudentInfoRequestTO;
import com.jzl.to.api.cas.GetStudentInfoResponseTO;
import com.jzl.to.api.cas.AddPreviewCourseRequestTO;
import com.jzl.to.api.cas.AddPreviewCourseResponseTO;
import com.jzl.to.api.cas.GetPreviewRecordRequestTO;
import com.jzl.to.api.cas.GetPreviewRecordResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【学生】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "studentAPIController")
@RequestMapping("api/student")
public class StudentAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(StudentAPIController.class);


	/**
	 * 进入直播课堂
     * @param request
     * @param enterLiveCourseRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "enterLiveCourse")
    @ResponseBody
    public Object enterLiveCourse(HttpServletRequest request, EnterLiveCourseRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            EnterLiveCourseResponseTO responseTO = new EnterLiveCourseResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("进入直播课堂出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得学生详情
     * @param request
     * @param getStudentInfoRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getStudentInfo")
    @ResponseBody
    public Object getStudentInfo(HttpServletRequest request, GetStudentInfoRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetStudentInfoResponseTO responseTO = new GetStudentInfoResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得学生详情出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 试听课程
     * @param request
     * @param addPreviewCourseRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "addPreviewCourse")
    @ResponseBody
    public Object addPreviewCourse(HttpServletRequest request, AddPreviewCourseRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            AddPreviewCourseResponseTO responseTO = new AddPreviewCourseResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("试听课程出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得试听历史
     * @param request
     * @param getPreviewRecordRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getPreviewRecord")
    @ResponseBody
    public Object getPreviewRecord(HttpServletRequest request, GetPreviewRecordRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetPreviewRecordResponseTO responseTO = new GetPreviewRecordResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得试听历史出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
