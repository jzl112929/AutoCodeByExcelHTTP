package com.jzl.controller.api.cas;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.cas.LoginRequestTO;
import com.jzl.to.api.cas.LoginResponseTO;
import com.jzl.to.api.cas.StudentRegisterRequestTO;
import com.jzl.to.api.cas.StudentRegisterResponseTO;
import com.jzl.to.api.cas.TeacherRegisterRequestTO;
import com.jzl.to.api.cas.TeacherRegisterResponseTO;
import com.jzl.to.api.cas.ImproveCasStudentInfoRequestTO;
import com.jzl.to.api.cas.ImproveCasStudentInfoResponseTO;
import com.jzl.to.api.cas.ImproveCasTeacherInfoRequestTO;
import com.jzl.to.api.cas.ImproveCasTeacherInfoResponseTO;
import com.jzl.to.api.cas.GetVerificationCodeByfindPasswordRequestTO;
import com.jzl.to.api.cas.GetVerificationCodeByfindPasswordResponseTO;
import com.jzl.to.api.cas.CheckVerificationCodeRequestTO;
import com.jzl.to.api.cas.CheckVerificationCodeResponseTO;
import com.jzl.to.api.cas.UpdatePasswordRequestTO;
import com.jzl.to.api.cas.UpdatePasswordResponseTO;
import com.jzl.to.api.cas.UpdatePasswordLoginRequestTO;
import com.jzl.to.api.cas.UpdatePasswordLoginResponseTO;
import com.jzl.to.api.cas.LogoutRequestTO;
import com.jzl.to.api.cas.LogoutResponseTO;
import com.jzl.to.api.cas.GetVerificationCodeForRegisterRequestTO;
import com.jzl.to.api.cas.GetVerificationCodeForRegisterResponseTO;
import com.jzl.to.api.cas.GetVerificationCodeForPayPasswordRequestTO;
import com.jzl.to.api.cas.GetVerificationCodeForPayPasswordResponseTO;
import com.jzl.to.api.cas.CheckVerificationCodeForPayPasswordRequestTO;
import com.jzl.to.api.cas.CheckVerificationCodeForPayPasswordResponseTO;
import com.jzl.to.api.cas.UpdatePayPasswordRequestTO;
import com.jzl.to.api.cas.UpdatePayPasswordResponseTO;
import com.jzl.to.api.cas.CheckOldPayPasswordRequestTO;
import com.jzl.to.api.cas.CheckOldPayPasswordResponseTO;
import com.jzl.to.api.cas.UpdateStudentCasUserRequestTO;
import com.jzl.to.api.cas.UpdateStudentCasUserResponseTO;
import com.jzl.to.api.cas.UpdateTeacherCasUserRequestTO;
import com.jzl.to.api.cas.UpdateTeacherCasUserResponseTO;
import com.jzl.to.api.cas.UpdateAvatarRequestTO;
import com.jzl.to.api.cas.UpdateAvatarResponseTO;
import com.jzl.to.api.cas.GetCasStudentUserRequestTO;
import com.jzl.to.api.cas.GetCasStudentUserResponseTO;
import com.jzl.to.api.cas.GetCasTeacherUserRequestTO;
import com.jzl.to.api.cas.GetCasTeacherUserResponseTO;
import com.jzl.to.api.cas.UploadPreviewCourseRequestTO;
import com.jzl.to.api.cas.UploadPreviewCourseResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【用户】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "casUserAPIController")
@RequestMapping("api/casUser")
public class CasUserAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CasUserAPIController.class);


	/**
	 * 登录
     * @param request
     * @param loginRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "login")
    @ResponseBody
    public Object login(HttpServletRequest request, LoginRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            LoginResponseTO responseTO = new LoginResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("登录出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 学生端注册
     * @param request
     * @param studentRegisterRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "studentRegister")
    @ResponseBody
    public Object studentRegister(HttpServletRequest request, StudentRegisterRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            StudentRegisterResponseTO responseTO = new StudentRegisterResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("学生端注册出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 老师端注册
     * @param request
     * @param teacherRegisterRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "teacherRegister")
    @ResponseBody
    public Object teacherRegister(HttpServletRequest request, TeacherRegisterRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            TeacherRegisterResponseTO responseTO = new TeacherRegisterResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("老师端注册出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 完善学生信息
     * @param request
     * @param improveCasStudentInfoRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "improveCasStudentInfo")
    @ResponseBody
    public Object improveCasStudentInfo(HttpServletRequest request, ImproveCasStudentInfoRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            ImproveCasStudentInfoResponseTO responseTO = new ImproveCasStudentInfoResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("完善学生信息出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 完善老师信息
     * @param request
     * @param improveCasTeacherInfoRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "improveCasTeacherInfo")
    @ResponseBody
    public Object improveCasTeacherInfo(HttpServletRequest request, ImproveCasTeacherInfoRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            ImproveCasTeacherInfoResponseTO responseTO = new ImproveCasTeacherInfoResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("完善老师信息出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 找回密码获取验证码
     * @param request
     * @param getVerificationCodeByfindPasswordRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getVerificationCodeByfindPassword")
    @ResponseBody
    public Object getVerificationCodeByfindPassword(HttpServletRequest request, GetVerificationCodeByfindPasswordRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetVerificationCodeByfindPasswordResponseTO responseTO = new GetVerificationCodeByfindPasswordResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("找回密码获取验证码出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 忘记密码校验验证码
     * @param request
     * @param checkVerificationCodeRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "checkVerificationCode")
    @ResponseBody
    public Object checkVerificationCode(HttpServletRequest request, CheckVerificationCodeRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CheckVerificationCodeResponseTO responseTO = new CheckVerificationCodeResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("忘记密码校验验证码出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 修改密码
     * @param request
     * @param updatePasswordRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "updatePassword")
    @ResponseBody
    public Object updatePassword(HttpServletRequest request, UpdatePasswordRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UpdatePasswordResponseTO responseTO = new UpdatePasswordResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("修改密码出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 登陆后修改密码
     * @param request
     * @param updatePasswordLoginRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "updatePasswordLogin")
    @ResponseBody
    public Object updatePasswordLogin(HttpServletRequest request, UpdatePasswordLoginRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UpdatePasswordLoginResponseTO responseTO = new UpdatePasswordLoginResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("登陆后修改密码出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 注销登陆
     * @param request
     * @param logoutRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "logout")
    @ResponseBody
    public Object logout(HttpServletRequest request, LogoutRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            LogoutResponseTO responseTO = new LogoutResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("注销登陆出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 注册获取验证码
     * @param request
     * @param getVerificationCodeForRegisterRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getVerificationCodeForRegister")
    @ResponseBody
    public Object getVerificationCodeForRegister(HttpServletRequest request, GetVerificationCodeForRegisterRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetVerificationCodeForRegisterResponseTO responseTO = new GetVerificationCodeForRegisterResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("注册获取验证码出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 支付密码获取验证码
     * @param request
     * @param getVerificationCodeForPayPasswordRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getVerificationCodeForPayPassword")
    @ResponseBody
    public Object getVerificationCodeForPayPassword(HttpServletRequest request, GetVerificationCodeForPayPasswordRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetVerificationCodeForPayPasswordResponseTO responseTO = new GetVerificationCodeForPayPasswordResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("支付密码获取验证码出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 校验支付密码验证码
     * @param request
     * @param checkVerificationCodeForPayPasswordRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "checkVerificationCodeForPayPassword")
    @ResponseBody
    public Object checkVerificationCodeForPayPassword(HttpServletRequest request, CheckVerificationCodeForPayPasswordRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CheckVerificationCodeForPayPasswordResponseTO responseTO = new CheckVerificationCodeForPayPasswordResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("校验支付密码验证码出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 修改支付密码
     * @param request
     * @param updatePayPasswordRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "updatePayPassword")
    @ResponseBody
    public Object updatePayPassword(HttpServletRequest request, UpdatePayPasswordRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UpdatePayPasswordResponseTO responseTO = new UpdatePayPasswordResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("修改支付密码出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 校验旧支付密码
     * @param request
     * @param checkOldPayPasswordRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "checkOldPayPassword")
    @ResponseBody
    public Object checkOldPayPassword(HttpServletRequest request, CheckOldPayPasswordRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CheckOldPayPasswordResponseTO responseTO = new CheckOldPayPasswordResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("校验旧支付密码出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 学生用户修改基本信息
     * @param request
     * @param updateStudentCasUserRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "updateStudentCasUser")
    @ResponseBody
    public Object updateStudentCasUser(HttpServletRequest request, UpdateStudentCasUserRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UpdateStudentCasUserResponseTO responseTO = new UpdateStudentCasUserResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("学生用户修改基本信息出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 老师用户修改基本信息
     * @param request
     * @param updateTeacherCasUserRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "updateTeacherCasUser")
    @ResponseBody
    public Object updateTeacherCasUser(HttpServletRequest request, UpdateTeacherCasUserRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UpdateTeacherCasUserResponseTO responseTO = new UpdateTeacherCasUserResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("老师用户修改基本信息出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 修改用户头像
     * @param request
     * @param updateAvatarRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "updateAvatar")
    @ResponseBody
    public Object updateAvatar(HttpServletRequest request, UpdateAvatarRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UpdateAvatarResponseTO responseTO = new UpdateAvatarResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("修改用户头像出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取学生用户信息
     * @param request
     * @param getCasStudentUserRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCasStudentUser")
    @ResponseBody
    public Object getCasStudentUser(HttpServletRequest request, GetCasStudentUserRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCasStudentUserResponseTO responseTO = new GetCasStudentUserResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取学生用户信息出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取老师用户信息
     * @param request
     * @param getCasTeacherUserRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCasTeacherUser")
    @ResponseBody
    public Object getCasTeacherUser(HttpServletRequest request, GetCasTeacherUserRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCasTeacherUserResponseTO responseTO = new GetCasTeacherUserResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取老师用户信息出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 上传试听课
     * @param request
     * @param uploadPreviewCourseRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "uploadPreviewCourse")
    @ResponseBody
    public Object uploadPreviewCourse(HttpServletRequest request, UploadPreviewCourseRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UploadPreviewCourseResponseTO responseTO = new UploadPreviewCourseResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("上传试听课出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
