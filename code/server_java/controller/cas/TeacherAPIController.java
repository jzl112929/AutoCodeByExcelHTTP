package com.jzl.controller.api.cas;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.cas.GetTeachersListRequestTO;
import com.jzl.to.api.cas.GetTeachersListResponseTO;
import com.jzl.to.api.cas.GetTeacherDetailRequestTO;
import com.jzl.to.api.cas.GetTeacherDetailResponseTO;
import com.jzl.to.api.cas.CreateServiceByTypeRequestTO;
import com.jzl.to.api.cas.CreateServiceByTypeResponseTO;
import com.jzl.to.api.cas.GetCourseListByTeacherIdRequestTO;
import com.jzl.to.api.cas.GetCourseListByTeacherIdResponseTO;
import com.jzl.to.api.cas.GetTutorialServiceListRequestTO;
import com.jzl.to.api.cas.GetTutorialServiceListResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【老师】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "teacherAPIController")
@RequestMapping("api/teacher")
public class TeacherAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TeacherAPIController.class);


	/**
	 * 获得老师列表
     * @param request
     * @param getTeachersListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getTeachersList")
    @ResponseBody
    public Object getTeachersList(HttpServletRequest request, GetTeachersListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetTeachersListResponseTO responseTO = new GetTeachersListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得老师列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得老师详情
     * @param request
     * @param getTeacherDetailRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getTeacherDetail")
    @ResponseBody
    public Object getTeacherDetail(HttpServletRequest request, GetTeacherDetailRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetTeacherDetailResponseTO responseTO = new GetTeacherDetailResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得老师详情出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 上架一对一/群组服务
     * @param request
     * @param createServiceByTypeRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "createServiceByType")
    @ResponseBody
    public Object createServiceByType(HttpServletRequest request, CreateServiceByTypeRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CreateServiceByTypeResponseTO responseTO = new CreateServiceByTypeResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("上架一对一/群组服务出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 根据老师Id获取专题课程列表
     * @param request
     * @param getCourseListByTeacherIdRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCourseListByTeacherId")
    @ResponseBody
    public Object getCourseListByTeacherId(HttpServletRequest request, GetCourseListByTeacherIdRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCourseListByTeacherIdResponseTO responseTO = new GetCourseListByTeacherIdResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("根据老师Id获取专题课程列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取老师辅导服务列表
     * @param request
     * @param getTutorialServiceListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getTutorialServiceList")
    @ResponseBody
    public Object getTutorialServiceList(HttpServletRequest request, GetTutorialServiceListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetTutorialServiceListResponseTO responseTO = new GetTutorialServiceListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取老师辅导服务列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
