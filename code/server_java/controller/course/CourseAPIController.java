package com.jzl.controller.api.course;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.course.UploadPicForCourseRequestTO;
import com.jzl.to.api.course.UploadPicForCourseResponseTO;
import com.jzl.to.api.course.CreateCourseScheduleRequestTO;
import com.jzl.to.api.course.CreateCourseScheduleResponseTO;
import com.jzl.to.api.course.CreateSpecialSubjectCourseRequestTO;
import com.jzl.to.api.course.CreateSpecialSubjectCourseResponseTO;
import com.jzl.to.api.course.GetCourseDetailRequestTO;
import com.jzl.to.api.course.GetCourseDetailResponseTO;
import com.jzl.to.api.course.GetCourseDetailForStudentRequestTO;
import com.jzl.to.api.course.GetCourseDetailForStudentResponseTO;
import com.jzl.to.api.course.GetAllCoursesListByTeacherIdRequestTO;
import com.jzl.to.api.course.GetAllCoursesListByTeacherIdResponseTO;
import com.jzl.to.api.course.GetAllCoursesListRequestTO;
import com.jzl.to.api.course.GetAllCoursesListResponseTO;
import com.jzl.to.api.course.BuyAndPayForCourseRequestTO;
import com.jzl.to.api.course.BuyAndPayForCourseResponseTO;
import com.jzl.to.api.course.BuyAndPayForTeacherRequestTO;
import com.jzl.to.api.course.BuyAndPayForTeacherResponseTO;
import com.jzl.to.api.course.CreateOneToOneCourseRequestTO;
import com.jzl.to.api.course.CreateOneToOneCourseResponseTO;
import com.jzl.to.api.course.GetOneToOneStudentTutorialRequestTO;
import com.jzl.to.api.course.GetOneToOneStudentTutorialResponseTO;
import com.jzl.to.api.course.GetTutorialRecordListRequestTO;
import com.jzl.to.api.course.GetTutorialRecordListResponseTO;
import com.jzl.to.api.course.GetTutorialRecordDetailRequestTO;
import com.jzl.to.api.course.GetTutorialRecordDetailResponseTO;
import com.jzl.to.api.course.CreateGroupCourseRequestTO;
import com.jzl.to.api.course.CreateGroupCourseResponseTO;
import com.jzl.to.api.course.GetRunningCoursesListRequestTO;
import com.jzl.to.api.course.GetRunningCoursesListResponseTO;
import com.jzl.to.api.course.GetGroupRecordCoursesDetailRequestTO;
import com.jzl.to.api.course.GetGroupRecordCoursesDetailResponseTO;
import com.jzl.to.api.course.GetStudentListBuyTutorialIdRequestTO;
import com.jzl.to.api.course.GetStudentListBuyTutorialIdResponseTO;
import com.jzl.to.api.course.GetMyCourseByStudentIdRequestTO;
import com.jzl.to.api.course.GetMyCourseByStudentIdResponseTO;
import com.jzl.to.api.course.GetCourseAlreadyFinishedByStudentIdRequestTO;
import com.jzl.to.api.course.GetCourseAlreadyFinishedByStudentIdResponseTO;
import com.jzl.to.api.course.DownloadCourseRequestTO;
import com.jzl.to.api.course.DownloadCourseResponseTO;
import com.jzl.to.api.course.SearchCoursesRequestTO;
import com.jzl.to.api.course.SearchCoursesResponseTO;
import com.jzl.to.api.course.CheckIsPassVertifyRequestTO;
import com.jzl.to.api.course.CheckIsPassVertifyResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【课程】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "courseAPIController")
@RequestMapping("api/course")
public class CourseAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CourseAPIController.class);


	/**
	 * 上传课程标识图片
     * @param request
     * @param uploadPicForCourseRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "uploadPicForCourse")
    @ResponseBody
    public Object uploadPicForCourse(HttpServletRequest request, UploadPicForCourseRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UploadPicForCourseResponseTO responseTO = new UploadPicForCourseResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("上传课程标识图片出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 创建专题计划
     * @param request
     * @param createCourseScheduleRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "createCourseSchedule")
    @ResponseBody
    public Object createCourseSchedule(HttpServletRequest request, CreateCourseScheduleRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CreateCourseScheduleResponseTO responseTO = new CreateCourseScheduleResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("创建专题计划出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 创建专题课程
     * @param request
     * @param createSpecialSubjectCourseRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "createSpecialSubjectCourse")
    @ResponseBody
    public Object createSpecialSubjectCourse(HttpServletRequest request, CreateSpecialSubjectCourseRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CreateSpecialSubjectCourseResponseTO responseTO = new CreateSpecialSubjectCourseResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("创建专题课程出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得专题课程详情（老师端)
     * @param request
     * @param getCourseDetailRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCourseDetail")
    @ResponseBody
    public Object getCourseDetail(HttpServletRequest request, GetCourseDetailRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCourseDetailResponseTO responseTO = new GetCourseDetailResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得专题课程详情（老师端)出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得专题课程详情（学生端）
     * @param request
     * @param getCourseDetailForStudentRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCourseDetailForStudent")
    @ResponseBody
    public Object getCourseDetailForStudent(HttpServletRequest request, GetCourseDetailForStudentRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCourseDetailForStudentResponseTO responseTO = new GetCourseDetailForStudentResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得专题课程详情（学生端）出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得所有专题课程（教师端）
     * @param request
     * @param getAllCoursesListByTeacherIdRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getAllCoursesListByTeacherId")
    @ResponseBody
    public Object getAllCoursesListByTeacherId(HttpServletRequest request, GetAllCoursesListByTeacherIdRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetAllCoursesListByTeacherIdResponseTO responseTO = new GetAllCoursesListByTeacherIdResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得所有专题课程（教师端）出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得所有专题课程（学生端）
     * @param request
     * @param getAllCoursesListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getAllCoursesList")
    @ResponseBody
    public Object getAllCoursesList(HttpServletRequest request, GetAllCoursesListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetAllCoursesListResponseTO responseTO = new GetAllCoursesListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得所有专题课程（学生端）出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 专题课程支付购买
     * @param request
     * @param buyAndPayForCourseRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "buyAndPayForCourse")
    @ResponseBody
    public Object buyAndPayForCourse(HttpServletRequest request, BuyAndPayForCourseRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            BuyAndPayForCourseResponseTO responseTO = new BuyAndPayForCourseResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("专题课程支付购买出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 购买一对一/群组老师
     * @param request
     * @param buyAndPayForTeacherRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "buyAndPayForTeacher")
    @ResponseBody
    public Object buyAndPayForTeacher(HttpServletRequest request, BuyAndPayForTeacherRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            BuyAndPayForTeacherResponseTO responseTO = new BuyAndPayForTeacherResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("购买一对一/群组老师出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 创建一对一课程
     * @param request
     * @param createOneToOneCourseRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "createOneToOneCourse")
    @ResponseBody
    public Object createOneToOneCourse(HttpServletRequest request, CreateOneToOneCourseRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CreateOneToOneCourseResponseTO responseTO = new CreateOneToOneCourseResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("创建一对一课程出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取已购买一对一学生列表
     * @param request
     * @param getOneToOneStudentTutorialRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getOneToOneStudentTutorial")
    @ResponseBody
    public Object getOneToOneStudentTutorial(HttpServletRequest request, GetOneToOneStudentTutorialRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetOneToOneStudentTutorialResponseTO responseTO = new GetOneToOneStudentTutorialResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取已购买一对一学生列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 根据老师Id及类型获取辅导课开课记录列表
     * @param request
     * @param getTutorialRecordListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getTutorialRecordList")
    @ResponseBody
    public Object getTutorialRecordList(HttpServletRequest request, GetTutorialRecordListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetTutorialRecordListResponseTO responseTO = new GetTutorialRecordListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("根据老师Id及类型获取辅导课开课记录列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 根据辅导课Id及类型获取辅导课课程详情
     * @param request
     * @param getTutorialRecordDetailRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getTutorialRecordDetail")
    @ResponseBody
    public Object getTutorialRecordDetail(HttpServletRequest request, GetTutorialRecordDetailRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetTutorialRecordDetailResponseTO responseTO = new GetTutorialRecordDetailResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("根据辅导课Id及类型获取辅导课课程详情出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 创建群组课程
     * @param request
     * @param createGroupCourseRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "createGroupCourse")
    @ResponseBody
    public Object createGroupCourse(HttpServletRequest request, CreateGroupCourseRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CreateGroupCourseResponseTO responseTO = new CreateGroupCourseResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("创建群组课程出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取在线课程(正在直播的群组课)列表
     * @param request
     * @param getRunningCoursesListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getRunningCoursesList")
    @ResponseBody
    public Object getRunningCoursesList(HttpServletRequest request, GetRunningCoursesListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetRunningCoursesListResponseTO responseTO = new GetRunningCoursesListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取在线课程(正在直播的群组课)列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取在线课程详情
     * @param request
     * @param getGroupRecordCoursesDetailRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getGroupRecordCoursesDetail")
    @ResponseBody
    public Object getGroupRecordCoursesDetail(HttpServletRequest request, GetGroupRecordCoursesDetailRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetGroupRecordCoursesDetailResponseTO responseTO = new GetGroupRecordCoursesDetailResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取在线课程详情出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取已购买群组该群组服务的学生列表
     * @param request
     * @param getStudentListBuyTutorialIdRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getStudentListBuyTutorialId")
    @ResponseBody
    public Object getStudentListBuyTutorialId(HttpServletRequest request, GetStudentListBuyTutorialIdRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetStudentListBuyTutorialIdResponseTO responseTO = new GetStudentListBuyTutorialIdResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取已购买群组该群组服务的学生列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取我的课程
     * @param request
     * @param getMyCourseByStudentIdRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getMyCourseByStudentId")
    @ResponseBody
    public Object getMyCourseByStudentId(HttpServletRequest request, GetMyCourseByStudentIdRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetMyCourseByStudentIdResponseTO responseTO = new GetMyCourseByStudentIdResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取我的课程出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取我已上过的课程
     * @param request
     * @param getCourseAlreadyFinishedByStudentIdRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCourseAlreadyFinishedByStudentId")
    @ResponseBody
    public Object getCourseAlreadyFinishedByStudentId(HttpServletRequest request, GetCourseAlreadyFinishedByStudentIdRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCourseAlreadyFinishedByStudentIdResponseTO responseTO = new GetCourseAlreadyFinishedByStudentIdResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取我已上过的课程出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 下载课程
     * @param request
     * @param downloadCourseRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "downloadCourse")
    @ResponseBody
    public Object downloadCourse(HttpServletRequest request, DownloadCourseRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            DownloadCourseResponseTO responseTO = new DownloadCourseResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("下载课程出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 课程搜索
     * @param request
     * @param searchCoursesRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "searchCourses")
    @ResponseBody
    public Object searchCourses(HttpServletRequest request, SearchCoursesRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            SearchCoursesResponseTO responseTO = new SearchCoursesResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("课程搜索出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 是否通过验证
     * @param request
     * @param checkIsPassVertifyRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "checkIsPassVertify")
    @ResponseBody
    public Object checkIsPassVertify(HttpServletRequest request, CheckIsPassVertifyRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CheckIsPassVertifyResponseTO responseTO = new CheckIsPassVertifyResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("是否通过验证出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
