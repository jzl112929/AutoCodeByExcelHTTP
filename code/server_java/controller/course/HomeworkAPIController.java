package com.jzl.controller.api.course;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.course.AddHomeworkRequestTO;
import com.jzl.to.api.course.AddHomeworkResponseTO;
import com.jzl.to.api.course.GetHomeworkDetailRequestTO;
import com.jzl.to.api.course.GetHomeworkDetailResponseTO;
import com.jzl.to.api.course.GetHomeworkListRequestTO;
import com.jzl.to.api.course.GetHomeworkListResponseTO;
import com.jzl.to.api.course.GetHomeworkDetailByStudentIdRequestTO;
import com.jzl.to.api.course.GetHomeworkDetailByStudentIdResponseTO;
import com.jzl.to.api.course.RemindDoneHomeworkRequestTO;
import com.jzl.to.api.course.RemindDoneHomeworkResponseTO;
import com.jzl.to.api.course.UploadDoneImageRequestTO;
import com.jzl.to.api.course.UploadDoneImageResponseTO;
import com.jzl.to.api.course.UploadFileRequestTO;
import com.jzl.to.api.course.UploadFileResponseTO;
import com.jzl.to.api.course.FinishHomeWorkRequestTO;
import com.jzl.to.api.course.FinishHomeWorkResponseTO;
import com.jzl.to.api.course.GetHomeworkListByStudentIdRequestTO;
import com.jzl.to.api.course.GetHomeworkListByStudentIdResponseTO;
import com.jzl.to.api.course.GetFinishedWorkDetailRequestTO;
import com.jzl.to.api.course.GetFinishedWorkDetailResponseTO;
import com.jzl.to.api.course.ReviewHomeworkRequestTO;
import com.jzl.to.api.course.ReviewHomeworkResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【作业】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "homeworkAPIController")
@RequestMapping("api/homework")
public class HomeworkAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(HomeworkAPIController.class);


	/**
	 * 布置作业
     * @param request
     * @param addHomeworkRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "addHomework")
    @ResponseBody
    public Object addHomework(HttpServletRequest request, AddHomeworkRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            AddHomeworkResponseTO responseTO = new AddHomeworkResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("布置作业出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得作业详情（老师端）
     * @param request
     * @param getHomeworkDetailRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getHomeworkDetail")
    @ResponseBody
    public Object getHomeworkDetail(HttpServletRequest request, GetHomeworkDetailRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetHomeworkDetailResponseTO responseTO = new GetHomeworkDetailResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得作业详情（老师端）出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得作业列表（老师端）
     * @param request
     * @param getHomeworkListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getHomeworkList")
    @ResponseBody
    public Object getHomeworkList(HttpServletRequest request, GetHomeworkListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetHomeworkListResponseTO responseTO = new GetHomeworkListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得作业列表（老师端）出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取学生作业详情（学生端）
     * @param request
     * @param getHomeworkDetailByStudentIdRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getHomeworkDetailByStudentId")
    @ResponseBody
    public Object getHomeworkDetailByStudentId(HttpServletRequest request, GetHomeworkDetailByStudentIdRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetHomeworkDetailByStudentIdResponseTO responseTO = new GetHomeworkDetailByStudentIdResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取学生作业详情（学生端）出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 提醒完成作业
     * @param request
     * @param remindDoneHomeworkRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "remindDoneHomework")
    @ResponseBody
    public Object remindDoneHomework(HttpServletRequest request, RemindDoneHomeworkRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            RemindDoneHomeworkResponseTO responseTO = new RemindDoneHomeworkResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("提醒完成作业出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 图片批阅上传
     * @param request
     * @param uploadDoneImageRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "uploadDoneImage")
    @ResponseBody
    public Object uploadDoneImage(HttpServletRequest request, UploadDoneImageRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UploadDoneImageResponseTO responseTO = new UploadDoneImageResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("图片批阅上传出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 上传文件
     * @param request
     * @param uploadFileRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "uploadFile")
    @ResponseBody
    public Object uploadFile(HttpServletRequest request, UploadFileRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UploadFileResponseTO responseTO = new UploadFileResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("上传文件出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 完成作业
     * @param request
     * @param finishHomeWorkRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "finishHomeWork")
    @ResponseBody
    public Object finishHomeWork(HttpServletRequest request, FinishHomeWorkRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            FinishHomeWorkResponseTO responseTO = new FinishHomeWorkResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("完成作业出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 根据学生Id获得作业列表
     * @param request
     * @param getHomeworkListByStudentIdRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getHomeworkListByStudentId")
    @ResponseBody
    public Object getHomeworkListByStudentId(HttpServletRequest request, GetHomeworkListByStudentIdRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetHomeworkListByStudentIdResponseTO responseTO = new GetHomeworkListByStudentIdResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("根据学生Id获得作业列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取已完成的作业详情
     * @param request
     * @param getFinishedWorkDetailRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getFinishedWorkDetail")
    @ResponseBody
    public Object getFinishedWorkDetail(HttpServletRequest request, GetFinishedWorkDetailRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetFinishedWorkDetailResponseTO responseTO = new GetFinishedWorkDetailResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取已完成的作业详情出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 点评作业
     * @param request
     * @param reviewHomeworkRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "reviewHomework")
    @ResponseBody
    public Object reviewHomework(HttpServletRequest request, ReviewHomeworkRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            ReviewHomeworkResponseTO responseTO = new ReviewHomeworkResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("点评作业出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
