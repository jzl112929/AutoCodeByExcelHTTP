package com.jzl.controller.api.live;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.live.RegisterRequestTO;
import com.jzl.to.api.live.RegisterResponseTO;
import com.jzl.to.api.live.UnregisterRequestTO;
import com.jzl.to.api.live.UnregisterResponseTO;
import com.jzl.to.api.live.ListRequestTO;
import com.jzl.to.api.live.ListResponseTO;
import com.jzl.to.api.live.EnterLiveCourseRequestTO;
import com.jzl.to.api.live.EnterLiveCourseResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【直播】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "liveAPIController")
@RequestMapping("api/live")
public class LiveAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(LiveAPIController.class);


	/**
	 * 注册直播间
     * @param request
     * @param registerRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "register")
    @ResponseBody
    public Object register(HttpServletRequest request, RegisterRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            RegisterResponseTO responseTO = new RegisterResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("注册直播间出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 注销直播间
     * @param request
     * @param unregisterRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "unregister")
    @ResponseBody
    public Object unregister(HttpServletRequest request, UnregisterRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UnregisterResponseTO responseTO = new UnregisterResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("注销直播间出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 查询直播间列表
     * @param request
     * @param listRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "list")
    @ResponseBody
    public Object list(HttpServletRequest request, ListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            ListResponseTO responseTO = new ListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("查询直播间列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 进入直播课堂
     * @param request
     * @param enterLiveCourseRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "enterLiveCourse")
    @ResponseBody
    public Object enterLiveCourse(HttpServletRequest request, EnterLiveCourseRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            EnterLiveCourseResponseTO responseTO = new EnterLiveCourseResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("进入直播课堂出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
