package com.jzl.controller.api.usercenter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.usercenter.CreateGroupRequestTO;
import com.jzl.to.api.usercenter.CreateGroupResponseTO;
import com.jzl.to.api.usercenter.GetGroupDetailRequestTO;
import com.jzl.to.api.usercenter.GetGroupDetailResponseTO;
import com.jzl.to.api.usercenter.EditGroupDetailRequestTO;
import com.jzl.to.api.usercenter.EditGroupDetailResponseTO;
import com.jzl.to.api.usercenter.GetGroupMemberListRequestTO;
import com.jzl.to.api.usercenter.GetGroupMemberListResponseTO;
import com.jzl.to.api.usercenter.GetContactListRequestTO;
import com.jzl.to.api.usercenter.GetContactListResponseTO;
import com.jzl.to.api.usercenter.AddMemberToGroupRequestTO;
import com.jzl.to.api.usercenter.AddMemberToGroupResponseTO;
import com.jzl.to.api.usercenter.GetCasSiteMessageCountRequestTO;
import com.jzl.to.api.usercenter.GetCasSiteMessageCountResponseTO;
import com.jzl.to.api.usercenter.GetCasSiteMessageListRequestTO;
import com.jzl.to.api.usercenter.GetCasSiteMessageListResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【通讯簿】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "contactAPIController")
@RequestMapping("api/contact")
public class ContactAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ContactAPIController.class);


	/**
	 * 创建群组
     * @param request
     * @param createGroupRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "createGroup")
    @ResponseBody
    public Object createGroup(HttpServletRequest request, CreateGroupRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CreateGroupResponseTO responseTO = new CreateGroupResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("创建群组出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得群组详情
     * @param request
     * @param getGroupDetailRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getGroupDetail")
    @ResponseBody
    public Object getGroupDetail(HttpServletRequest request, GetGroupDetailRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetGroupDetailResponseTO responseTO = new GetGroupDetailResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得群组详情出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 编辑群组
     * @param request
     * @param editGroupDetailRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "editGroupDetail")
    @ResponseBody
    public Object editGroupDetail(HttpServletRequest request, EditGroupDetailRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            EditGroupDetailResponseTO responseTO = new EditGroupDetailResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("编辑群组出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得群组成员列表
     * @param request
     * @param getGroupMemberListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getGroupMemberList")
    @ResponseBody
    public Object getGroupMemberList(HttpServletRequest request, GetGroupMemberListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetGroupMemberListResponseTO responseTO = new GetGroupMemberListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得群组成员列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得联系人列表
     * @param request
     * @param getContactListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getContactList")
    @ResponseBody
    public Object getContactList(HttpServletRequest request, GetContactListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetContactListResponseTO responseTO = new GetContactListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得联系人列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 加入群组
     * @param request
     * @param addMemberToGroupRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "addMemberToGroup")
    @ResponseBody
    public Object addMemberToGroup(HttpServletRequest request, AddMemberToGroupRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            AddMemberToGroupResponseTO responseTO = new AddMemberToGroupResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("加入群组出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得系统消息统计
     * @param request
     * @param getCasSiteMessageCountRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCasSiteMessageCount")
    @ResponseBody
    public Object getCasSiteMessageCount(HttpServletRequest request, GetCasSiteMessageCountRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCasSiteMessageCountResponseTO responseTO = new GetCasSiteMessageCountResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得系统消息统计出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得系统消息列表
     * @param request
     * @param getCasSiteMessageListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCasSiteMessageList")
    @ResponseBody
    public Object getCasSiteMessageList(HttpServletRequest request, GetCasSiteMessageListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCasSiteMessageListResponseTO responseTO = new GetCasSiteMessageListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得系统消息列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
