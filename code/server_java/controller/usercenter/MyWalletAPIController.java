package com.jzl.controller.api.usercenter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.usercenter.GetMyCountInfoRequestTO;
import com.jzl.to.api.usercenter.GetMyCountInfoResponseTO;
import com.jzl.to.api.usercenter.GetBalanceOfPayRecordRequestTO;
import com.jzl.to.api.usercenter.GetBalanceOfPayRecordResponseTO;
import com.jzl.to.api.usercenter.GetCashFromMyCountRequestTO;
import com.jzl.to.api.usercenter.GetCashFromMyCountResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【我的钱包】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "myWalletAPIController")
@RequestMapping("api/myWallet")
public class MyWalletAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyWalletAPIController.class);


	/**
	 * 获得我的可提现金额
     * @param request
     * @param getMyCountInfoRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getMyCountInfo")
    @ResponseBody
    public Object getMyCountInfo(HttpServletRequest request, GetMyCountInfoRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetMyCountInfoResponseTO responseTO = new GetMyCountInfoResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得我的可提现金额出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得收支明细记录
     * @param request
     * @param getBalanceOfPayRecordRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getBalanceOfPayRecord")
    @ResponseBody
    public Object getBalanceOfPayRecord(HttpServletRequest request, GetBalanceOfPayRecordRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetBalanceOfPayRecordResponseTO responseTO = new GetBalanceOfPayRecordResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得收支明细记录出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 提现
     * @param request
     * @param getCashFromMyCountRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCashFromMyCount")
    @ResponseBody
    public Object getCashFromMyCount(HttpServletRequest request, GetCashFromMyCountRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCashFromMyCountResponseTO responseTO = new GetCashFromMyCountResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("提现出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
