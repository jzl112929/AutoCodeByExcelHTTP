package com.jzl.controller.api.usercenter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.usercenter.UploadCertificateRequestTO;
import com.jzl.to.api.usercenter.UploadCertificateResponseTO;
import com.jzl.to.api.usercenter.DeleteCertificateRequestTO;
import com.jzl.to.api.usercenter.DeleteCertificateResponseTO;
import com.jzl.to.api.usercenter.GetCertificateListRequestTO;
import com.jzl.to.api.usercenter.GetCertificateListResponseTO;
import com.jzl.to.api.usercenter.CheckCertificateIsPassRequestTO;
import com.jzl.to.api.usercenter.CheckCertificateIsPassResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【资质】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "qualificationAPIController")
@RequestMapping("api/qualification")
public class QualificationAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(QualificationAPIController.class);


	/**
	 * 上传资质证明
     * @param request
     * @param uploadCertificateRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "uploadCertificate")
    @ResponseBody
    public Object uploadCertificate(HttpServletRequest request, UploadCertificateRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            UploadCertificateResponseTO responseTO = new UploadCertificateResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("上传资质证明出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 删除资质
     * @param request
     * @param deleteCertificateRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "deleteCertificate")
    @ResponseBody
    public Object deleteCertificate(HttpServletRequest request, DeleteCertificateRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            DeleteCertificateResponseTO responseTO = new DeleteCertificateResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("删除资质出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取资质列表
     * @param request
     * @param getCertificateListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCertificateList")
    @ResponseBody
    public Object getCertificateList(HttpServletRequest request, GetCertificateListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCertificateListResponseTO responseTO = new GetCertificateListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取资质列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 校验是否通过了资质验证
     * @param request
     * @param checkCertificateIsPassRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "checkCertificateIsPass")
    @ResponseBody
    public Object checkCertificateIsPass(HttpServletRequest request, CheckCertificateIsPassRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            CheckCertificateIsPassResponseTO responseTO = new CheckCertificateIsPassResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("校验是否通过了资质验证出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
