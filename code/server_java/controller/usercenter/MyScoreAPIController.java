package com.jzl.controller.api.usercenter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.usercenter.GetMemberPointsRecordRequestTO;
import com.jzl.to.api.usercenter.GetMemberPointsRecordResponseTO;
import com.jzl.to.api.usercenter.GetMemberProductListRequestTO;
import com.jzl.to.api.usercenter.GetMemberProductListResponseTO;
import com.jzl.to.api.usercenter.GetMemberProductDetailRequestTO;
import com.jzl.to.api.usercenter.GetMemberProductDetailResponseTO;
import com.jzl.to.api.usercenter.ExchangeMemberScoreRequestTO;
import com.jzl.to.api.usercenter.ExchangeMemberScoreResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【积分】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "myScoreAPIController")
@RequestMapping("api/myScore")
public class MyScoreAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyScoreAPIController.class);


	/**
	 * 获得积分累计记录
     * @param request
     * @param getMemberPointsRecordRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getMemberPointsRecord")
    @ResponseBody
    public Object getMemberPointsRecord(HttpServletRequest request, GetMemberPointsRecordRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetMemberPointsRecordResponseTO responseTO = new GetMemberPointsRecordResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得积分累计记录出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得积分兑换产品列表
     * @param request
     * @param getMemberProductListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getMemberProductList")
    @ResponseBody
    public Object getMemberProductList(HttpServletRequest request, GetMemberProductListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetMemberProductListResponseTO responseTO = new GetMemberProductListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得积分兑换产品列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 积分商品详情
     * @param request
     * @param getMemberProductDetailRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getMemberProductDetail")
    @ResponseBody
    public Object getMemberProductDetail(HttpServletRequest request, GetMemberProductDetailRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetMemberProductDetailResponseTO responseTO = new GetMemberProductDetailResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("积分商品详情出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 积分兑换
     * @param request
     * @param exchangeMemberScoreRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "exchangeMemberScore")
    @ResponseBody
    public Object exchangeMemberScore(HttpServletRequest request, ExchangeMemberScoreRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            ExchangeMemberScoreResponseTO responseTO = new ExchangeMemberScoreResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("积分兑换出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
