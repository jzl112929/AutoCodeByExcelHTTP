package com.jzl.controller.api.usercenter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.usercenter.GetChargeBeanListRequestTO;
import com.jzl.to.api.usercenter.GetChargeBeanListResponseTO;
import com.jzl.to.api.usercenter.BuyStudentBeanRequestTO;
import com.jzl.to.api.usercenter.BuyStudentBeanResponseTO;
import com.jzl.to.api.usercenter.GetLeftAmountRequestTO;
import com.jzl.to.api.usercenter.GetLeftAmountResponseTO;
import com.jzl.to.api.usercenter.GetPayAndChargeRecordRequestTO;
import com.jzl.to.api.usercenter.GetPayAndChargeRecordResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【我的账户】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "myAcountAPIController")
@RequestMapping("api/myAcount")
public class MyAcountAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(MyAcountAPIController.class);


	/**
	 * 获取充值产品列表
     * @param request
     * @param getChargeBeanListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getChargeBeanList")
    @ResponseBody
    public Object getChargeBeanList(HttpServletRequest request, GetChargeBeanListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetChargeBeanListResponseTO responseTO = new GetChargeBeanListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取充值产品列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 购买学习豆
     * @param request
     * @param buyStudentBeanRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "buyStudentBean")
    @ResponseBody
    public Object buyStudentBean(HttpServletRequest request, BuyStudentBeanRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            BuyStudentBeanResponseTO responseTO = new BuyStudentBeanResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("购买学习豆出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取账户余额信息
     * @param request
     * @param getLeftAmountRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getLeftAmount")
    @ResponseBody
    public Object getLeftAmount(HttpServletRequest request, GetLeftAmountRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetLeftAmountResponseTO responseTO = new GetLeftAmountResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取账户余额信息出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获取支付和充值记录
     * @param request
     * @param getPayAndChargeRecordRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getPayAndChargeRecord")
    @ResponseBody
    public Object getPayAndChargeRecord(HttpServletRequest request, GetPayAndChargeRecordRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetPayAndChargeRecordResponseTO responseTO = new GetPayAndChargeRecordResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获取支付和充值记录出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
