package com.jzl.controller.api.usercenter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.usercenter.GetMyFavoriteListRequestTO;
import com.jzl.to.api.usercenter.GetMyFavoriteListResponseTO;
import com.jzl.to.api.usercenter.AddTeacherToMyFavoriteRequestTO;
import com.jzl.to.api.usercenter.AddTeacherToMyFavoriteResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【收藏】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "favoriteAPIController")
@RequestMapping("api/favorite")
public class FavoriteAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(FavoriteAPIController.class);


	/**
	 * 获得收藏列表
     * @param request
     * @param getMyFavoriteListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getMyFavoriteList")
    @ResponseBody
    public Object getMyFavoriteList(HttpServletRequest request, GetMyFavoriteListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetMyFavoriteListResponseTO responseTO = new GetMyFavoriteListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得收藏列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 收藏老师
     * @param request
     * @param addTeacherToMyFavoriteRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "addTeacherToMyFavorite")
    @ResponseBody
    public Object addTeacherToMyFavorite(HttpServletRequest request, AddTeacherToMyFavoriteRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            AddTeacherToMyFavoriteResponseTO responseTO = new AddTeacherToMyFavoriteResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("收藏老师出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
