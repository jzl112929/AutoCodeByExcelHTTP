package com.jzl.controller.api.usercenter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.usercenter.GetCommentListRequestTO;
import com.jzl.to.api.usercenter.GetCommentListResponseTO;
import com.jzl.to.api.usercenter.MakeCommentRequestTO;
import com.jzl.to.api.usercenter.MakeCommentResponseTO;
import com.jzl.to.api.usercenter.MakeReportRequestTO;
import com.jzl.to.api.usercenter.MakeReportResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【评价】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "commentAPIController")
@RequestMapping("api/comment")
public class CommentAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(CommentAPIController.class);


	/**
	 * 根据id和类型获得评价列表
     * @param request
     * @param getCommentListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getCommentList")
    @ResponseBody
    public Object getCommentList(HttpServletRequest request, GetCommentListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetCommentListResponseTO responseTO = new GetCommentListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("根据id和类型获得评价列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 评价
     * @param request
     * @param makeCommentRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "makeComment")
    @ResponseBody
    public Object makeComment(HttpServletRequest request, MakeCommentRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            MakeCommentResponseTO responseTO = new MakeCommentResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("评价出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 投诉或意见反馈
     * @param request
     * @param makeReportRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "makeReport")
    @ResponseBody
    public Object makeReport(HttpServletRequest request, MakeReportRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            MakeReportResponseTO responseTO = new MakeReportResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("投诉或意见反馈出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
