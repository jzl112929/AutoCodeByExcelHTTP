package com.jzl.controller.api.usercenter;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.appcore.util.JsonUtil;
import com.jzl.to.api.usercenter.GetExcellentStudentListRequestTO;
import com.jzl.to.api.usercenter.GetExcellentStudentListResponseTO;
import com.jzl.to.api.usercenter.GetExcellentTeacherListRequestTO;
import com.jzl.to.api.usercenter.GetExcellentTeacherListResponseTO;
import com.jzl.controller.AbstractAPIController;
import com.jzl.to.FailResponseTO;

/**
 * 【状元榜】控制器
 * 
 * @author AutoCode 1129290218@qq.com
 * @date 2016-12
 * 
 */
@Controller(value = "championAPIController")
@RequestMapping("api/champion")
public class ChampionAPIController extends AbstractAPIController {

    private static final Logger LOGGER = LoggerFactory.getLogger(ChampionAPIController.class);


	/**
	 * 获得状元榜列表
     * @param request
     * @param getExcellentStudentListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getExcellentStudentList")
    @ResponseBody
    public Object getExcellentStudentList(HttpServletRequest request, GetExcellentStudentListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetExcellentStudentListResponseTO responseTO = new GetExcellentStudentListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得状元榜列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }

	/**
	 * 获得名师榜列表
     * @param request
     * @param getExcellentTeacherListRequestTO
     * @return Object
	 * @author AutoCode 1129290218@qq.com
     * @date 2016-12
	 */
    @RequestMapping(value = "getExcellentTeacherList")
    @ResponseBody
    public Object getExcellentTeacherList(HttpServletRequest request, GetExcellentTeacherListRequestTO requestTO) {
        
        LOGGER.debug("请求ip【{}】，请求信息【{}】", new Object[] { request.getRemoteHost(), requestTO });

        try{
            //处理业务
            
            GetExcellentTeacherListResponseTO responseTO = new GetExcellentTeacherListResponseTO();
            return responseTO;
        }catch(Exception e){
            e.printStackTrace();
            LOGGER.error("获得名师榜列表出现异常【{}】，请求ip【{}】，请求信息【{}】", new Object[] { e.getMessage(), request.getRemoteAddr(), requestTO });
            return FailResponseTO.newFailResponseTO();
        }
		
    }




}
