package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【点评作业】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ReviewHomeworkRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//学生Id
	private Integer studentWordId;
	//评语
	private String comment;


    /**设置学生Id*/
	public void setStudentWordId(Integer studentWordId){
		this.studentWordId=studentWordId;
	}
	/**获取学生Id*/
	public Integer getStudentWordId(){
		return this.studentWordId;
	}
    /**设置评语*/
	public void setComment(String comment){
		this.comment=comment;
	}
	/**获取评语*/
	public String getComment(){
		return this.comment;
	}


}
