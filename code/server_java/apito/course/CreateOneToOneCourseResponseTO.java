package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【创建一对一课程】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateOneToOneCourseResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public CreateOneToOneCourseResponseTO(){
        this.returnCode = 1;
    }



}
