package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【根据辅导课Id及类型获取辅导课课程详情】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetTutorialRecordDetailRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//辅导课开课记录Id
	private Integer recordId;


    /**设置辅导课开课记录Id*/
	public void setRecordId(Integer recordId){
		this.recordId=recordId;
	}
	/**获取辅导课开课记录Id*/
	public Integer getRecordId(){
		return this.recordId;
	}


}
