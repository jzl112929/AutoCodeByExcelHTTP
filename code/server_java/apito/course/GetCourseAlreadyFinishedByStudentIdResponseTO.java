package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CourseBaseInfoTO;

/**
 * 【获取我已上过的课程】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCourseAlreadyFinishedByStudentIdResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程信息
    private List<CourseBaseInfoTO> courseInfoList = new ArrayList<CourseBaseInfoTO>();

    public GetCourseAlreadyFinishedByStudentIdResponseTO(){
        this.returnCode = 1;
    }

    /**设置课程信息*/
	public void setCourseInfoList(List<CourseBaseInfoTO> courseInfoList){
		this.courseInfoList=courseInfoList;
	}
	/**获取课程信息*/
	public List<CourseBaseInfoTO> getCourseInfoList(){
		return this.courseInfoList;
	}


}
