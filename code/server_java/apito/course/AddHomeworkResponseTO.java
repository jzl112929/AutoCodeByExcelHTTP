package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【布置作业】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class AddHomeworkResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public AddHomeworkResponseTO(){
        this.returnCode = 1;
    }



}
