package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获得所有专题课程（教师端）】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetAllCoursesListByTeacherIdRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//页数
	private Integer pageIndex;


    /**设置页数*/
	public void setPageIndex(Integer pageIndex){
		this.pageIndex=pageIndex;
	}
	/**获取页数*/
	public Integer getPageIndex(){
		return this.pageIndex;
	}


}
