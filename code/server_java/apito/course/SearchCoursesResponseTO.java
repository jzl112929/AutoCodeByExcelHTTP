package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CourseScheduleTO;

/**
 * 【课程搜索】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class SearchCoursesResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程信息
    private List<CourseScheduleTO> courseInfoList = new ArrayList<CourseScheduleTO>();

    public SearchCoursesResponseTO(){
        this.returnCode = 1;
    }

    /**设置课程信息*/
	public void setCourseInfoList(List<CourseScheduleTO> courseInfoList){
		this.courseInfoList=courseInfoList;
	}
	/**获取课程信息*/
	public List<CourseScheduleTO> getCourseInfoList(){
		return this.courseInfoList;
	}


}
