package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【根据学生Id获得作业列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetHomeworkListByStudentIdResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//作业列表
    private List<Object[]TO> homeworkInfoList = new ArrayList<Object[]TO>();

    public GetHomeworkListByStudentIdResponseTO(){
        this.returnCode = 1;
    }

    /**设置作业列表*/
	public void setHomeworkInfoList(List<Object[]TO> homeworkInfoList){
		this.homeworkInfoList=homeworkInfoList;
	}
	/**获取作业列表*/
	public List<Object[]TO> getHomeworkInfoList(){
		return this.homeworkInfoList;
	}


}
