package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.GroupRecordTO;

/**
 * 【获取在线课程(正在直播的群组课)列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetRunningCoursesListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//在线群组课程列表
    private List<GroupRecordTO> groupRecordList = new ArrayList<GroupRecordTO>();

    public GetRunningCoursesListResponseTO(){
        this.returnCode = 1;
    }

    /**设置在线群组课程列表*/
	public void setGroupRecordList(List<GroupRecordTO> groupRecordList){
		this.groupRecordList=groupRecordList;
	}
	/**获取在线群组课程列表*/
	public List<GroupRecordTO> getGroupRecordList(){
		return this.groupRecordList;
	}


}
