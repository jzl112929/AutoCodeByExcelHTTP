package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【完成作业】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class FinishHomeWorkResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public FinishHomeWorkResponseTO(){
        this.returnCode = 1;
    }



}
