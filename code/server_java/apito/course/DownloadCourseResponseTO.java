package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【下载课程】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class DownloadCourseResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public DownloadCourseResponseTO(){
        this.returnCode = 1;
    }



}
