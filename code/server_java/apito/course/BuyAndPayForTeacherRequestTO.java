package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【购买一对一/群组老师】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class BuyAndPayForTeacherRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程类型    1，一对一  2，群组课    
	private Integer courseType;
	//辅导课Id
	private Integer tutorialId;


    /**设置课程类型    1，一对一  2，群组课    */
	public void setCourseType(Integer courseType){
		this.courseType=courseType;
	}
	/**获取课程类型    1，一对一  2，群组课    */
	public Integer getCourseType(){
		return this.courseType;
	}
    /**设置辅导课Id*/
	public void setTutorialId(Integer tutorialId){
		this.tutorialId=tutorialId;
	}
	/**获取辅导课Id*/
	public Integer getTutorialId(){
		return this.tutorialId;
	}


}
