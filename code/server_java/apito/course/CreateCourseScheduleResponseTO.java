package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【创建专题计划】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateCourseScheduleResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程计划Id
	private Integer courseScheduleId;

    public CreateCourseScheduleResponseTO(){
        this.returnCode = 1;
    }

    /**设置课程计划Id*/
	public void setCourseScheduleId(Integer courseScheduleId){
		this.courseScheduleId=courseScheduleId;
	}
	/**获取课程计划Id*/
	public Integer getCourseScheduleId(){
		return this.courseScheduleId;
	}


}
