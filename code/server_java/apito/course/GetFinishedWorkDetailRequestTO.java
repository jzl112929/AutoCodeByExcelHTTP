package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获取已完成的作业详情】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetFinishedWorkDetailRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//学生作业Id
	private Integer studentWorkId;
	//老师作业Id
	private Integer teacherWorkId;


    /**设置学生作业Id*/
	public void setStudentWorkId(Integer studentWorkId){
		this.studentWorkId=studentWorkId;
	}
	/**获取学生作业Id*/
	public Integer getStudentWorkId(){
		return this.studentWorkId;
	}
    /**设置老师作业Id*/
	public void setTeacherWorkId(Integer teacherWorkId){
		this.teacherWorkId=teacherWorkId;
	}
	/**获取老师作业Id*/
	public Integer getTeacherWorkId(){
		return this.teacherWorkId;
	}


}
