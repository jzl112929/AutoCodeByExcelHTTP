package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获取学生作业详情（学生端）】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetHomeworkDetailByStudentIdRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//作业Id
	private Integer homeworkId;


    /**设置作业Id*/
	public void setHomeworkId(Integer homeworkId){
		this.homeworkId=homeworkId;
	}
	/**获取作业Id*/
	public Integer getHomeworkId(){
		return this.homeworkId;
	}


}
