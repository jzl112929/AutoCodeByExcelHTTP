package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【提醒完成作业】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class RemindDoneHomeworkRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//学生Id
    private List<Integer> studentIds = new ArrayList<Integer>();
	//作业Id
	private Integer studentWorkId;


    /**设置学生Id*/
	public void setStudentIds(List<Integer> studentIds){
		this.studentIds=studentIds;
	}
	/**获取学生Id*/
	public List<Integer> getStudentIds(){
		return this.studentIds;
	}
    /**设置作业Id*/
	public void setStudentWorkId(Integer studentWorkId){
		this.studentWorkId=studentWorkId;
	}
	/**获取作业Id*/
	public Integer getStudentWorkId(){
		return this.studentWorkId;
	}


}
