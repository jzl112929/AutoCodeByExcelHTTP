package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.GroupRecordDetailTO;

/**
 * 【获取在线课程详情】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetGroupRecordCoursesDetailResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//在线辅导课详情
	private GroupRecordDetailTO groupRecordDetail = new GroupRecordDetailTO();

    public GetGroupRecordCoursesDetailResponseTO(){
        this.returnCode = 1;
    }

    /**设置在线辅导课详情*/
	public void setGroupRecordDetail(GroupRecordDetailTO groupRecordDetail){
		this.groupRecordDetail=groupRecordDetail;
	}
	/**获取在线辅导课详情*/
	public GroupRecordDetailTO getGroupRecordDetail(){
		return this.groupRecordDetail;
	}


}
