package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【创建专题课程】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateSpecialSubjectCourseRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//专题名称
	private String courseName;
	//专题价格（学习豆）
	private Integer price;
	//专题简介
	private String description;
	//课程计划
    private List<Integer> courseScheduleIds = new ArrayList<Integer>();
	//是否推广    0、不推广  1、推广
	private Integer isAdvertised;
	//课程等级    1、幼儿园   2、小学   3、初中  4、高中  5、大学  6、研究生   7、博士
	private Integer schoolType;
	//科目类别    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理
	private Integer type;
	//课程缩略图地址
    private List<Object[]TO> pic = new ArrayList<Object[]TO>();
	//开始时间
	private String startTime;
	//结束时间
	private String endTime;


    /**设置专题名称*/
	public void setCourseName(String courseName){
		this.courseName=courseName;
	}
	/**获取专题名称*/
	public String getCourseName(){
		return this.courseName;
	}
    /**设置专题价格（学习豆）*/
	public void setPrice(Integer price){
		this.price=price;
	}
	/**获取专题价格（学习豆）*/
	public Integer getPrice(){
		return this.price;
	}
    /**设置专题简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取专题简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置课程计划*/
	public void setCourseScheduleIds(List<Integer> courseScheduleIds){
		this.courseScheduleIds=courseScheduleIds;
	}
	/**获取课程计划*/
	public List<Integer> getCourseScheduleIds(){
		return this.courseScheduleIds;
	}
    /**设置是否推广    0、不推广  1、推广*/
	public void setIsAdvertised(Integer isAdvertised){
		this.isAdvertised=isAdvertised;
	}
	/**获取是否推广    0、不推广  1、推广*/
	public Integer getIsAdvertised(){
		return this.isAdvertised;
	}
    /**设置课程等级    1、幼儿园   2、小学   3、初中  4、高中  5、大学  6、研究生   7、博士*/
	public void setSchoolType(Integer schoolType){
		this.schoolType=schoolType;
	}
	/**获取课程等级    1、幼儿园   2、小学   3、初中  4、高中  5、大学  6、研究生   7、博士*/
	public Integer getSchoolType(){
		return this.schoolType;
	}
    /**设置科目类别    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取科目类别    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理*/
	public Integer getType(){
		return this.type;
	}
    /**设置课程缩略图地址*/
	public void setPic(List<Object[]TO> pic){
		this.pic=pic;
	}
	/**获取课程缩略图地址*/
	public List<Object[]TO> getPic(){
		return this.pic;
	}
    /**设置开始时间*/
	public void setStartTime(String startTime){
		this.startTime=startTime;
	}
	/**获取开始时间*/
	public String getStartTime(){
		return this.startTime;
	}
    /**设置结束时间*/
	public void setEndTime(String endTime){
		this.endTime=endTime;
	}
	/**获取结束时间*/
	public String getEndTime(){
		return this.endTime;
	}


}
