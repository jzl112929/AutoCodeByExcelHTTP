package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.StudentTutorialTO;

/**
 * 【获取已购买群组该群组服务的学生列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetStudentListBuyTutorialIdResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//学生名单
    private List<StudentTutorialTO> studentList = new ArrayList<StudentTutorialTO>();

    public GetStudentListBuyTutorialIdResponseTO(){
        this.returnCode = 1;
    }

    /**设置学生名单*/
	public void setStudentList(List<StudentTutorialTO> studentList){
		this.studentList=studentList;
	}
	/**获取学生名单*/
	public List<StudentTutorialTO> getStudentList(){
		return this.studentList;
	}


}
