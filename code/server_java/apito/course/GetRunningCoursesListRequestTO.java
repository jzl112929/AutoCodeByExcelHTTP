package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获取在线课程(正在直播的群组课)列表】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetRunningCoursesListRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//城市Id
	private String regionId;
	//排序类型    1、开课时间  2、购买人数  3、好评率  4、学习豆由低到高  5、学习豆由高到底
	private Integer sortType;
	//页数
	private Integer pageIndex;
	//课程等级    1、幼儿园   2、小学   3、初中  4、高中  5、大学  6、研究生   7、博士
    private List<Integer> schoolType = new ArrayList<Integer>();
	//科目类别    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理
    private List<Integer> type = new ArrayList<Integer>();
	//关键字    老师昵称、课程名称
	private String searchStr;


    /**设置城市Id*/
	public void setRegionId(String regionId){
		this.regionId=regionId;
	}
	/**获取城市Id*/
	public String getRegionId(){
		return this.regionId;
	}
    /**设置排序类型    1、开课时间  2、购买人数  3、好评率  4、学习豆由低到高  5、学习豆由高到底*/
	public void setSortType(Integer sortType){
		this.sortType=sortType;
	}
	/**获取排序类型    1、开课时间  2、购买人数  3、好评率  4、学习豆由低到高  5、学习豆由高到底*/
	public Integer getSortType(){
		return this.sortType;
	}
    /**设置页数*/
	public void setPageIndex(Integer pageIndex){
		this.pageIndex=pageIndex;
	}
	/**获取页数*/
	public Integer getPageIndex(){
		return this.pageIndex;
	}
    /**设置课程等级    1、幼儿园   2、小学   3、初中  4、高中  5、大学  6、研究生   7、博士*/
	public void setSchoolType(List<Integer> schoolType){
		this.schoolType=schoolType;
	}
	/**获取课程等级    1、幼儿园   2、小学   3、初中  4、高中  5、大学  6、研究生   7、博士*/
	public List<Integer> getSchoolType(){
		return this.schoolType;
	}
    /**设置科目类别    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理*/
	public void setType(List<Integer> type){
		this.type=type;
	}
	/**获取科目类别    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理*/
	public List<Integer> getType(){
		return this.type;
	}
    /**设置关键字    老师昵称、课程名称*/
	public void setSearchStr(String searchStr){
		this.searchStr=searchStr;
	}
	/**获取关键字    老师昵称、课程名称*/
	public String getSearchStr(){
		return this.searchStr;
	}


}
