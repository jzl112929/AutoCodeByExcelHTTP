package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【上传课程标识图片】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UploadPicForCourseResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//缩率图
	private String pic;

    public UploadPicForCourseResponseTO(){
        this.returnCode = 1;
    }

    /**设置缩率图*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取缩率图*/
	public String getPic(){
		return this.pic;
	}


}
