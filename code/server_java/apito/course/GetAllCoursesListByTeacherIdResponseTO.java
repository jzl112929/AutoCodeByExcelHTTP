package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CourseBaseInfoTO;

/**
 * 【获得所有专题课程（教师端）】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetAllCoursesListByTeacherIdResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程基础信息
    private List<CourseBaseInfoTO> courseBaseInfo = new ArrayList<CourseBaseInfoTO>();

    public GetAllCoursesListByTeacherIdResponseTO(){
        this.returnCode = 1;
    }

    /**设置课程基础信息*/
	public void setCourseBaseInfo(List<CourseBaseInfoTO> courseBaseInfo){
		this.courseBaseInfo=courseBaseInfo;
	}
	/**获取课程基础信息*/
	public List<CourseBaseInfoTO> getCourseBaseInfo(){
		return this.courseBaseInfo;
	}


}
