package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【创建一对一课程】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateOneToOneCourseRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程名称
	private String courseName;
	//辅导课Id
	private Integer tutorialId;
	//学生Id
	private Integer studentId;
	//课时类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟
	private Integer timeType;
	//课程简介
	private String description;
	//课程图片
    private List<Object[]TO> pic = new ArrayList<Object[]TO>();


    /**设置课程名称*/
	public void setCourseName(String courseName){
		this.courseName=courseName;
	}
	/**获取课程名称*/
	public String getCourseName(){
		return this.courseName;
	}
    /**设置辅导课Id*/
	public void setTutorialId(Integer tutorialId){
		this.tutorialId=tutorialId;
	}
	/**获取辅导课Id*/
	public Integer getTutorialId(){
		return this.tutorialId;
	}
    /**设置学生Id*/
	public void setStudentId(Integer studentId){
		this.studentId=studentId;
	}
	/**获取学生Id*/
	public Integer getStudentId(){
		return this.studentId;
	}
    /**设置课时类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟*/
	public void setTimeType(Integer timeType){
		this.timeType=timeType;
	}
	/**获取课时类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟*/
	public Integer getTimeType(){
		return this.timeType;
	}
    /**设置课程简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取课程简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置课程图片*/
	public void setPic(List<Object[]TO> pic){
		this.pic=pic;
	}
	/**获取课程图片*/
	public List<Object[]TO> getPic(){
		return this.pic;
	}


}
