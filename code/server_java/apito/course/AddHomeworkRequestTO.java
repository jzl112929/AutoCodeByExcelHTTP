package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【布置作业】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class AddHomeworkRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//科目id
	private Integer subjectId;
	//学生Id
    private List<Integer> studentIds = new ArrayList<Integer>();
	//群组Id
	private Integer groupId;
	//作业内容
	private String content;
	//作业文件
    private List<Object[]TO> files = new ArrayList<Object[]TO>();


    /**设置科目id*/
	public void setSubjectId(Integer subjectId){
		this.subjectId=subjectId;
	}
	/**获取科目id*/
	public Integer getSubjectId(){
		return this.subjectId;
	}
    /**设置学生Id*/
	public void setStudentIds(List<Integer> studentIds){
		this.studentIds=studentIds;
	}
	/**获取学生Id*/
	public List<Integer> getStudentIds(){
		return this.studentIds;
	}
    /**设置群组Id*/
	public void setGroupId(Integer groupId){
		this.groupId=groupId;
	}
	/**获取群组Id*/
	public Integer getGroupId(){
		return this.groupId;
	}
    /**设置作业内容*/
	public void setContent(String content){
		this.content=content;
	}
	/**获取作业内容*/
	public String getContent(){
		return this.content;
	}
    /**设置作业文件*/
	public void setFiles(List<Object[]TO> files){
		this.files=files;
	}
	/**获取作业文件*/
	public List<Object[]TO> getFiles(){
		return this.files;
	}


}
