package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.HomeworkBaseTO;

/**
 * 【获取已完成的作业详情】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetFinishedWorkDetailResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//作业列表
    private List<HomeworkBaseTO> homeworkInfoList = new ArrayList<HomeworkBaseTO>();

    public GetFinishedWorkDetailResponseTO(){
        this.returnCode = 1;
    }

    /**设置作业列表*/
	public void setHomeworkInfoList(List<HomeworkBaseTO> homeworkInfoList){
		this.homeworkInfoList=homeworkInfoList;
	}
	/**获取作业列表*/
	public List<HomeworkBaseTO> getHomeworkInfoList(){
		return this.homeworkInfoList;
	}


}
