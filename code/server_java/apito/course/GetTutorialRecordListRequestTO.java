package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【根据老师Id及类型获取辅导课开课记录列表】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetTutorialRecordListRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//分页页码
	private Integer pageIndex;
	//课程类型    1，一对一  2，群组课    
	private Integer courseType;


    /**设置分页页码*/
	public void setPageIndex(Integer pageIndex){
		this.pageIndex=pageIndex;
	}
	/**获取分页页码*/
	public Integer getPageIndex(){
		return this.pageIndex;
	}
    /**设置课程类型    1，一对一  2，群组课    */
	public void setCourseType(Integer courseType){
		this.courseType=courseType;
	}
	/**获取课程类型    1，一对一  2，群组课    */
	public Integer getCourseType(){
		return this.courseType;
	}


}
