package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【下载课程】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class DownloadCourseRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程ID    1，专享分享  2，活动分享  3，资讯分享  4，课程分享  5，理财产品分析
	private Integer courseId;


    /**设置课程ID    1，专享分享  2，活动分享  3，资讯分享  4，课程分享  5，理财产品分析*/
	public void setCourseId(Integer courseId){
		this.courseId=courseId;
	}
	/**获取课程ID    1，专享分享  2，活动分享  3，资讯分享  4，课程分享  5，理财产品分析*/
	public Integer getCourseId(){
		return this.courseId;
	}


}
