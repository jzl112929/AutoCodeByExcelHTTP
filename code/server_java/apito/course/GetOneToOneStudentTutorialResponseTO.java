package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.StudentTutorialTO;

/**
 * 【获取已购买一对一学生列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetOneToOneStudentTutorialResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//一对一学生名单
    private List<StudentTutorialTO> studentTutorialList = new ArrayList<StudentTutorialTO>();

    public GetOneToOneStudentTutorialResponseTO(){
        this.returnCode = 1;
    }

    /**设置一对一学生名单*/
	public void setStudentTutorialList(List<StudentTutorialTO> studentTutorialList){
		this.studentTutorialList=studentTutorialList;
	}
	/**获取一对一学生名单*/
	public List<StudentTutorialTO> getStudentTutorialList(){
		return this.studentTutorialList;
	}


}
