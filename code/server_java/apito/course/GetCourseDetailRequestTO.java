package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获得专题课程详情（老师端)】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCourseDetailRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程ID
	private Integer courseId;


    /**设置课程ID*/
	public void setCourseId(Integer courseId){
		this.courseId=courseId;
	}
	/**获取课程ID*/
	public Integer getCourseId(){
		return this.courseId;
	}


}
