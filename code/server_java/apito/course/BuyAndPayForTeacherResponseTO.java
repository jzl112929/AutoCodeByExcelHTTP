package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【购买一对一/群组老师】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class BuyAndPayForTeacherResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public BuyAndPayForTeacherResponseTO(){
        this.returnCode = 1;
    }



}
