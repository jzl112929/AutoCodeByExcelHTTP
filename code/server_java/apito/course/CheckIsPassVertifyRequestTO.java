package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【是否通过验证】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CheckIsPassVertifyRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//老师ID
	private Integer teacherId;


    /**设置老师ID*/
	public void setTeacherId(Integer teacherId){
		this.teacherId=teacherId;
	}
	/**获取老师ID*/
	public Integer getTeacherId(){
		return this.teacherId;
	}


}
