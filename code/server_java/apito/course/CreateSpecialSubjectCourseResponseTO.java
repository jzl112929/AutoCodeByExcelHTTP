package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【创建专题课程】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateSpecialSubjectCourseResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public CreateSpecialSubjectCourseResponseTO(){
        this.returnCode = 1;
    }



}
