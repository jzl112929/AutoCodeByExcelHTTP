package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【完成作业】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class FinishHomeWorkRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//作业Id
	private Integer homeworkId;
	//文件
    private List<Object[]TO> files = new ArrayList<Object[]TO>();
	//作业内容
	private String content;


    /**设置作业Id*/
	public void setHomeworkId(Integer homeworkId){
		this.homeworkId=homeworkId;
	}
	/**获取作业Id*/
	public Integer getHomeworkId(){
		return this.homeworkId;
	}
    /**设置文件*/
	public void setFiles(List<Object[]TO> files){
		this.files=files;
	}
	/**获取文件*/
	public List<Object[]TO> getFiles(){
		return this.files;
	}
    /**设置作业内容*/
	public void setContent(String content){
		this.content=content;
	}
	/**获取作业内容*/
	public String getContent(){
		return this.content;
	}


}
