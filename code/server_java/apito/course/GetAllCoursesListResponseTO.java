package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CourseInfoTO;

/**
 * 【获得所有专题课程（学生端）】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetAllCoursesListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程信息
    private List<CourseInfoTO> courseInfo = new ArrayList<CourseInfoTO>();

    public GetAllCoursesListResponseTO(){
        this.returnCode = 1;
    }

    /**设置课程信息*/
	public void setCourseInfo(List<CourseInfoTO> courseInfo){
		this.courseInfo=courseInfo;
	}
	/**获取课程信息*/
	public List<CourseInfoTO> getCourseInfo(){
		return this.courseInfo;
	}


}
