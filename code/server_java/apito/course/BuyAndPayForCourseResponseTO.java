package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【专题课程支付购买】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class BuyAndPayForCourseResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public BuyAndPayForCourseResponseTO(){
        this.returnCode = 1;
    }



}
