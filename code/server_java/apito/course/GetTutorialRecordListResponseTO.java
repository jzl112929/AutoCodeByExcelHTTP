package com.jzl.to.api.course;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.TutorialRecordTO;

/**
 * 【根据老师Id及类型获取辅导课开课记录列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetTutorialRecordListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//开课记录列表
    private List<TutorialRecordTO> tutorialRecordList = new ArrayList<TutorialRecordTO>();

    public GetTutorialRecordListResponseTO(){
        this.returnCode = 1;
    }

    /**设置开课记录列表*/
	public void setTutorialRecordList(List<TutorialRecordTO> tutorialRecordList){
		this.tutorialRecordList=tutorialRecordList;
	}
	/**获取开课记录列表*/
	public List<TutorialRecordTO> getTutorialRecordList(){
		return this.tutorialRecordList;
	}


}
