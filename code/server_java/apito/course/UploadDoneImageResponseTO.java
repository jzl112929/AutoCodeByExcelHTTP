package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【图片批阅上传】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UploadDoneImageResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public UploadDoneImageResponseTO(){
        this.returnCode = 1;
    }



}
