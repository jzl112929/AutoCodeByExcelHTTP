package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获取已购买群组该群组服务的学生列表】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetStudentListBuyTutorialIdRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//学生ID
	private Integer tutorialId;


    /**设置学生ID*/
	public void setTutorialId(Integer tutorialId){
		this.tutorialId=tutorialId;
	}
	/**获取学生ID*/
	public Integer getTutorialId(){
		return this.tutorialId;
	}


}
