package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获取我已上过的课程】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCourseAlreadyFinishedByStudentIdRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;





}
