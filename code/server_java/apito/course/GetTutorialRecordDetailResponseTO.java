package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.TutorialRecordTO;

/**
 * 【根据辅导课Id及类型获取辅导课课程详情】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetTutorialRecordDetailResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//辅导课记录详情
	private TutorialRecordTO tutorialRecordDetail = new TutorialRecordTO();

    public GetTutorialRecordDetailResponseTO(){
        this.returnCode = 1;
    }

    /**设置辅导课记录详情*/
	public void setTutorialRecordDetail(TutorialRecordTO tutorialRecordDetail){
		this.tutorialRecordDetail=tutorialRecordDetail;
	}
	/**获取辅导课记录详情*/
	public TutorialRecordTO getTutorialRecordDetail(){
		return this.tutorialRecordDetail;
	}


}
