package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【上传文件】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UploadFileResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public UploadFileResponseTO(){
        this.returnCode = 1;
    }



}
