package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【课程搜索】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class SearchCoursesRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程名称
	private String courseName;
	//老师昵称
	private String teacherName;
	//学科
	private String subject;


    /**设置课程名称*/
	public void setCourseName(String courseName){
		this.courseName=courseName;
	}
	/**获取课程名称*/
	public String getCourseName(){
		return this.courseName;
	}
    /**设置老师昵称*/
	public void setTeacherName(String teacherName){
		this.teacherName=teacherName;
	}
	/**获取老师昵称*/
	public String getTeacherName(){
		return this.teacherName;
	}
    /**设置学科*/
	public void setSubject(String subject){
		this.subject=subject;
	}
	/**获取学科*/
	public String getSubject(){
		return this.subject;
	}


}
