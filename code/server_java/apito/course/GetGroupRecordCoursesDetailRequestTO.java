package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获取在线课程详情】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetGroupRecordCoursesDetailRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//辅导课记录Id
	private Integer recordId;


    /**设置辅导课记录Id*/
	public void setRecordId(Integer recordId){
		this.recordId=recordId;
	}
	/**获取辅导课记录Id*/
	public Integer getRecordId(){
		return this.recordId;
	}


}
