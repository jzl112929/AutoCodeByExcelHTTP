package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【创建专题计划】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateCourseScheduleRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程名称
	private String courseName;
	//专题简介
	private String description;
	//开始时间
	private String startTime;
	//结束时间
	private String endTime;


    /**设置课程名称*/
	public void setCourseName(String courseName){
		this.courseName=courseName;
	}
	/**获取课程名称*/
	public String getCourseName(){
		return this.courseName;
	}
    /**设置专题简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取专题简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置开始时间*/
	public void setStartTime(String startTime){
		this.startTime=startTime;
	}
	/**获取开始时间*/
	public String getStartTime(){
		return this.startTime;
	}
    /**设置结束时间*/
	public void setEndTime(String endTime){
		this.endTime=endTime;
	}
	/**获取结束时间*/
	public String getEndTime(){
		return this.endTime;
	}


}
