package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【点评作业】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ReviewHomeworkResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public ReviewHomeworkResponseTO(){
        this.returnCode = 1;
    }



}
