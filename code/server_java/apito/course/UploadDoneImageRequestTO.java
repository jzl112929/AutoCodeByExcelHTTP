package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【图片批阅上传】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UploadDoneImageRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//文件Id
	private Integer fileId;


    /**设置文件Id*/
	public void setFileId(Integer fileId){
		this.fileId=fileId;
	}
	/**获取文件Id*/
	public Integer getFileId(){
		return this.fileId;
	}


}
