package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【提醒完成作业】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class RemindDoneHomeworkResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public RemindDoneHomeworkResponseTO(){
        this.returnCode = 1;
    }



}
