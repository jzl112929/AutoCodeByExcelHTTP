package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.SpecialCourseInfoTO;

/**
 * 【获得专题课程详情（学生端）】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCourseDetailForStudentResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//专题课详情信息
	private SpecialCourseInfoTO specialCourseInfo = new SpecialCourseInfoTO();

    public GetCourseDetailForStudentResponseTO(){
        this.returnCode = 1;
    }

    /**设置专题课详情信息*/
	public void setSpecialCourseInfo(SpecialCourseInfoTO specialCourseInfo){
		this.specialCourseInfo=specialCourseInfo;
	}
	/**获取专题课详情信息*/
	public SpecialCourseInfoTO getSpecialCourseInfo(){
		return this.specialCourseInfo;
	}


}
