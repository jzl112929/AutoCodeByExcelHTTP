package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.HomeworkInfoTO;

/**
 * 【获取学生作业详情（学生端）】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetHomeworkDetailByStudentIdResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//作业详情
	private HomeworkInfoTO homeworkInfo = new HomeworkInfoTO();

    public GetHomeworkDetailByStudentIdResponseTO(){
        this.returnCode = 1;
    }

    /**设置作业详情*/
	public void setHomeworkInfo(HomeworkInfoTO homeworkInfo){
		this.homeworkInfo=homeworkInfo;
	}
	/**获取作业详情*/
	public HomeworkInfoTO getHomeworkInfo(){
		return this.homeworkInfo;
	}


}
