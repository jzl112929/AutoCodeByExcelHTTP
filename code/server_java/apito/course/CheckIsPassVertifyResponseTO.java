package com.jzl.to.api.course;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CourseScheduleTO;

/**
 * 【是否通过验证】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CheckIsPassVertifyResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//老师信息
	private CourseScheduleTO teacherInfo = new CourseScheduleTO();

    public CheckIsPassVertifyResponseTO(){
        this.returnCode = 1;
    }

    /**设置老师信息*/
	public void setTeacherInfo(CourseScheduleTO teacherInfo){
		this.teacherInfo=teacherInfo;
	}
	/**获取老师信息*/
	public CourseScheduleTO getTeacherInfo(){
		return this.teacherInfo;
	}


}
