package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【年级】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GradeTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//年级Id
	private Integer gradeId;
	//年级名称
	private String gradeName;


    /**设置年级Id*/
	public void setGradeId(Integer gradeId){
		this.gradeId=gradeId;
	}
	/**获取年级Id*/
	public Integer getGradeId(){
		return this.gradeId;
	}
    /**设置年级名称*/
	public void setGradeName(String gradeName){
		this.gradeName=gradeName;
	}
	/**获取年级名称*/
	public String getGradeName(){
		return this.gradeName;
	}


}
