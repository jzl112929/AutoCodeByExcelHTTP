package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【学科】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class SubjectTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//学科Id
	private Integer subjectId;
	//学科名称
	private String subjectName;


    /**设置学科Id*/
	public void setSubjectId(Integer subjectId){
		this.subjectId=subjectId;
	}
	/**获取学科Id*/
	public Integer getSubjectId(){
		return this.subjectId;
	}
    /**设置学科名称*/
	public void setSubjectName(String subjectName){
		this.subjectName=subjectName;
	}
	/**获取学科名称*/
	public String getSubjectName(){
		return this.subjectName;
	}


}
