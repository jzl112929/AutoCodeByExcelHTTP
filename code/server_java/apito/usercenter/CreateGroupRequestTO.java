package com.jzl.to.api.usercenter;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【创建群组】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateGroupRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//群组名称
	private String groupName;
	//辅导服务Id
	private Integer tutorialId;
	//人数
	private Integer memberNum;
	//群组图片
    private List<Object[]TO> files = new ArrayList<Object[]TO>();
	//课程简介
	private String description;


    /**设置群组名称*/
	public void setGroupName(String groupName){
		this.groupName=groupName;
	}
	/**获取群组名称*/
	public String getGroupName(){
		return this.groupName;
	}
    /**设置辅导服务Id*/
	public void setTutorialId(Integer tutorialId){
		this.tutorialId=tutorialId;
	}
	/**获取辅导服务Id*/
	public Integer getTutorialId(){
		return this.tutorialId;
	}
    /**设置人数*/
	public void setMemberNum(Integer memberNum){
		this.memberNum=memberNum;
	}
	/**获取人数*/
	public Integer getMemberNum(){
		return this.memberNum;
	}
    /**设置群组图片*/
	public void setFiles(List<Object[]TO> files){
		this.files=files;
	}
	/**获取群组图片*/
	public List<Object[]TO> getFiles(){
		return this.files;
	}
    /**设置课程简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取课程简介*/
	public String getDescription(){
		return this.description;
	}


}
