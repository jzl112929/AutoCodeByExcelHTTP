package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【投诉或意见反馈】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class MakeReportResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public MakeReportResponseTO(){
        this.returnCode = 1;
    }



}
