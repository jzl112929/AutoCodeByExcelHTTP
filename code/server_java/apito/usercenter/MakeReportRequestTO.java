package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【投诉或意见反馈】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class MakeReportRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//被投诉人    反馈意见可不传参
	private Integer atUserId;
	//投诉内容
	private String content;
	//类型    1、投诉  2、意见反馈
	private Integer reportType;


    /**设置被投诉人    反馈意见可不传参*/
	public void setAtUserId(Integer atUserId){
		this.atUserId=atUserId;
	}
	/**获取被投诉人    反馈意见可不传参*/
	public Integer getAtUserId(){
		return this.atUserId;
	}
    /**设置投诉内容*/
	public void setContent(String content){
		this.content=content;
	}
	/**获取投诉内容*/
	public String getContent(){
		return this.content;
	}
    /**设置类型    1、投诉  2、意见反馈*/
	public void setReportType(Integer reportType){
		this.reportType=reportType;
	}
	/**获取类型    1、投诉  2、意见反馈*/
	public Integer getReportType(){
		return this.reportType;
	}


}
