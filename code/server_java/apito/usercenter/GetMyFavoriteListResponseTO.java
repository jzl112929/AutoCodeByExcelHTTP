package com.jzl.to.api.usercenter;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.TeacherBaseInfoTO;

/**
 * 【获得收藏列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetMyFavoriteListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//收藏列表
    private List<TeacherBaseInfoTO> teacherBaseList = new ArrayList<TeacherBaseInfoTO>();

    public GetMyFavoriteListResponseTO(){
        this.returnCode = 1;
    }

    /**设置收藏列表*/
	public void setTeacherBaseList(List<TeacherBaseInfoTO> teacherBaseList){
		this.teacherBaseList=teacherBaseList;
	}
	/**获取收藏列表*/
	public List<TeacherBaseInfoTO> getTeacherBaseList(){
		return this.teacherBaseList;
	}


}
