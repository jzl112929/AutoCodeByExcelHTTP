package com.jzl.to.api.usercenter;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.PayAndChargeRecordTO;

/**
 * 【获得收支明细记录】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetBalanceOfPayRecordResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//支付与充值记录
    private List<PayAndChargeRecordTO> payAndChargeRecord = new ArrayList<PayAndChargeRecordTO>();

    public GetBalanceOfPayRecordResponseTO(){
        this.returnCode = 1;
    }

    /**设置支付与充值记录*/
	public void setPayAndChargeRecord(List<PayAndChargeRecordTO> payAndChargeRecord){
		this.payAndChargeRecord=payAndChargeRecord;
	}
	/**获取支付与充值记录*/
	public List<PayAndChargeRecordTO> getPayAndChargeRecord(){
		return this.payAndChargeRecord;
	}


}
