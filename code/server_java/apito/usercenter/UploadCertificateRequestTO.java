package com.jzl.to.api.usercenter;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【上传资质证明】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UploadCertificateRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//资质认证文件
    private List<Object[]TO> file = new ArrayList<Object[]TO>();
	//资质类型    1、身份证正面  2、身份证反面  3、教师资格证  4、其他毕业证  5、作品  6、普通话等级  7、在校学生证
	private Integer type;
	//资质Id
	private Integer certificateId;


    /**设置资质认证文件*/
	public void setFile(List<Object[]TO> file){
		this.file=file;
	}
	/**获取资质认证文件*/
	public List<Object[]TO> getFile(){
		return this.file;
	}
    /**设置资质类型    1、身份证正面  2、身份证反面  3、教师资格证  4、其他毕业证  5、作品  6、普通话等级  7、在校学生证*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取资质类型    1、身份证正面  2、身份证反面  3、教师资格证  4、其他毕业证  5、作品  6、普通话等级  7、在校学生证*/
	public Integer getType(){
		return this.type;
	}
    /**设置资质Id*/
	public void setCertificateId(Integer certificateId){
		this.certificateId=certificateId;
	}
	/**获取资质Id*/
	public Integer getCertificateId(){
		return this.certificateId;
	}


}
