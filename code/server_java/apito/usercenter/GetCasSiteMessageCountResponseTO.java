package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获得系统消息统计】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCasSiteMessageCountResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//消息数目
	private Integer messageNum;

    public GetCasSiteMessageCountResponseTO(){
        this.returnCode = 1;
    }

    /**设置消息数目*/
	public void setMessageNum(Integer messageNum){
		this.messageNum=messageNum;
	}
	/**获取消息数目*/
	public Integer getMessageNum(){
		return this.messageNum;
	}


}
