package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【根据id和类型获得评价列表】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCommentListRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//被评人Id
	private Integer atUserId;
	//分页页码
	private Integer pageIndex;
	//被评人类型    1、老师  2、学生
	private Integer type;


    /**设置被评人Id*/
	public void setAtUserId(Integer atUserId){
		this.atUserId=atUserId;
	}
	/**获取被评人Id*/
	public Integer getAtUserId(){
		return this.atUserId;
	}
    /**设置分页页码*/
	public void setPageIndex(Integer pageIndex){
		this.pageIndex=pageIndex;
	}
	/**获取分页页码*/
	public Integer getPageIndex(){
		return this.pageIndex;
	}
    /**设置被评人类型    1、老师  2、学生*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取被评人类型    1、老师  2、学生*/
	public Integer getType(){
		return this.type;
	}


}
