package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【校验是否通过了资质验证】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CheckCertificateIsPassResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//资质审核结果  张浩  0、不通过  1、通过
	private Integer isPass;

    public CheckCertificateIsPassResponseTO(){
        this.returnCode = 1;
    }

    /**设置资质审核结果  张浩  0、不通过  1、通过*/
	public void setIsPass(Integer isPass){
		this.isPass=isPass;
	}
	/**获取资质审核结果  张浩  0、不通过  1、通过*/
	public Integer getIsPass(){
		return this.isPass;
	}


}
