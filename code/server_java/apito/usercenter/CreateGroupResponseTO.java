package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【创建群组】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateGroupResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public CreateGroupResponseTO(){
        this.returnCode = 1;
    }



}
