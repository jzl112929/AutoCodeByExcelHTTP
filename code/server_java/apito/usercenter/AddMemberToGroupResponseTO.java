package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【加入群组】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class AddMemberToGroupResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public AddMemberToGroupResponseTO(){
        this.returnCode = 1;
    }



}
