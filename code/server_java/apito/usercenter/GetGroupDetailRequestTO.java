package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获得群组详情】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetGroupDetailRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//群组Id
	private Integer groupId;


    /**设置群组Id*/
	public void setGroupId(Integer groupId){
		this.groupId=groupId;
	}
	/**获取群组Id*/
	public Integer getGroupId(){
		return this.groupId;
	}


}
