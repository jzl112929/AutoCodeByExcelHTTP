package com.jzl.to.api.usercenter;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CertificateBaseTO;

/**
 * 【获取资质列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCertificateListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//资质列表
    private List<CertificateBaseTO> certificateList = new ArrayList<CertificateBaseTO>();

    public GetCertificateListResponseTO(){
        this.returnCode = 1;
    }

    /**设置资质列表*/
	public void setCertificateList(List<CertificateBaseTO> certificateList){
		this.certificateList=certificateList;
	}
	/**获取资质列表*/
	public List<CertificateBaseTO> getCertificateList(){
		return this.certificateList;
	}


}
