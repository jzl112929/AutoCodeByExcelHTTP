package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【删除资质】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class DeleteCertificateResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public DeleteCertificateResponseTO(){
        this.returnCode = 1;
    }



}
