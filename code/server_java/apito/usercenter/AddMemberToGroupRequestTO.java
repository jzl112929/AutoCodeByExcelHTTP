package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【加入群组】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class AddMemberToGroupRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//学生Id
	private Integer studentId;
	//群组Id
	private Integer groupId;


    /**设置学生Id*/
	public void setStudentId(Integer studentId){
		this.studentId=studentId;
	}
	/**获取学生Id*/
	public Integer getStudentId(){
		return this.studentId;
	}
    /**设置群组Id*/
	public void setGroupId(Integer groupId){
		this.groupId=groupId;
	}
	/**获取群组Id*/
	public Integer getGroupId(){
		return this.groupId;
	}


}
