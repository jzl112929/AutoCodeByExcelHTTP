package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【收藏老师】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class AddTeacherToMyFavoriteResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public AddTeacherToMyFavoriteResponseTO(){
        this.returnCode = 1;
    }



}
