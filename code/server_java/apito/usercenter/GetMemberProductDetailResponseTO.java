package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.ScoreProductTO;

/**
 * 【积分商品详情】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetMemberProductDetailResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//产品详情
	private ScoreProductTO memberProductInfo = new ScoreProductTO();

    public GetMemberProductDetailResponseTO(){
        this.returnCode = 1;
    }

    /**设置产品详情*/
	public void setMemberProductInfo(ScoreProductTO memberProductInfo){
		this.memberProductInfo=memberProductInfo;
	}
	/**获取产品详情*/
	public ScoreProductTO getMemberProductInfo(){
		return this.memberProductInfo;
	}


}
