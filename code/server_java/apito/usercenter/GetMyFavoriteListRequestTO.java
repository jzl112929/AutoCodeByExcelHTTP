package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获得收藏列表】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetMyFavoriteListRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//分页页码
	private Integer pageIndex;


    /**设置分页页码*/
	public void setPageIndex(Integer pageIndex){
		this.pageIndex=pageIndex;
	}
	/**获取分页页码*/
	public Integer getPageIndex(){
		return this.pageIndex;
	}


}
