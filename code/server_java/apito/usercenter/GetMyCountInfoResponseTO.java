package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CoinInfoTO;

/**
 * 【获得我的可提现金额】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetMyCountInfoResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//可提现金额
	private CoinInfoTO coinInfo = new CoinInfoTO();

    public GetMyCountInfoResponseTO(){
        this.returnCode = 1;
    }

    /**设置可提现金额*/
	public void setCoinInfo(CoinInfoTO coinInfo){
		this.coinInfo=coinInfo;
	}
	/**获取可提现金额*/
	public CoinInfoTO getCoinInfo(){
		return this.coinInfo;
	}


}
