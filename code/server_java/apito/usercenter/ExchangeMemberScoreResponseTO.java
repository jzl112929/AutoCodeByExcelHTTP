package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【积分兑换】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ExchangeMemberScoreResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public ExchangeMemberScoreResponseTO(){
        this.returnCode = 1;
    }



}
