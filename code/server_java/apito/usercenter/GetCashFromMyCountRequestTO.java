package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【提现】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCashFromMyCountRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//提现学习豆数
	private Long coinCash;
	//收款账号
	private String cardNumber;
	//银行
	private String bank;
	//开户地址
	private String address;
	//姓名
	private String name;
	//身份证号
	private String idCard;
	//支付密码
	private String payPassword;


    /**设置提现学习豆数*/
	public void setCoinCash(Long coinCash){
		this.coinCash=coinCash;
	}
	/**获取提现学习豆数*/
	public Long getCoinCash(){
		return this.coinCash;
	}
    /**设置收款账号*/
	public void setCardNumber(String cardNumber){
		this.cardNumber=cardNumber;
	}
	/**获取收款账号*/
	public String getCardNumber(){
		return this.cardNumber;
	}
    /**设置银行*/
	public void setBank(String bank){
		this.bank=bank;
	}
	/**获取银行*/
	public String getBank(){
		return this.bank;
	}
    /**设置开户地址*/
	public void setAddress(String address){
		this.address=address;
	}
	/**获取开户地址*/
	public String getAddress(){
		return this.address;
	}
    /**设置姓名*/
	public void setName(String name){
		this.name=name;
	}
	/**获取姓名*/
	public String getName(){
		return this.name;
	}
    /**设置身份证号*/
	public void setIdCard(String idCard){
		this.idCard=idCard;
	}
	/**获取身份证号*/
	public String getIdCard(){
		return this.idCard;
	}
    /**设置支付密码*/
	public void setPayPassword(String payPassword){
		this.payPassword=payPassword;
	}
	/**获取支付密码*/
	public String getPayPassword(){
		return this.payPassword;
	}


}
