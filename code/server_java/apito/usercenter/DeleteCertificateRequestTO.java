package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【删除资质】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class DeleteCertificateRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//资质Id
	private Integer certificateId;


    /**设置资质Id*/
	public void setCertificateId(Integer certificateId){
		this.certificateId=certificateId;
	}
	/**获取资质Id*/
	public Integer getCertificateId(){
		return this.certificateId;
	}


}
