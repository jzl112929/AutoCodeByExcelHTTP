package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获得群组成员列表】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetGroupMemberListRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//群组ID
	private Integer groupId;


    /**设置群组ID*/
	public void setGroupId(Integer groupId){
		this.groupId=groupId;
	}
	/**获取群组ID*/
	public Integer getGroupId(){
		return this.groupId;
	}


}
