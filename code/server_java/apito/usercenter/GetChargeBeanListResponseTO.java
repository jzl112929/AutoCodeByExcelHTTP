package com.jzl.to.api.usercenter;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.StudentBeanTO;

/**
 * 【获取充值产品列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetChargeBeanListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//学习豆产品列表
    private List<StudentBeanTO> studentBeanList = new ArrayList<StudentBeanTO>();

    public GetChargeBeanListResponseTO(){
        this.returnCode = 1;
    }

    /**设置学习豆产品列表*/
	public void setStudentBeanList(List<StudentBeanTO> studentBeanList){
		this.studentBeanList=studentBeanList;
	}
	/**获取学习豆产品列表*/
	public List<StudentBeanTO> getStudentBeanList(){
		return this.studentBeanList;
	}


}
