package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CasStudentTO;

/**
 * 【获取账户余额信息】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetLeftAmountResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//学生账户信息
	private CasStudentTO casStudent = new CasStudentTO();

    public GetLeftAmountResponseTO(){
        this.returnCode = 1;
    }

    /**设置学生账户信息*/
	public void setCasStudent(CasStudentTO casStudent){
		this.casStudent=casStudent;
	}
	/**获取学生账户信息*/
	public CasStudentTO getCasStudent(){
		return this.casStudent;
	}


}
