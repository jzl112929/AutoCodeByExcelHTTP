package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【提现】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCashFromMyCountResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public GetCashFromMyCountResponseTO(){
        this.returnCode = 1;
    }



}
