package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【购买学习豆】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class BuyStudentBeanResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public BuyStudentBeanResponseTO(){
        this.returnCode = 1;
    }



}
