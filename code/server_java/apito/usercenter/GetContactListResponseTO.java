package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.ContactTO;

/**
 * 【获得联系人列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetContactListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//联系人列表
	private ContactTO contactList = new ContactTO();

    public GetContactListResponseTO(){
        this.returnCode = 1;
    }

    /**设置联系人列表*/
	public void setContactList(ContactTO contactList){
		this.contactList=contactList;
	}
	/**获取联系人列表*/
	public ContactTO getContactList(){
		return this.contactList;
	}


}
