package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CommentInfoTO;

/**
 * 【根据id和类型获得评价列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCommentListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//评价列表
	private CommentInfoTO commentList = new CommentInfoTO();

    public GetCommentListResponseTO(){
        this.returnCode = 1;
    }

    /**设置评价列表*/
	public void setCommentList(CommentInfoTO commentList){
		this.commentList=commentList;
	}
	/**获取评价列表*/
	public CommentInfoTO getCommentList(){
		return this.commentList;
	}


}
