package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【收藏老师】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class AddTeacherToMyFavoriteRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//辅导课Id
	private Integer tutorialId;
	//收藏类型    1、专题课  2、辅导课
	private Integer type;


    /**设置辅导课Id*/
	public void setTutorialId(Integer tutorialId){
		this.tutorialId=tutorialId;
	}
	/**获取辅导课Id*/
	public Integer getTutorialId(){
		return this.tutorialId;
	}
    /**设置收藏类型    1、专题课  2、辅导课*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取收藏类型    1、专题课  2、辅导课*/
	public Integer getType(){
		return this.type;
	}


}
