package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【评价】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class MakeCommentResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public MakeCommentResponseTO(){
        this.returnCode = 1;
    }



}
