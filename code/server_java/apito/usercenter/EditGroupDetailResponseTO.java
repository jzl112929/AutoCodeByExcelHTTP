package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【编辑群组】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class EditGroupDetailResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public EditGroupDetailResponseTO(){
        this.returnCode = 1;
    }



}
