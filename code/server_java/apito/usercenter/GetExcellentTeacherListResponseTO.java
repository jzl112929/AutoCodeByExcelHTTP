package com.jzl.to.api.usercenter;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.ChampionTO;

/**
 * 【获得名师榜列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetExcellentTeacherListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//状元榜列表
    private List<ChampionTO> championList = new ArrayList<ChampionTO>();

    public GetExcellentTeacherListResponseTO(){
        this.returnCode = 1;
    }

    /**设置状元榜列表*/
	public void setChampionList(List<ChampionTO> championList){
		this.championList=championList;
	}
	/**获取状元榜列表*/
	public List<ChampionTO> getChampionList(){
		return this.championList;
	}


}
