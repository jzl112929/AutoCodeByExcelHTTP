package com.jzl.to.api.usercenter;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.MessageInfoTO;

/**
 * 【获得系统消息列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCasSiteMessageListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//消息内容
    private List<MessageInfoTO> messageInfo = new ArrayList<MessageInfoTO>();

    public GetCasSiteMessageListResponseTO(){
        this.returnCode = 1;
    }

    /**设置消息内容*/
	public void setMessageInfo(List<MessageInfoTO> messageInfo){
		this.messageInfo=messageInfo;
	}
	/**获取消息内容*/
	public List<MessageInfoTO> getMessageInfo(){
		return this.messageInfo;
	}


}
