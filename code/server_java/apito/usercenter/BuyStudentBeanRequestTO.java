package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【购买学习豆】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class BuyStudentBeanRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//产品Id
	private Integer beanId;
	//支付方式    1、微信支付  2、支付宝支付
	private Integer payType;


    /**设置产品Id*/
	public void setBeanId(Integer beanId){
		this.beanId=beanId;
	}
	/**获取产品Id*/
	public Integer getBeanId(){
		return this.beanId;
	}
    /**设置支付方式    1、微信支付  2、支付宝支付*/
	public void setPayType(Integer payType){
		this.payType=payType;
	}
	/**获取支付方式    1、微信支付  2、支付宝支付*/
	public Integer getPayType(){
		return this.payType;
	}


}
