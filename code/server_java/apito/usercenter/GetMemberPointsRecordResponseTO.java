package com.jzl.to.api.usercenter;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.MemberScoreTO;

/**
 * 【获得积分累计记录】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetMemberPointsRecordResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//积分记录列表
    private List<MemberScoreTO> memberScoreRecord = new ArrayList<MemberScoreTO>();
	//系统当前时间
	private String currentTime;

    public GetMemberPointsRecordResponseTO(){
        this.returnCode = 1;
    }

    /**设置积分记录列表*/
	public void setMemberScoreRecord(List<MemberScoreTO> memberScoreRecord){
		this.memberScoreRecord=memberScoreRecord;
	}
	/**获取积分记录列表*/
	public List<MemberScoreTO> getMemberScoreRecord(){
		return this.memberScoreRecord;
	}
    /**设置系统当前时间*/
	public void setCurrentTime(String currentTime){
		this.currentTime=currentTime;
	}
	/**获取系统当前时间*/
	public String getCurrentTime(){
		return this.currentTime;
	}


}
