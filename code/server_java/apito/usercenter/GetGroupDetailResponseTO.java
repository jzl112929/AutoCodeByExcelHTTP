package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.GroupBaseTO;

/**
 * 【获得群组详情】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetGroupDetailResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//群组详情
	private GroupBaseTO groupInfo = new GroupBaseTO();

    public GetGroupDetailResponseTO(){
        this.returnCode = 1;
    }

    /**设置群组详情*/
	public void setGroupInfo(GroupBaseTO groupInfo){
		this.groupInfo=groupInfo;
	}
	/**获取群组详情*/
	public GroupBaseTO getGroupInfo(){
		return this.groupInfo;
	}


}
