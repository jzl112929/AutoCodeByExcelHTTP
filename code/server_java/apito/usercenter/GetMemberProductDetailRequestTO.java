package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【积分商品详情】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetMemberProductDetailRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//产品Id
	private Integer productId;


    /**设置产品Id*/
	public void setProductId(Integer productId){
		this.productId=productId;
	}
	/**获取产品Id*/
	public Integer getProductId(){
		return this.productId;
	}


}
