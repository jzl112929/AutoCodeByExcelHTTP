package com.jzl.to.api.usercenter;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.MemberTO;

/**
 * 【获得群组成员列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetGroupMemberListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//群组列表
    private List<MemberTO> groupMembeList = new ArrayList<MemberTO>();

    public GetGroupMemberListResponseTO(){
        this.returnCode = 1;
    }

    /**设置群组列表*/
	public void setGroupMembeList(List<MemberTO> groupMembeList){
		this.groupMembeList=groupMembeList;
	}
	/**获取群组列表*/
	public List<MemberTO> getGroupMembeList(){
		return this.groupMembeList;
	}


}
