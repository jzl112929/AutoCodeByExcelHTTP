package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获得联系人列表】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetContactListRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//用户类型    1、学生  2、老师
	private Integer userType;


    /**设置用户类型    1、学生  2、老师*/
	public void setUserType(Integer userType){
		this.userType=userType;
	}
	/**获取用户类型    1、学生  2、老师*/
	public Integer getUserType(){
		return this.userType;
	}


}
