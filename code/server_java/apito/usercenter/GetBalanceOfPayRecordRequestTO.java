package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获得收支明细记录】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetBalanceOfPayRecordRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//分页页码
	private Integer pageIndex;
	//分页大小
	private Integer pageSize;
	//几个月    app端可不传
	private Integer month;


    /**设置分页页码*/
	public void setPageIndex(Integer pageIndex){
		this.pageIndex=pageIndex;
	}
	/**获取分页页码*/
	public Integer getPageIndex(){
		return this.pageIndex;
	}
    /**设置分页大小*/
	public void setPageSize(Integer pageSize){
		this.pageSize=pageSize;
	}
	/**获取分页大小*/
	public Integer getPageSize(){
		return this.pageSize;
	}
    /**设置几个月    app端可不传*/
	public void setMonth(Integer month){
		this.month=month;
	}
	/**获取几个月    app端可不传*/
	public Integer getMonth(){
		return this.month;
	}


}
