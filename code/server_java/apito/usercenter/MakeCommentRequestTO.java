package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【评价】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class MakeCommentRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//被评人Id
	private Integer atUserId;
	//星星值
	private Integer stars;
	//内容
	private String content;
	//被评人类型    1、老师  2、学生
	private Integer type;


    /**设置被评人Id*/
	public void setAtUserId(Integer atUserId){
		this.atUserId=atUserId;
	}
	/**获取被评人Id*/
	public Integer getAtUserId(){
		return this.atUserId;
	}
    /**设置星星值*/
	public void setStars(Integer stars){
		this.stars=stars;
	}
	/**获取星星值*/
	public Integer getStars(){
		return this.stars;
	}
    /**设置内容*/
	public void setContent(String content){
		this.content=content;
	}
	/**获取内容*/
	public String getContent(){
		return this.content;
	}
    /**设置被评人类型    1、老师  2、学生*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取被评人类型    1、老师  2、学生*/
	public Integer getType(){
		return this.type;
	}


}
