package com.jzl.to.api.usercenter;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【上传资质证明】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UploadCertificateResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//资质文件地址
	private String picUrl;
	//资质Id
	private Integer certificateId;

    public UploadCertificateResponseTO(){
        this.returnCode = 1;
    }

    /**设置资质文件地址*/
	public void setPicUrl(String picUrl){
		this.picUrl=picUrl;
	}
	/**获取资质文件地址*/
	public String getPicUrl(){
		return this.picUrl;
	}
    /**设置资质Id*/
	public void setCertificateId(Integer certificateId){
		this.certificateId=certificateId;
	}
	/**获取资质Id*/
	public Integer getCertificateId(){
		return this.certificateId;
	}


}
