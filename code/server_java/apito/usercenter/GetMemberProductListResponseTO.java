package com.jzl.to.api.usercenter;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.ScoreProductTO;

/**
 * 【获得积分兑换产品列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetMemberProductListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//会员可兑换产品列表
    private List<ScoreProductTO> memberProductList = new ArrayList<ScoreProductTO>();

    public GetMemberProductListResponseTO(){
        this.returnCode = 1;
    }

    /**设置会员可兑换产品列表*/
	public void setMemberProductList(List<ScoreProductTO> memberProductList){
		this.memberProductList=memberProductList;
	}
	/**获取会员可兑换产品列表*/
	public List<ScoreProductTO> getMemberProductList(){
		return this.memberProductList;
	}


}
