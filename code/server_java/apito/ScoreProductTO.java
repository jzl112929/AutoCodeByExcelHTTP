package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【积分产品】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ScoreProductTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//产品Id
	private Integer productId;
	//产品名称
	private String productName;
	//所需积分
	private Integer score;
	//商品价格
	private Integer price;
	//商品简介
	private String description;
	//图片
	private String pic;


    /**设置产品Id*/
	public void setProductId(Integer productId){
		this.productId=productId;
	}
	/**获取产品Id*/
	public Integer getProductId(){
		return this.productId;
	}
    /**设置产品名称*/
	public void setProductName(String productName){
		this.productName=productName;
	}
	/**获取产品名称*/
	public String getProductName(){
		return this.productName;
	}
    /**设置所需积分*/
	public void setScore(Integer score){
		this.score=score;
	}
	/**获取所需积分*/
	public Integer getScore(){
		return this.score;
	}
    /**设置商品价格*/
	public void setPrice(Integer price){
		this.price=price;
	}
	/**获取商品价格*/
	public Integer getPrice(){
		return this.price;
	}
    /**设置商品简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取商品简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置图片*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取图片*/
	public String getPic(){
		return this.pic;
	}


}
