package com.jzl.to.api.common;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【签到】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class SignAppResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//获得积分
	private Integer score;

    public SignAppResponseTO(){
        this.returnCode = 1;
    }

    /**设置获得积分*/
	public void setScore(Integer score){
		this.score=score;
	}
	/**获取获得积分*/
	public Integer getScore(){
		return this.score;
	}


}
