package com.jzl.to.api.common;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.VersionTO;

/**
 * 【获取版本信息】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetVersionResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//版本信息
	private VersionTO versionInfo = new VersionTO();

    public GetVersionResponseTO(){
        this.returnCode = 1;
    }

    /**设置版本信息*/
	public void setVersionInfo(VersionTO versionInfo){
		this.versionInfo=versionInfo;
	}
	/**获取版本信息*/
	public VersionTO getVersionInfo(){
		return this.versionInfo;
	}


}
