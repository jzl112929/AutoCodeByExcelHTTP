package com.jzl.to.api.common;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获取版本信息】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetVersionRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//平台类型    平台类型:  1、ios   2、android   3、微信   4、web
	private Integer platformType;


    /**设置平台类型    平台类型:  1、ios   2、android   3、微信   4、web*/
	public void setPlatformType(Integer platformType){
		this.platformType=platformType;
	}
	/**获取平台类型    平台类型:  1、ios   2、android   3、微信   4、web*/
	public Integer getPlatformType(){
		return this.platformType;
	}


}
