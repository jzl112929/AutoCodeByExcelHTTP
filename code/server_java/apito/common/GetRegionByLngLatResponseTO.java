package com.jzl.to.api.common;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【根据经纬度获得城市】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetRegionByLngLatResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//地区名称
	private String regionName;
	//地区Id
	private String regionId;

    public GetRegionByLngLatResponseTO(){
        this.returnCode = 1;
    }

    /**设置地区名称*/
	public void setRegionName(String regionName){
		this.regionName=regionName;
	}
	/**获取地区名称*/
	public String getRegionName(){
		return this.regionName;
	}
    /**设置地区Id*/
	public void setRegionId(String regionId){
		this.regionId=regionId;
	}
	/**获取地区Id*/
	public String getRegionId(){
		return this.regionId;
	}


}
