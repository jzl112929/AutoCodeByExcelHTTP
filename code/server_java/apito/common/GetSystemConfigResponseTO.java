package com.jzl.to.api.common;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.SystemConfigTO;

/**
 * 【获取系统相关参数】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetSystemConfigResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//配置信息
	private SystemConfigTO systemConfig = new SystemConfigTO();

    public GetSystemConfigResponseTO(){
        this.returnCode = 1;
    }

    /**设置配置信息*/
	public void setSystemConfig(SystemConfigTO systemConfig){
		this.systemConfig=systemConfig;
	}
	/**获取配置信息*/
	public SystemConfigTO getSystemConfig(){
		return this.systemConfig;
	}


}
