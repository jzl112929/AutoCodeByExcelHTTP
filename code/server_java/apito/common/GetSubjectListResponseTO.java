package com.jzl.to.api.common;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.SubjectTO;

/**
 * 【获取科目】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetSubjectListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//科目列表
    private List<SubjectTO> subjectList = new ArrayList<SubjectTO>();

    public GetSubjectListResponseTO(){
        this.returnCode = 1;
    }

    /**设置科目列表*/
	public void setSubjectList(List<SubjectTO> subjectList){
		this.subjectList=subjectList;
	}
	/**获取科目列表*/
	public List<SubjectTO> getSubjectList(){
		return this.subjectList;
	}


}
