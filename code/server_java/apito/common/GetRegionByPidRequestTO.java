package com.jzl.to.api.common;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【根据Pid获得地区信息】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetRegionByPidRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//父节点Id
	private Integer parentId;


    /**设置父节点Id*/
	public void setParentId(Integer parentId){
		this.parentId=parentId;
	}
	/**获取父节点Id*/
	public Integer getParentId(){
		return this.parentId;
	}


}
