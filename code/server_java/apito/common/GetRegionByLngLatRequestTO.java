package com.jzl.to.api.common;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【根据经纬度获得城市】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetRegionByLngLatRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//经度
	private String lat;
	//纬度
	private String lng;


    /**设置经度*/
	public void setLat(String lat){
		this.lat=lat;
	}
	/**获取经度*/
	public String getLat(){
		return this.lat;
	}
    /**设置纬度*/
	public void setLng(String lng){
		this.lng=lng;
	}
	/**获取纬度*/
	public String getLng(){
		return this.lng;
	}


}
