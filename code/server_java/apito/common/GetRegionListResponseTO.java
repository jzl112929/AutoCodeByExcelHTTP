package com.jzl.to.api.common;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.RegionTO;

/**
 * 【获取城市】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetRegionListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//城市列表
    private List<RegionTO> cityList = new ArrayList<RegionTO>();

    public GetRegionListResponseTO(){
        this.returnCode = 1;
    }

    /**设置城市列表*/
	public void setCityList(List<RegionTO> cityList){
		this.cityList=cityList;
	}
	/**获取城市列表*/
	public List<RegionTO> getCityList(){
		return this.cityList;
	}


}
