package com.jzl.to.api.common;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.GradeTO;

/**
 * 【获取年级】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetGradeListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//年级列表
    private List<GradeTO> gradeList = new ArrayList<GradeTO>();

    public GetGradeListResponseTO(){
        this.returnCode = 1;
    }

    /**设置年级列表*/
	public void setGradeList(List<GradeTO> gradeList){
		this.gradeList=gradeList;
	}
	/**获取年级列表*/
	public List<GradeTO> getGradeList(){
		return this.gradeList;
	}


}
