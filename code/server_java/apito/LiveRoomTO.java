package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【直播间用户】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class LiveRoomTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//id
	private Integer id;
	//用户Id
	private Integer userId;
	//IM群Id
	private String chatRoomId;
	//房间Id
	private Integer roomId;
	//播放地址
	private String playUrl;
	//上传地址
	private String uploadUrl;


    /**设置id*/
	public void setId(Integer id){
		this.id=id;
	}
	/**获取id*/
	public Integer getId(){
		return this.id;
	}
    /**设置用户Id*/
	public void setUserId(Integer userId){
		this.userId=userId;
	}
	/**获取用户Id*/
	public Integer getUserId(){
		return this.userId;
	}
    /**设置IM群Id*/
	public void setChatRoomId(String chatRoomId){
		this.chatRoomId=chatRoomId;
	}
	/**获取IM群Id*/
	public String getChatRoomId(){
		return this.chatRoomId;
	}
    /**设置房间Id*/
	public void setRoomId(Integer roomId){
		this.roomId=roomId;
	}
	/**获取房间Id*/
	public Integer getRoomId(){
		return this.roomId;
	}
    /**设置播放地址*/
	public void setPlayUrl(String playUrl){
		this.playUrl=playUrl;
	}
	/**获取播放地址*/
	public String getPlayUrl(){
		return this.playUrl;
	}
    /**设置上传地址*/
	public void setUploadUrl(String uploadUrl){
		this.uploadUrl=uploadUrl;
	}
	/**获取上传地址*/
	public String getUploadUrl(){
		return this.uploadUrl;
	}


}
