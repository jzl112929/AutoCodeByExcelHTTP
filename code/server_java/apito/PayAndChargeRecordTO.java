package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【消费记录】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class PayAndChargeRecordTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//学习豆数
	private Long coin;
	//消耗类型    类型:  1、获取   2、使用  
	private Integer type;
	//行为类型    行为操作:  1、充值   2、购买一对一   3、购买群组  4、购买专题课   5、推广  6、提现
	private Integer actionType;
	//消费时间
	private String time;
	//商品名称
	private String name;
	//商品图片
	private String pic;


    /**设置学习豆数*/
	public void setCoin(Long coin){
		this.coin=coin;
	}
	/**获取学习豆数*/
	public Long getCoin(){
		return this.coin;
	}
    /**设置消耗类型    类型:  1、获取   2、使用  */
	public void setType(Integer type){
		this.type=type;
	}
	/**获取消耗类型    类型:  1、获取   2、使用  */
	public Integer getType(){
		return this.type;
	}
    /**设置行为类型    行为操作:  1、充值   2、购买一对一   3、购买群组  4、购买专题课   5、推广  6、提现*/
	public void setActionType(Integer actionType){
		this.actionType=actionType;
	}
	/**获取行为类型    行为操作:  1、充值   2、购买一对一   3、购买群组  4、购买专题课   5、推广  6、提现*/
	public Integer getActionType(){
		return this.actionType;
	}
    /**设置消费时间*/
	public void setTime(String time){
		this.time=time;
	}
	/**获取消费时间*/
	public String getTime(){
		return this.time;
	}
    /**设置商品名称*/
	public void setName(String name){
		this.name=name;
	}
	/**获取商品名称*/
	public String getName(){
		return this.name;
	}
    /**设置商品图片*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取商品图片*/
	public String getPic(){
		return this.pic;
	}


}
