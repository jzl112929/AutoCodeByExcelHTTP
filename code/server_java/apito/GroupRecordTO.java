package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【在线群组记录】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GroupRecordTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//记录Id
	private Integer recordId;
	//课程名称
	private String courseName;
	//开始时间
	private String beginTime;
	//结束时间
	private String endTime;
	//课时类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟
	private Integer type;
	//讲师头像
	private String avatar;
	//讲师昵称
	private String nickname;
	//上课人数
	private Integer studentSum;


    /**设置记录Id*/
	public void setRecordId(Integer recordId){
		this.recordId=recordId;
	}
	/**获取记录Id*/
	public Integer getRecordId(){
		return this.recordId;
	}
    /**设置课程名称*/
	public void setCourseName(String courseName){
		this.courseName=courseName;
	}
	/**获取课程名称*/
	public String getCourseName(){
		return this.courseName;
	}
    /**设置开始时间*/
	public void setBeginTime(String beginTime){
		this.beginTime=beginTime;
	}
	/**获取开始时间*/
	public String getBeginTime(){
		return this.beginTime;
	}
    /**设置结束时间*/
	public void setEndTime(String endTime){
		this.endTime=endTime;
	}
	/**获取结束时间*/
	public String getEndTime(){
		return this.endTime;
	}
    /**设置课时类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取课时类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟*/
	public Integer getType(){
		return this.type;
	}
    /**设置讲师头像*/
	public void setAvatar(String avatar){
		this.avatar=avatar;
	}
	/**获取讲师头像*/
	public String getAvatar(){
		return this.avatar;
	}
    /**设置讲师昵称*/
	public void setNickname(String nickname){
		this.nickname=nickname;
	}
	/**获取讲师昵称*/
	public String getNickname(){
		return this.nickname;
	}
    /**设置上课人数*/
	public void setStudentSum(Integer studentSum){
		this.studentSum=studentSum;
	}
	/**获取上课人数*/
	public Integer getStudentSum(){
		return this.studentSum;
	}


}
