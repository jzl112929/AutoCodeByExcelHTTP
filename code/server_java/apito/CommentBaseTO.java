package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【评价记录】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CommentBaseTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//评价人
	private Integer userId;
	//星星值
	private Integer stars;
	//评价时间
	private String time;
	//评价内容
	private String content;
	//被评人
	private Integer atUserId;
	//评价姓名
	private String name;


    /**设置评价人*/
	public void setUserId(Integer userId){
		this.userId=userId;
	}
	/**获取评价人*/
	public Integer getUserId(){
		return this.userId;
	}
    /**设置星星值*/
	public void setStars(Integer stars){
		this.stars=stars;
	}
	/**获取星星值*/
	public Integer getStars(){
		return this.stars;
	}
    /**设置评价时间*/
	public void setTime(String time){
		this.time=time;
	}
	/**获取评价时间*/
	public String getTime(){
		return this.time;
	}
    /**设置评价内容*/
	public void setContent(String content){
		this.content=content;
	}
	/**获取评价内容*/
	public String getContent(){
		return this.content;
	}
    /**设置被评人*/
	public void setAtUserId(Integer atUserId){
		this.atUserId=atUserId;
	}
	/**获取被评人*/
	public Integer getAtUserId(){
		return this.atUserId;
	}
    /**设置评价姓名*/
	public void setName(String name){
		this.name=name;
	}
	/**获取评价姓名*/
	public String getName(){
		return this.name;
	}


}
