package com.jzl.to.api;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【评价记录】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CommentInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//综合星星值
	private Integer combinedStars;
	//综合评分
	private Integer combinedScore;
	//评价总数
	private Integer commentSum;
	//评价列表
	private List<CommentBaseTO> commentList = new ArrayList<CommentBaseTO>();


    /**设置综合星星值*/
	public void setCombinedStars(Integer combinedStars){
		this.combinedStars=combinedStars;
	}
	/**获取综合星星值*/
	public Integer getCombinedStars(){
		return this.combinedStars;
	}
    /**设置综合评分*/
	public void setCombinedScore(Integer combinedScore){
		this.combinedScore=combinedScore;
	}
	/**获取综合评分*/
	public Integer getCombinedScore(){
		return this.combinedScore;
	}
    /**设置评价总数*/
	public void setCommentSum(Integer commentSum){
		this.commentSum=commentSum;
	}
	/**获取评价总数*/
	public Integer getCommentSum(){
		return this.commentSum;
	}
    /**设置评价列表*/
	public void setCommentList(List<CommentBaseTO> commentList){
		this.commentList=commentList;
	}
	/**获取评价列表*/
	public List<CommentBaseTO> getCommentList(){
		return this.commentList;
	}


}
