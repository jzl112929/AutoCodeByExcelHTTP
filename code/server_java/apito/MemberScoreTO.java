package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【积分记录】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class MemberScoreTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//类型    1、获取  2、兑换
	private Integer type;
	//获取操作类型    1、注册  2、签到  3、交易  4、兑换
	private Integer actionType;
	//积分数
	private Integer score;
	//操作时间
	private String ctime;


    /**设置类型    1、获取  2、兑换*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取类型    1、获取  2、兑换*/
	public Integer getType(){
		return this.type;
	}
    /**设置获取操作类型    1、注册  2、签到  3、交易  4、兑换*/
	public void setActionType(Integer actionType){
		this.actionType=actionType;
	}
	/**获取获取操作类型    1、注册  2、签到  3、交易  4、兑换*/
	public Integer getActionType(){
		return this.actionType;
	}
    /**设置积分数*/
	public void setScore(Integer score){
		this.score=score;
	}
	/**获取积分数*/
	public Integer getScore(){
		return this.score;
	}
    /**设置操作时间*/
	public void setCtime(String ctime){
		this.ctime=ctime;
	}
	/**获取操作时间*/
	public String getCtime(){
		return this.ctime;
	}


}
