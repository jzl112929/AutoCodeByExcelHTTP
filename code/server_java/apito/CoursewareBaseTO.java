package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【课件列表】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CoursewareBaseTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//课件Id
	private Integer id;
	//课件名称
	private String name;
	//课件简介
	private String description;
	//文件地址
	private String filePath;


    /**设置课件Id*/
	public void setId(Integer id){
		this.id=id;
	}
	/**获取课件Id*/
	public Integer getId(){
		return this.id;
	}
    /**设置课件名称*/
	public void setName(String name){
		this.name=name;
	}
	/**获取课件名称*/
	public String getName(){
		return this.name;
	}
    /**设置课件简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取课件简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置文件地址*/
	public void setFilePath(String filePath){
		this.filePath=filePath;
	}
	/**获取文件地址*/
	public String getFilePath(){
		return this.filePath;
	}


}
