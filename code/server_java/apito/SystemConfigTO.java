package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【系统参数】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class SystemConfigTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//注册协议
	private String registerProtocol;
	//金币兑换比例
	private Float goldRatio;
	//帮助中心
	private String helpCenter;
	//关于
	private String anenst;
	//二维码
	private String codeAndroid;
	//ios二维码
	private String codeIos;


    /**设置注册协议*/
	public void setRegisterProtocol(String registerProtocol){
		this.registerProtocol=registerProtocol;
	}
	/**获取注册协议*/
	public String getRegisterProtocol(){
		return this.registerProtocol;
	}
    /**设置金币兑换比例*/
	public void setGoldRatio(Float goldRatio){
		this.goldRatio=goldRatio;
	}
	/**获取金币兑换比例*/
	public Float getGoldRatio(){
		return this.goldRatio;
	}
    /**设置帮助中心*/
	public void setHelpCenter(String helpCenter){
		this.helpCenter=helpCenter;
	}
	/**获取帮助中心*/
	public String getHelpCenter(){
		return this.helpCenter;
	}
    /**设置关于*/
	public void setAnenst(String anenst){
		this.anenst=anenst;
	}
	/**获取关于*/
	public String getAnenst(){
		return this.anenst;
	}
    /**设置二维码*/
	public void setCodeAndroid(String codeAndroid){
		this.codeAndroid=codeAndroid;
	}
	/**获取二维码*/
	public String getCodeAndroid(){
		return this.codeAndroid;
	}
    /**设置ios二维码*/
	public void setCodeIos(String codeIos){
		this.codeIos=codeIos;
	}
	/**获取ios二维码*/
	public String getCodeIos(){
		return this.codeIos;
	}


}
