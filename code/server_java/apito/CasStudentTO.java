package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【学生用户】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CasStudentTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//姓名
	private String name;
	//图像地址
	private String avatar;
	//性别    1、男  2、女
	private Integer sex;
	//手机号码
	private String phone;
	//年级
	private Integer grade;
	//学校
	private String school;
	//年龄
	private Integer age;
	//账户学习豆
	private Long coin;
	//会员积分
	private Long memberScore;
	//会员等级
	private String memberLevel;


    /**设置姓名*/
	public void setName(String name){
		this.name=name;
	}
	/**获取姓名*/
	public String getName(){
		return this.name;
	}
    /**设置图像地址*/
	public void setAvatar(String avatar){
		this.avatar=avatar;
	}
	/**获取图像地址*/
	public String getAvatar(){
		return this.avatar;
	}
    /**设置性别    1、男  2、女*/
	public void setSex(Integer sex){
		this.sex=sex;
	}
	/**获取性别    1、男  2、女*/
	public Integer getSex(){
		return this.sex;
	}
    /**设置手机号码*/
	public void setPhone(String phone){
		this.phone=phone;
	}
	/**获取手机号码*/
	public String getPhone(){
		return this.phone;
	}
    /**设置年级*/
	public void setGrade(Integer grade){
		this.grade=grade;
	}
	/**获取年级*/
	public Integer getGrade(){
		return this.grade;
	}
    /**设置学校*/
	public void setSchool(String school){
		this.school=school;
	}
	/**获取学校*/
	public String getSchool(){
		return this.school;
	}
    /**设置年龄*/
	public void setAge(Integer age){
		this.age=age;
	}
	/**获取年龄*/
	public Integer getAge(){
		return this.age;
	}
    /**设置账户学习豆*/
	public void setCoin(Long coin){
		this.coin=coin;
	}
	/**获取账户学习豆*/
	public Long getCoin(){
		return this.coin;
	}
    /**设置会员积分*/
	public void setMemberScore(Long memberScore){
		this.memberScore=memberScore;
	}
	/**获取会员积分*/
	public Long getMemberScore(){
		return this.memberScore;
	}
    /**设置会员等级*/
	public void setMemberLevel(String memberLevel){
		this.memberLevel=memberLevel;
	}
	/**获取会员等级*/
	public String getMemberLevel(){
		return this.memberLevel;
	}


}
