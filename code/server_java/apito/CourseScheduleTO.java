package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【课程计划】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CourseScheduleTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//计划Id
	private Integer scheduleId;
	//课程名称
	private Integer name;
	//计划开始时间
	private String startTime;
	//计划结束时间
	private String endTime;
	//课程简介
	private String description;


    /**设置计划Id*/
	public void setScheduleId(Integer scheduleId){
		this.scheduleId=scheduleId;
	}
	/**获取计划Id*/
	public Integer getScheduleId(){
		return this.scheduleId;
	}
    /**设置课程名称*/
	public void setName(Integer name){
		this.name=name;
	}
	/**获取课程名称*/
	public Integer getName(){
		return this.name;
	}
    /**设置计划开始时间*/
	public void setStartTime(String startTime){
		this.startTime=startTime;
	}
	/**获取计划开始时间*/
	public String getStartTime(){
		return this.startTime;
	}
    /**设置计划结束时间*/
	public void setEndTime(String endTime){
		this.endTime=endTime;
	}
	/**获取计划结束时间*/
	public String getEndTime(){
		return this.endTime;
	}
    /**设置课程简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取课程简介*/
	public String getDescription(){
		return this.description;
	}


}
