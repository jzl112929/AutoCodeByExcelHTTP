package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【成员】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class MemberTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//成员id
	private Integer memberId;
	//成员名称
	private Integer memberName;
	//图像地址
	private String avatar;
	//用户IM号
	private String imuserId;


    /**设置成员id*/
	public void setMemberId(Integer memberId){
		this.memberId=memberId;
	}
	/**获取成员id*/
	public Integer getMemberId(){
		return this.memberId;
	}
    /**设置成员名称*/
	public void setMemberName(Integer memberName){
		this.memberName=memberName;
	}
	/**获取成员名称*/
	public Integer getMemberName(){
		return this.memberName;
	}
    /**设置图像地址*/
	public void setAvatar(String avatar){
		this.avatar=avatar;
	}
	/**获取图像地址*/
	public String getAvatar(){
		return this.avatar;
	}
    /**设置用户IM号*/
	public void setImuserId(String imuserId){
		this.imuserId=imuserId;
	}
	/**获取用户IM号*/
	public String getImuserId(){
		return this.imuserId;
	}


}
