package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【修改用户头像】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UpdateAvatarResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//头像
	private String avatar;

    public UpdateAvatarResponseTO(){
        this.returnCode = 1;
    }

    /**设置头像*/
	public void setAvatar(String avatar){
		this.avatar=avatar;
	}
	/**获取头像*/
	public String getAvatar(){
		return this.avatar;
	}


}
