package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获得学生详情】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetStudentInfoRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//学生Id
	private Integer studentId;


    /**设置学生Id*/
	public void setStudentId(Integer studentId){
		this.studentId=studentId;
	}
	/**获取学生Id*/
	public Integer getStudentId(){
		return this.studentId;
	}


}
