package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.TeacherInfoTO;

/**
 * 【获得老师详情】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetTeacherDetailResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//老师详情信息
	private TeacherInfoTO teacherInfo = new TeacherInfoTO();

    public GetTeacherDetailResponseTO(){
        this.returnCode = 1;
    }

    /**设置老师详情信息*/
	public void setTeacherInfo(TeacherInfoTO teacherInfo){
		this.teacherInfo=teacherInfo;
	}
	/**获取老师详情信息*/
	public TeacherInfoTO getTeacherInfo(){
		return this.teacherInfo;
	}


}
