package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【完善老师信息】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ImproveCasTeacherInfoResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//已注册人数
	private Integer hasRigisterNum;

    public ImproveCasTeacherInfoResponseTO(){
        this.returnCode = 1;
    }

    /**设置已注册人数*/
	public void setHasRigisterNum(Integer hasRigisterNum){
		this.hasRigisterNum=hasRigisterNum;
	}
	/**获取已注册人数*/
	public Integer getHasRigisterNum(){
		return this.hasRigisterNum;
	}


}
