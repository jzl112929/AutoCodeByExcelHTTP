package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【校验支付密码验证码】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CheckVerificationCodeForPayPasswordResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public CheckVerificationCodeForPayPasswordResponseTO(){
        this.returnCode = 1;
    }



}
