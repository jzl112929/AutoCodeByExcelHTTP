package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【学生用户修改基本信息】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UpdateStudentCasUserResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public UpdateStudentCasUserResponseTO(){
        this.returnCode = 1;
    }



}
