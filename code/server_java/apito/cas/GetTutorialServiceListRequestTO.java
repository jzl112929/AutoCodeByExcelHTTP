package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获取老师辅导服务列表】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetTutorialServiceListRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//分页页码
	private Integer pageIndex;
	//服务类型    1、一对一  2、群组课
	private Integer type;
	//每页记录数
	private Integer pageSize;


    /**设置分页页码*/
	public void setPageIndex(Integer pageIndex){
		this.pageIndex=pageIndex;
	}
	/**获取分页页码*/
	public Integer getPageIndex(){
		return this.pageIndex;
	}
    /**设置服务类型    1、一对一  2、群组课*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取服务类型    1、一对一  2、群组课*/
	public Integer getType(){
		return this.type;
	}
    /**设置每页记录数*/
	public void setPageSize(Integer pageSize){
		this.pageSize=pageSize;
	}
	/**获取每页记录数*/
	public Integer getPageSize(){
		return this.pageSize;
	}


}
