package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【试听课程】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class AddPreviewCourseResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public AddPreviewCourseResponseTO(){
        this.returnCode = 1;
    }



}
