package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【注册获取验证码】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetVerificationCodeForRegisterResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public GetVerificationCodeForRegisterResponseTO(){
        this.returnCode = 1;
    }



}
