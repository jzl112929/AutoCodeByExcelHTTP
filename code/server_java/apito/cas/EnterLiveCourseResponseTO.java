package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【进入直播课堂】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class EnterLiveCourseResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public EnterLiveCourseResponseTO(){
        this.returnCode = 1;
    }



}
