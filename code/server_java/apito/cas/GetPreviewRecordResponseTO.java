package com.jzl.to.api.cas;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CourseBaseInfoTO;

/**
 * 【获得试听历史】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetPreviewRecordResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//试听历史记录
    private List<CourseBaseInfoTO> previewList = new ArrayList<CourseBaseInfoTO>();

    public GetPreviewRecordResponseTO(){
        this.returnCode = 1;
    }

    /**设置试听历史记录*/
	public void setPreviewList(List<CourseBaseInfoTO> previewList){
		this.previewList=previewList;
	}
	/**获取试听历史记录*/
	public List<CourseBaseInfoTO> getPreviewList(){
		return this.previewList;
	}


}
