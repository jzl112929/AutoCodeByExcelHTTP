package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【修改密码】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UpdatePasswordRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//手机号码
	private String phone;
	//密码
	private String password;


    /**设置手机号码*/
	public void setPhone(String phone){
		this.phone=phone;
	}
	/**获取手机号码*/
	public String getPhone(){
		return this.phone;
	}
    /**设置密码*/
	public void setPassword(String password){
		this.password=password;
	}
	/**获取密码*/
	public String getPassword(){
		return this.password;
	}


}
