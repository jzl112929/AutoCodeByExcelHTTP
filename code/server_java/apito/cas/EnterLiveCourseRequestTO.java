package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【进入直播课堂】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class EnterLiveCourseRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课堂Id
	private Integer courseDetailId;
	//课堂类型    1、专题课程  2、辅导课
	private Integer courseType;


    /**设置课堂Id*/
	public void setCourseDetailId(Integer courseDetailId){
		this.courseDetailId=courseDetailId;
	}
	/**获取课堂Id*/
	public Integer getCourseDetailId(){
		return this.courseDetailId;
	}
    /**设置课堂类型    1、专题课程  2、辅导课*/
	public void setCourseType(Integer courseType){
		this.courseType=courseType;
	}
	/**获取课堂类型    1、专题课程  2、辅导课*/
	public Integer getCourseType(){
		return this.courseType;
	}


}
