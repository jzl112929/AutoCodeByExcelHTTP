package com.jzl.to.api.cas;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【学生用户修改基本信息】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UpdateStudentCasUserRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//姓名
	private String name;
	//年龄
	private Integer age;
	//年级    1、幼儿园   2、小学   3、初中  4、高中  5、大学  6、研究生   7、博士
	private Integer grade;
	//学校
	private String school;
	//头像
    private List<Object[]TO> files = new ArrayList<Object[]TO>();


    /**设置姓名*/
	public void setName(String name){
		this.name=name;
	}
	/**获取姓名*/
	public String getName(){
		return this.name;
	}
    /**设置年龄*/
	public void setAge(Integer age){
		this.age=age;
	}
	/**获取年龄*/
	public Integer getAge(){
		return this.age;
	}
    /**设置年级    1、幼儿园   2、小学   3、初中  4、高中  5、大学  6、研究生   7、博士*/
	public void setGrade(Integer grade){
		this.grade=grade;
	}
	/**获取年级    1、幼儿园   2、小学   3、初中  4、高中  5、大学  6、研究生   7、博士*/
	public Integer getGrade(){
		return this.grade;
	}
    /**设置学校*/
	public void setSchool(String school){
		this.school=school;
	}
	/**获取学校*/
	public String getSchool(){
		return this.school;
	}
    /**设置头像*/
	public void setFiles(List<Object[]TO> files){
		this.files=files;
	}
	/**获取头像*/
	public List<Object[]TO> getFiles(){
		return this.files;
	}


}
