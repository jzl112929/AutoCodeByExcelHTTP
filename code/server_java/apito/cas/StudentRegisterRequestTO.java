package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【学生端注册】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class StudentRegisterRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//手机号码
	private String phone;
	//密码
	private String loginPassword;
	//验证码
	private String verificationCode;
	//用户名
	private String userName;
	//用户类型    用户类型  1、学生  2、老师  3、区域代理
	private Integer userType;


    /**设置手机号码*/
	public void setPhone(String phone){
		this.phone=phone;
	}
	/**获取手机号码*/
	public String getPhone(){
		return this.phone;
	}
    /**设置密码*/
	public void setLoginPassword(String loginPassword){
		this.loginPassword=loginPassword;
	}
	/**获取密码*/
	public String getLoginPassword(){
		return this.loginPassword;
	}
    /**设置验证码*/
	public void setVerificationCode(String verificationCode){
		this.verificationCode=verificationCode;
	}
	/**获取验证码*/
	public String getVerificationCode(){
		return this.verificationCode;
	}
    /**设置用户名*/
	public void setUserName(String userName){
		this.userName=userName;
	}
	/**获取用户名*/
	public String getUserName(){
		return this.userName;
	}
    /**设置用户类型    用户类型  1、学生  2、老师  3、区域代理*/
	public void setUserType(Integer userType){
		this.userType=userType;
	}
	/**获取用户类型    用户类型  1、学生  2、老师  3、区域代理*/
	public Integer getUserType(){
		return this.userType;
	}


}
