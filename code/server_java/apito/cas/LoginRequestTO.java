package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【登录】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class LoginRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//用户名
	private String userName;
	//密码
	private String loginPassword;
	//用户类型    用户类型  1、学生  2、老师  3、区域代理
	private Integer userType;
	//用户登录设备号
	private String deviceNo;


    /**设置用户名*/
	public void setUserName(String userName){
		this.userName=userName;
	}
	/**获取用户名*/
	public String getUserName(){
		return this.userName;
	}
    /**设置密码*/
	public void setLoginPassword(String loginPassword){
		this.loginPassword=loginPassword;
	}
	/**获取密码*/
	public String getLoginPassword(){
		return this.loginPassword;
	}
    /**设置用户类型    用户类型  1、学生  2、老师  3、区域代理*/
	public void setUserType(Integer userType){
		this.userType=userType;
	}
	/**获取用户类型    用户类型  1、学生  2、老师  3、区域代理*/
	public Integer getUserType(){
		return this.userType;
	}
    /**设置用户登录设备号*/
	public void setDeviceNo(String deviceNo){
		this.deviceNo=deviceNo;
	}
	/**获取用户登录设备号*/
	public String getDeviceNo(){
		return this.deviceNo;
	}


}
