package com.jzl.to.api.cas;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.TeacherBaseInfoTO;

/**
 * 【获得老师列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetTeachersListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//老师基础信息
    private List<TeacherBaseInfoTO> teacherBaseList = new ArrayList<TeacherBaseInfoTO>();

    public GetTeachersListResponseTO(){
        this.returnCode = 1;
    }

    /**设置老师基础信息*/
	public void setTeacherBaseList(List<TeacherBaseInfoTO> teacherBaseList){
		this.teacherBaseList=teacherBaseList;
	}
	/**获取老师基础信息*/
	public List<TeacherBaseInfoTO> getTeacherBaseList(){
		return this.teacherBaseList;
	}


}
