package com.jzl.to.api.cas;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.TeacherBaseInfoTO;

/**
 * 【获取老师辅导服务列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetTutorialServiceListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//服务基础信息
    private List<TeacherBaseInfoTO> tutorialList = new ArrayList<TeacherBaseInfoTO>();

    public GetTutorialServiceListResponseTO(){
        this.returnCode = 1;
    }

    /**设置服务基础信息*/
	public void setTutorialList(List<TeacherBaseInfoTO> tutorialList){
		this.tutorialList=tutorialList;
	}
	/**获取服务基础信息*/
	public List<TeacherBaseInfoTO> getTutorialList(){
		return this.tutorialList;
	}


}
