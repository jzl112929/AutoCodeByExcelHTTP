package com.jzl.to.api.cas;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【老师端注册】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class TeacherRegisterRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//手机号码
	private String phone;
	//密码
	private String loginPassword;
	//验证码
	private String verificationCode;
	//用户名
	private String userName;
	//授权码
	private String authCode;
	//用户类型    用户类型  1、学生  2、老师  3、区域代理
	private Integer userType;
	//认证文件
    private List<Object[]TO> file = new ArrayList<Object[]TO>();


    /**设置手机号码*/
	public void setPhone(String phone){
		this.phone=phone;
	}
	/**获取手机号码*/
	public String getPhone(){
		return this.phone;
	}
    /**设置密码*/
	public void setLoginPassword(String loginPassword){
		this.loginPassword=loginPassword;
	}
	/**获取密码*/
	public String getLoginPassword(){
		return this.loginPassword;
	}
    /**设置验证码*/
	public void setVerificationCode(String verificationCode){
		this.verificationCode=verificationCode;
	}
	/**获取验证码*/
	public String getVerificationCode(){
		return this.verificationCode;
	}
    /**设置用户名*/
	public void setUserName(String userName){
		this.userName=userName;
	}
	/**获取用户名*/
	public String getUserName(){
		return this.userName;
	}
    /**设置授权码*/
	public void setAuthCode(String authCode){
		this.authCode=authCode;
	}
	/**获取授权码*/
	public String getAuthCode(){
		return this.authCode;
	}
    /**设置用户类型    用户类型  1、学生  2、老师  3、区域代理*/
	public void setUserType(Integer userType){
		this.userType=userType;
	}
	/**获取用户类型    用户类型  1、学生  2、老师  3、区域代理*/
	public Integer getUserType(){
		return this.userType;
	}
    /**设置认证文件*/
	public void setFile(List<Object[]TO> file){
		this.file=file;
	}
	/**获取认证文件*/
	public List<Object[]TO> getFile(){
		return this.file;
	}


}
