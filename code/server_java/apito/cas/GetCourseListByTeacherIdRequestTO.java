package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【根据老师Id获取专题课程列表】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCourseListByTeacherIdRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//老师Id
	private Integer teacherId;
	//分页页码
	private Integer pageIndex;


    /**设置老师Id*/
	public void setTeacherId(Integer teacherId){
		this.teacherId=teacherId;
	}
	/**获取老师Id*/
	public Integer getTeacherId(){
		return this.teacherId;
	}
    /**设置分页页码*/
	public void setPageIndex(Integer pageIndex){
		this.pageIndex=pageIndex;
	}
	/**获取分页页码*/
	public Integer getPageIndex(){
		return this.pageIndex;
	}


}
