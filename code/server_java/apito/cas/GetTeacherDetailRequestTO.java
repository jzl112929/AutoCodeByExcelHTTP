package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获得老师详情】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetTeacherDetailRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//老师Id
	private Integer teacherId;
	//一对/群组Id
	private Integer tutorialId;


    /**设置老师Id*/
	public void setTeacherId(Integer teacherId){
		this.teacherId=teacherId;
	}
	/**获取老师Id*/
	public Integer getTeacherId(){
		return this.teacherId;
	}
    /**设置一对/群组Id*/
	public void setTutorialId(Integer tutorialId){
		this.tutorialId=tutorialId;
	}
	/**获取一对/群组Id*/
	public Integer getTutorialId(){
		return this.tutorialId;
	}


}
