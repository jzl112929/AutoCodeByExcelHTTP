package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【校验旧支付密码】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CheckOldPayPasswordResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public CheckOldPayPasswordResponseTO(){
        this.returnCode = 1;
    }



}
