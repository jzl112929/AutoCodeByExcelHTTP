package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【学生端注册】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class StudentRegisterResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//用户Id
	private Integer userId;

    public StudentRegisterResponseTO(){
        this.returnCode = 1;
    }

    /**设置用户Id*/
	public void setUserId(Integer userId){
		this.userId=userId;
	}
	/**获取用户Id*/
	public Integer getUserId(){
		return this.userId;
	}


}
