package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【修改支付密码】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UpdatePayPasswordRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//支付密码
	private String payPassword;


    /**设置支付密码*/
	public void setPayPassword(String payPassword){
		this.payPassword=payPassword;
	}
	/**获取支付密码*/
	public String getPayPassword(){
		return this.payPassword;
	}


}
