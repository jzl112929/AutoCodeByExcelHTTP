package com.jzl.to.api.cas;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【上传试听课】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UploadPreviewCourseRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//文件
    private List<Object[]TO> file = new ArrayList<Object[]TO>();


    /**设置文件*/
	public void setFile(List<Object[]TO> file){
		this.file=file;
	}
	/**获取文件*/
	public List<Object[]TO> getFile(){
		return this.file;
	}


}
