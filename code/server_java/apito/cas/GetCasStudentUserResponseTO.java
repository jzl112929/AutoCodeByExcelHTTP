package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CasStudentTO;

/**
 * 【获取学生用户信息】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCasStudentUserResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//学生用户
	private CasStudentTO casStudent = new CasStudentTO();

    public GetCasStudentUserResponseTO(){
        this.returnCode = 1;
    }

    /**设置学生用户*/
	public void setCasStudent(CasStudentTO casStudent){
		this.casStudent=casStudent;
	}
	/**获取学生用户*/
	public CasStudentTO getCasStudent(){
		return this.casStudent;
	}


}
