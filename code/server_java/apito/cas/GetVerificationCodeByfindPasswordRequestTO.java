package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【找回密码获取验证码】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetVerificationCodeByfindPasswordRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//手机号码
	private String phone;


    /**设置手机号码*/
	public void setPhone(String phone){
		this.phone=phone;
	}
	/**获取手机号码*/
	public String getPhone(){
		return this.phone;
	}


}
