package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【上传试听课】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UploadPreviewCourseResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//图片
	private String pic;
	//试听课Id
	private Integer previewCourseId;

    public UploadPreviewCourseResponseTO(){
        this.returnCode = 1;
    }

    /**设置图片*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取图片*/
	public String getPic(){
		return this.pic;
	}
    /**设置试听课Id*/
	public void setPreviewCourseId(Integer previewCourseId){
		this.previewCourseId=previewCourseId;
	}
	/**获取试听课Id*/
	public Integer getPreviewCourseId(){
		return this.previewCourseId;
	}


}
