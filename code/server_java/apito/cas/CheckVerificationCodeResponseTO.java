package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【忘记密码校验验证码】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CheckVerificationCodeResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public CheckVerificationCodeResponseTO(){
        this.returnCode = 1;
    }



}
