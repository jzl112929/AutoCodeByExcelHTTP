package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【修改支付密码】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UpdatePayPasswordResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public UpdatePayPasswordResponseTO(){
        this.returnCode = 1;
    }



}
