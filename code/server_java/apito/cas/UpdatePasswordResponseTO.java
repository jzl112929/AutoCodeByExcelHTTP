package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【修改密码】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UpdatePasswordResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public UpdatePasswordResponseTO(){
        this.returnCode = 1;
    }



}
