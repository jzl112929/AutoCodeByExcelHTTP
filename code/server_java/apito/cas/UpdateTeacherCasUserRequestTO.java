package com.jzl.to.api.cas;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【老师用户修改基本信息】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UpdateTeacherCasUserRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//昵称
	private String nickName;
	//头像
    private List<Object[]TO> files = new ArrayList<Object[]TO>();
	//性别    1、男  2、女
	private Integer sex;
	//开始从教时间
	private String beginSignTime;
	//擅长学科    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理
    private List<Integer> subjects = new ArrayList<Integer>();
	//个人简介
	private String description;
	//试听课ID
	private Integer previewCourseId;
	//城市Id
	private Integer regionId;


    /**设置昵称*/
	public void setNickName(String nickName){
		this.nickName=nickName;
	}
	/**获取昵称*/
	public String getNickName(){
		return this.nickName;
	}
    /**设置头像*/
	public void setFiles(List<Object[]TO> files){
		this.files=files;
	}
	/**获取头像*/
	public List<Object[]TO> getFiles(){
		return this.files;
	}
    /**设置性别    1、男  2、女*/
	public void setSex(Integer sex){
		this.sex=sex;
	}
	/**获取性别    1、男  2、女*/
	public Integer getSex(){
		return this.sex;
	}
    /**设置开始从教时间*/
	public void setBeginSignTime(String beginSignTime){
		this.beginSignTime=beginSignTime;
	}
	/**获取开始从教时间*/
	public String getBeginSignTime(){
		return this.beginSignTime;
	}
    /**设置擅长学科    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理*/
	public void setSubjects(List<Integer> subjects){
		this.subjects=subjects;
	}
	/**获取擅长学科    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理*/
	public List<Integer> getSubjects(){
		return this.subjects;
	}
    /**设置个人简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取个人简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置试听课ID*/
	public void setPreviewCourseId(Integer previewCourseId){
		this.previewCourseId=previewCourseId;
	}
	/**获取试听课ID*/
	public Integer getPreviewCourseId(){
		return this.previewCourseId;
	}
    /**设置城市Id*/
	public void setRegionId(Integer regionId){
		this.regionId=regionId;
	}
	/**获取城市Id*/
	public Integer getRegionId(){
		return this.regionId;
	}


}
