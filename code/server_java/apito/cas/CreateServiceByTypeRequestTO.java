package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【上架一对一/群组服务】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateServiceByTypeRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//服务类型    1、一对一  2、群组课
	private Integer type;
	//服务周期
	private Integer months;
	//服务名称
	private String name;
	//上架状态    0、未上架  1、上架
	private Integer status;
	//课程等级    学校等级:  1、幼儿园   2、小学   3、初中   4、高中   5、大学  6、研究生   7、博士  
	private Integer schoolType;
	//科目类别
	private Integer subjectType;
	//简介
	private String description;
	//课程时长类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟
	private Integer timeType;
	//人数上限
	private Integer maxNum;
	//课程次数
	private Integer lessons;


    /**设置服务类型    1、一对一  2、群组课*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取服务类型    1、一对一  2、群组课*/
	public Integer getType(){
		return this.type;
	}
    /**设置服务周期*/
	public void setMonths(Integer months){
		this.months=months;
	}
	/**获取服务周期*/
	public Integer getMonths(){
		return this.months;
	}
    /**设置服务名称*/
	public void setName(String name){
		this.name=name;
	}
	/**获取服务名称*/
	public String getName(){
		return this.name;
	}
    /**设置上架状态    0、未上架  1、上架*/
	public void setStatus(Integer status){
		this.status=status;
	}
	/**获取上架状态    0、未上架  1、上架*/
	public Integer getStatus(){
		return this.status;
	}
    /**设置课程等级    学校等级:  1、幼儿园   2、小学   3、初中   4、高中   5、大学  6、研究生   7、博士  */
	public void setSchoolType(Integer schoolType){
		this.schoolType=schoolType;
	}
	/**获取课程等级    学校等级:  1、幼儿园   2、小学   3、初中   4、高中   5、大学  6、研究生   7、博士  */
	public Integer getSchoolType(){
		return this.schoolType;
	}
    /**设置科目类别*/
	public void setSubjectType(Integer subjectType){
		this.subjectType=subjectType;
	}
	/**获取科目类别*/
	public Integer getSubjectType(){
		return this.subjectType;
	}
    /**设置简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置课程时长类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟*/
	public void setTimeType(Integer timeType){
		this.timeType=timeType;
	}
	/**获取课程时长类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟*/
	public Integer getTimeType(){
		return this.timeType;
	}
    /**设置人数上限*/
	public void setMaxNum(Integer maxNum){
		this.maxNum=maxNum;
	}
	/**获取人数上限*/
	public Integer getMaxNum(){
		return this.maxNum;
	}
    /**设置课程次数*/
	public void setLessons(Integer lessons){
		this.lessons=lessons;
	}
	/**获取课程次数*/
	public Integer getLessons(){
		return this.lessons;
	}


}
