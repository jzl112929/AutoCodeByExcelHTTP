package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【完善学生信息】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ImproveCasStudentInfoRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//姓名
	private String name;
	//年龄
	private Integer age;
	//年级
	private Integer grade;
	//学校
	private String school;
	//加强学科    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理
	private String weakSubjects;
	//用户Id
	private Integer userId;


    /**设置姓名*/
	public void setName(String name){
		this.name=name;
	}
	/**获取姓名*/
	public String getName(){
		return this.name;
	}
    /**设置年龄*/
	public void setAge(Integer age){
		this.age=age;
	}
	/**获取年龄*/
	public Integer getAge(){
		return this.age;
	}
    /**设置年级*/
	public void setGrade(Integer grade){
		this.grade=grade;
	}
	/**获取年级*/
	public Integer getGrade(){
		return this.grade;
	}
    /**设置学校*/
	public void setSchool(String school){
		this.school=school;
	}
	/**获取学校*/
	public String getSchool(){
		return this.school;
	}
    /**设置加强学科    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理*/
	public void setWeakSubjects(String weakSubjects){
		this.weakSubjects=weakSubjects;
	}
	/**获取加强学科    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理*/
	public String getWeakSubjects(){
		return this.weakSubjects;
	}
    /**设置用户Id*/
	public void setUserId(Integer userId){
		this.userId=userId;
	}
	/**获取用户Id*/
	public Integer getUserId(){
		return this.userId;
	}


}
