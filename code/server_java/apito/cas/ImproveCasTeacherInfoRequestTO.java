package com.jzl.to.api.cas;

import java.util.ArrayList;
import java.util.List;

import java.util.Date;

import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【完善老师信息】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ImproveCasTeacherInfoRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//昵称
	private String nickName;
	//性别    1、男  2、女
	private Integer sex;
	//教龄
	private Date beginSignTime;
	//擅常学科    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理
    private List<Integer> subjects = new ArrayList<Integer>();
	//个人简介
	private String description;
	//用户Id
	private Integer userId;
	//城市Id
	private Integer regionId;


    /**设置昵称*/
	public void setNickName(String nickName){
		this.nickName=nickName;
	}
	/**获取昵称*/
	public String getNickName(){
		return this.nickName;
	}
    /**设置性别    1、男  2、女*/
	public void setSex(Integer sex){
		this.sex=sex;
	}
	/**获取性别    1、男  2、女*/
	public Integer getSex(){
		return this.sex;
	}
    /**设置教龄*/
	public void setBeginSignTime(Date beginSignTime){
		this.beginSignTime=beginSignTime;
	}
	/**获取教龄*/
	public Date getBeginSignTime(){
		return this.beginSignTime;
	}
    /**设置擅常学科    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理*/
	public void setSubjects(List<Integer> subjects){
		this.subjects=subjects;
	}
	/**获取擅常学科    1、语文   2、数学   3、英语   4、物理   5、化学   6、生物   7、政治   8、历史   9、地理*/
	public List<Integer> getSubjects(){
		return this.subjects;
	}
    /**设置个人简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取个人简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置用户Id*/
	public void setUserId(Integer userId){
		this.userId=userId;
	}
	/**获取用户Id*/
	public Integer getUserId(){
		return this.userId;
	}
    /**设置城市Id*/
	public void setRegionId(Integer regionId){
		this.regionId=regionId;
	}
	/**获取城市Id*/
	public Integer getRegionId(){
		return this.regionId;
	}


}
