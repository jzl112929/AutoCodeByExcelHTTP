package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【上架一对一/群组服务】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateServiceByTypeResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public CreateServiceByTypeResponseTO(){
        this.returnCode = 1;
    }



}
