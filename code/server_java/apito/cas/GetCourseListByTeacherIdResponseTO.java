package com.jzl.to.api.cas;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CourseBaseInfoTO;

/**
 * 【根据老师Id获取专题课程列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCourseListByTeacherIdResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程列表
    private List<CourseBaseInfoTO> courseList = new ArrayList<CourseBaseInfoTO>();

    public GetCourseListByTeacherIdResponseTO(){
        this.returnCode = 1;
    }

    /**设置课程列表*/
	public void setCourseList(List<CourseBaseInfoTO> courseList){
		this.courseList=courseList;
	}
	/**获取课程列表*/
	public List<CourseBaseInfoTO> getCourseList(){
		return this.courseList;
	}


}
