package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【注销登陆】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class LogoutResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public LogoutResponseTO(){
        this.returnCode = 1;
    }



}
