package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【登陆后修改密码】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UpdatePasswordLoginResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public UpdatePasswordLoginResponseTO(){
        this.returnCode = 1;
    }



}
