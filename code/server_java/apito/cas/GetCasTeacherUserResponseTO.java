package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.SpecialCourseInfoTO;

/**
 * 【获取老师用户信息】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCasTeacherUserResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//老师用户
	private SpecialCourseInfoTO casTeacher = new SpecialCourseInfoTO();

    public GetCasTeacherUserResponseTO(){
        this.returnCode = 1;
    }

    /**设置老师用户*/
	public void setCasTeacher(SpecialCourseInfoTO casTeacher){
		this.casTeacher=casTeacher;
	}
	/**获取老师用户*/
	public SpecialCourseInfoTO getCasTeacher(){
		return this.casTeacher;
	}


}
