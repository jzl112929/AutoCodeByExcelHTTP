package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.StudentInfoTO;

/**
 * 【获得学生详情】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetStudentInfoResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//学生详情
	private StudentInfoTO studentInfo = new StudentInfoTO();

    public GetStudentInfoResponseTO(){
        this.returnCode = 1;
    }

    /**设置学生详情*/
	public void setStudentInfo(StudentInfoTO studentInfo){
		this.studentInfo=studentInfo;
	}
	/**获取学生详情*/
	public StudentInfoTO getStudentInfo(){
		return this.studentInfo;
	}


}
