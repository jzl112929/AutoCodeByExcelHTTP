package com.jzl.to.api.cas;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CasUserTO;

/**
 * 【登录】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class LoginResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//用户信息
	private CasUserTO caseUser = new CasUserTO();

    public LoginResponseTO(){
        this.returnCode = 1;
    }

    /**设置用户信息*/
	public void setCaseUser(CasUserTO caseUser){
		this.caseUser=caseUser;
	}
	/**获取用户信息*/
	public CasUserTO getCaseUser(){
		return this.caseUser;
	}


}
