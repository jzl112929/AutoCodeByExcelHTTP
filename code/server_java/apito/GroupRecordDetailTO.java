package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【群组课详情】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GroupRecordDetailTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//课程时间
	private String beginTime;
	//课程时间
	private String endTime;
	//课程名称
	private String courseName;
	//图像地址
	private String pic;
	//老师姓名
	private String teacherName;
	//课程简介
	private String description;
	//老师简介
	private String profile;
	//服务Id
	private Integer tutorialId;
	//价格
	private Integer price;
	//是否已购买    1、未购买  2、已购买
	private Integer isPurchase;


    /**设置课程时间*/
	public void setBeginTime(String beginTime){
		this.beginTime=beginTime;
	}
	/**获取课程时间*/
	public String getBeginTime(){
		return this.beginTime;
	}
    /**设置课程时间*/
	public void setEndTime(String endTime){
		this.endTime=endTime;
	}
	/**获取课程时间*/
	public String getEndTime(){
		return this.endTime;
	}
    /**设置课程名称*/
	public void setCourseName(String courseName){
		this.courseName=courseName;
	}
	/**获取课程名称*/
	public String getCourseName(){
		return this.courseName;
	}
    /**设置图像地址*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取图像地址*/
	public String getPic(){
		return this.pic;
	}
    /**设置老师姓名*/
	public void setTeacherName(String teacherName){
		this.teacherName=teacherName;
	}
	/**获取老师姓名*/
	public String getTeacherName(){
		return this.teacherName;
	}
    /**设置课程简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取课程简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置老师简介*/
	public void setProfile(String profile){
		this.profile=profile;
	}
	/**获取老师简介*/
	public String getProfile(){
		return this.profile;
	}
    /**设置服务Id*/
	public void setTutorialId(Integer tutorialId){
		this.tutorialId=tutorialId;
	}
	/**获取服务Id*/
	public Integer getTutorialId(){
		return this.tutorialId;
	}
    /**设置价格*/
	public void setPrice(Integer price){
		this.price=price;
	}
	/**获取价格*/
	public Integer getPrice(){
		return this.price;
	}
    /**设置是否已购买    1、未购买  2、已购买*/
	public void setIsPurchase(Integer isPurchase){
		this.isPurchase=isPurchase;
	}
	/**获取是否已购买    1、未购买  2、已购买*/
	public Integer getIsPurchase(){
		return this.isPurchase;
	}


}
