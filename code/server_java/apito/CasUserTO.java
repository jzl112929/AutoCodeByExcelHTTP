package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【用户信息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CasUserTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//用户ID
	private Integer userId;
	//头像图像地址
	private String avatar;
	//学习豆
	private Long coin;
	//用户类型
	private Integer userType;
	//登录名
	private String userName;
	//年龄
	private Integer age;
	//电话
	private String phone;
	//姓名
	private String name;
	//昵称
	private String nickname;
	//用户IM号
	private String imuserId;


    /**设置用户ID*/
	public void setUserId(Integer userId){
		this.userId=userId;
	}
	/**获取用户ID*/
	public Integer getUserId(){
		return this.userId;
	}
    /**设置头像图像地址*/
	public void setAvatar(String avatar){
		this.avatar=avatar;
	}
	/**获取头像图像地址*/
	public String getAvatar(){
		return this.avatar;
	}
    /**设置学习豆*/
	public void setCoin(Long coin){
		this.coin=coin;
	}
	/**获取学习豆*/
	public Long getCoin(){
		return this.coin;
	}
    /**设置用户类型*/
	public void setUserType(Integer userType){
		this.userType=userType;
	}
	/**获取用户类型*/
	public Integer getUserType(){
		return this.userType;
	}
    /**设置登录名*/
	public void setUserName(String userName){
		this.userName=userName;
	}
	/**获取登录名*/
	public String getUserName(){
		return this.userName;
	}
    /**设置年龄*/
	public void setAge(Integer age){
		this.age=age;
	}
	/**获取年龄*/
	public Integer getAge(){
		return this.age;
	}
    /**设置电话*/
	public void setPhone(String phone){
		this.phone=phone;
	}
	/**获取电话*/
	public String getPhone(){
		return this.phone;
	}
    /**设置姓名*/
	public void setName(String name){
		this.name=name;
	}
	/**获取姓名*/
	public String getName(){
		return this.name;
	}
    /**设置昵称*/
	public void setNickname(String nickname){
		this.nickname=nickname;
	}
	/**获取昵称*/
	public String getNickname(){
		return this.nickname;
	}
    /**设置用户IM号*/
	public void setImuserId(String imuserId){
		this.imuserId=imuserId;
	}
	/**获取用户IM号*/
	public String getImuserId(){
		return this.imuserId;
	}


}
