package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【广告价目】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class AdvertBannerCostTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//价目Id
	private Integer id;
	//名称
	private String itemName;
	//价格
	private Integer price;


    /**设置价目Id*/
	public void setId(Integer id){
		this.id=id;
	}
	/**获取价目Id*/
	public Integer getId(){
		return this.id;
	}
    /**设置名称*/
	public void setItemName(String itemName){
		this.itemName=itemName;
	}
	/**获取名称*/
	public String getItemName(){
		return this.itemName;
	}
    /**设置价格*/
	public void setPrice(Integer price){
		this.price=price;
	}
	/**获取价格*/
	public Integer getPrice(){
		return this.price;
	}


}
