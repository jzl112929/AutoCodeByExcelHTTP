package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【课程基础信息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CourseBaseInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//课程ID
	private Integer courseId;
	//课程名称
	private String name;
	//图像地址
	private String pic;
	//课程价格
	private Integer price;
	//课程简介
	private String description;
	//开始时间
	private String startTime;
	//结束时间
	private String endTime;
	//课程类型    1、专题课  2、辅导课  3、试听课
	private Integer courseType;
	//总课时
	private Integer lessons;
	//老师昵称
	private String nickname;
	//课程地址
	private String courseUrl;
	//试听创建时间
	private String ctime;


    /**设置课程ID*/
	public void setCourseId(Integer courseId){
		this.courseId=courseId;
	}
	/**获取课程ID*/
	public Integer getCourseId(){
		return this.courseId;
	}
    /**设置课程名称*/
	public void setName(String name){
		this.name=name;
	}
	/**获取课程名称*/
	public String getName(){
		return this.name;
	}
    /**设置图像地址*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取图像地址*/
	public String getPic(){
		return this.pic;
	}
    /**设置课程价格*/
	public void setPrice(Integer price){
		this.price=price;
	}
	/**获取课程价格*/
	public Integer getPrice(){
		return this.price;
	}
    /**设置课程简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取课程简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置开始时间*/
	public void setStartTime(String startTime){
		this.startTime=startTime;
	}
	/**获取开始时间*/
	public String getStartTime(){
		return this.startTime;
	}
    /**设置结束时间*/
	public void setEndTime(String endTime){
		this.endTime=endTime;
	}
	/**获取结束时间*/
	public String getEndTime(){
		return this.endTime;
	}
    /**设置课程类型    1、专题课  2、辅导课  3、试听课*/
	public void setCourseType(Integer courseType){
		this.courseType=courseType;
	}
	/**获取课程类型    1、专题课  2、辅导课  3、试听课*/
	public Integer getCourseType(){
		return this.courseType;
	}
    /**设置总课时*/
	public void setLessons(Integer lessons){
		this.lessons=lessons;
	}
	/**获取总课时*/
	public Integer getLessons(){
		return this.lessons;
	}
    /**设置老师昵称*/
	public void setNickname(String nickname){
		this.nickname=nickname;
	}
	/**获取老师昵称*/
	public String getNickname(){
		return this.nickname;
	}
    /**设置课程地址*/
	public void setCourseUrl(String courseUrl){
		this.courseUrl=courseUrl;
	}
	/**获取课程地址*/
	public String getCourseUrl(){
		return this.courseUrl;
	}
    /**设置试听创建时间*/
	public void setCtime(String ctime){
		this.ctime=ctime;
	}
	/**获取试听创建时间*/
	public String getCtime(){
		return this.ctime;
	}


}
