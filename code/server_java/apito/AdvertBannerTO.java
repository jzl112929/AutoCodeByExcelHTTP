package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【广告Banner】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class AdvertBannerTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//广告Id
	private Integer id;
	//推广目标Id
	private Integer targetId;
	//图片
	private String pic;
	//目标跳转详情类型    1、专题课  2、平台广告
	private Integer targetType;


    /**设置广告Id*/
	public void setId(Integer id){
		this.id=id;
	}
	/**获取广告Id*/
	public Integer getId(){
		return this.id;
	}
    /**设置推广目标Id*/
	public void setTargetId(Integer targetId){
		this.targetId=targetId;
	}
	/**获取推广目标Id*/
	public Integer getTargetId(){
		return this.targetId;
	}
    /**设置图片*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取图片*/
	public String getPic(){
		return this.pic;
	}
    /**设置目标跳转详情类型    1、专题课  2、平台广告*/
	public void setTargetType(Integer targetType){
		this.targetType=targetType;
	}
	/**获取目标跳转详情类型    1、专题课  2、平台广告*/
	public Integer getTargetType(){
		return this.targetType;
	}


}
