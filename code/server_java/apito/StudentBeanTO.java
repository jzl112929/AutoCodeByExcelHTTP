package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【学习豆产品信息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class StudentBeanTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//学习豆ID
	private Integer beanId;
	//名称
	private String name;
	//价格
	private Integer price;


    /**设置学习豆ID*/
	public void setBeanId(Integer beanId){
		this.beanId=beanId;
	}
	/**获取学习豆ID*/
	public Integer getBeanId(){
		return this.beanId;
	}
    /**设置名称*/
	public void setName(String name){
		this.name=name;
	}
	/**获取名称*/
	public String getName(){
		return this.name;
	}
    /**设置价格*/
	public void setPrice(Integer price){
		this.price=price;
	}
	/**获取价格*/
	public Integer getPrice(){
		return this.price;
	}


}
