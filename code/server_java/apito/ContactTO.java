package com.jzl.to.api;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【联系人】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ContactTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//一对一列表
	private List<MemberTO> memberList = new ArrayList<MemberTO>();
	//群组列表
	private List<GroupBaseTO> groupList = new ArrayList<GroupBaseTO>();


    /**设置一对一列表*/
	public void setMemberList(List<MemberTO> memberList){
		this.memberList=memberList;
	}
	/**获取一对一列表*/
	public List<MemberTO> getMemberList(){
		return this.memberList;
	}
    /**设置群组列表*/
	public void setGroupList(List<GroupBaseTO> groupList){
		this.groupList=groupList;
	}
	/**获取群组列表*/
	public List<GroupBaseTO> getGroupList(){
		return this.groupList;
	}


}
