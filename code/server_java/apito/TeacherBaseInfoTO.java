package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【老师详情信息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class TeacherBaseInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//姓名
	private String name;
	//图像地址
	private String avatar;
	//老师Id
	private Integer teacherId;
	//购买人数
	private Integer purchaseNum;
	//教龄
	private Double expricence;
	//评论数
	private Integer commentNum;
	//价格
	private Integer price;
	//昵称
	private String nickname;
	//辅导课Id
	private Integer tutorialId;
	//服务周期
	private Integer days;
	//学校等级
	private Integer schoolType;
	//学科类别
	private Integer subjectType;
	//是否已购买    1、未购买  2、已购买
	private Integer isPurchase;
	//辅导类型    1、一对一  2、群组
	private Integer tutorialType;
	//用户IM号
	private String imuserId;


    /**设置姓名*/
	public void setName(String name){
		this.name=name;
	}
	/**获取姓名*/
	public String getName(){
		return this.name;
	}
    /**设置图像地址*/
	public void setAvatar(String avatar){
		this.avatar=avatar;
	}
	/**获取图像地址*/
	public String getAvatar(){
		return this.avatar;
	}
    /**设置老师Id*/
	public void setTeacherId(Integer teacherId){
		this.teacherId=teacherId;
	}
	/**获取老师Id*/
	public Integer getTeacherId(){
		return this.teacherId;
	}
    /**设置购买人数*/
	public void setPurchaseNum(Integer purchaseNum){
		this.purchaseNum=purchaseNum;
	}
	/**获取购买人数*/
	public Integer getPurchaseNum(){
		return this.purchaseNum;
	}
    /**设置教龄*/
	public void setExpricence(Double expricence){
		this.expricence=expricence;
	}
	/**获取教龄*/
	public Double getExpricence(){
		return this.expricence;
	}
    /**设置评论数*/
	public void setCommentNum(Integer commentNum){
		this.commentNum=commentNum;
	}
	/**获取评论数*/
	public Integer getCommentNum(){
		return this.commentNum;
	}
    /**设置价格*/
	public void setPrice(Integer price){
		this.price=price;
	}
	/**获取价格*/
	public Integer getPrice(){
		return this.price;
	}
    /**设置昵称*/
	public void setNickname(String nickname){
		this.nickname=nickname;
	}
	/**获取昵称*/
	public String getNickname(){
		return this.nickname;
	}
    /**设置辅导课Id*/
	public void setTutorialId(Integer tutorialId){
		this.tutorialId=tutorialId;
	}
	/**获取辅导课Id*/
	public Integer getTutorialId(){
		return this.tutorialId;
	}
    /**设置服务周期*/
	public void setDays(Integer days){
		this.days=days;
	}
	/**获取服务周期*/
	public Integer getDays(){
		return this.days;
	}
    /**设置学校等级*/
	public void setSchoolType(Integer schoolType){
		this.schoolType=schoolType;
	}
	/**获取学校等级*/
	public Integer getSchoolType(){
		return this.schoolType;
	}
    /**设置学科类别*/
	public void setSubjectType(Integer subjectType){
		this.subjectType=subjectType;
	}
	/**获取学科类别*/
	public Integer getSubjectType(){
		return this.subjectType;
	}
    /**设置是否已购买    1、未购买  2、已购买*/
	public void setIsPurchase(Integer isPurchase){
		this.isPurchase=isPurchase;
	}
	/**获取是否已购买    1、未购买  2、已购买*/
	public Integer getIsPurchase(){
		return this.isPurchase;
	}
    /**设置辅导类型    1、一对一  2、群组*/
	public void setTutorialType(Integer tutorialType){
		this.tutorialType=tutorialType;
	}
	/**获取辅导类型    1、一对一  2、群组*/
	public Integer getTutorialType(){
		return this.tutorialType;
	}
    /**设置用户IM号*/
	public void setImuserId(String imuserId){
		this.imuserId=imuserId;
	}
	/**获取用户IM号*/
	public String getImuserId(){
		return this.imuserId;
	}


}
