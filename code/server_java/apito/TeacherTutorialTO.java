package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【服务基础】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class TeacherTutorialTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//服务
	private Integer tutorialId;
	//价格
	private String price;
	//年级学科
	private String gradeSubjectName;
	//月数
	private String months;
	//状态
	private String status;


    /**设置服务*/
	public void setTutorialId(Integer tutorialId){
		this.tutorialId=tutorialId;
	}
	/**获取服务*/
	public Integer getTutorialId(){
		return this.tutorialId;
	}
    /**设置价格*/
	public void setPrice(String price){
		this.price=price;
	}
	/**获取价格*/
	public String getPrice(){
		return this.price;
	}
    /**设置年级学科*/
	public void setGradeSubjectName(String gradeSubjectName){
		this.gradeSubjectName=gradeSubjectName;
	}
	/**获取年级学科*/
	public String getGradeSubjectName(){
		return this.gradeSubjectName;
	}
    /**设置月数*/
	public void setMonths(String months){
		this.months=months;
	}
	/**获取月数*/
	public String getMonths(){
		return this.months;
	}
    /**设置状态*/
	public void setStatus(String status){
		this.status=status;
	}
	/**获取状态*/
	public String getStatus(){
		return this.status;
	}


}
