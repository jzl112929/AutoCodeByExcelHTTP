package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【状元信息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ChampionTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//状元Id
	private Integer championId;
	//状元名称
	private String championName;
	//总课程时长
	private Integer sumLessionTime;
	//头像
	private String avatar;
	//星星数
	private Integer stars;
	//上课次数
	private Integer sumDone;
	//开课次数
	private Integer sumCreate;


    /**设置状元Id*/
	public void setChampionId(Integer championId){
		this.championId=championId;
	}
	/**获取状元Id*/
	public Integer getChampionId(){
		return this.championId;
	}
    /**设置状元名称*/
	public void setChampionName(String championName){
		this.championName=championName;
	}
	/**获取状元名称*/
	public String getChampionName(){
		return this.championName;
	}
    /**设置总课程时长*/
	public void setSumLessionTime(Integer sumLessionTime){
		this.sumLessionTime=sumLessionTime;
	}
	/**获取总课程时长*/
	public Integer getSumLessionTime(){
		return this.sumLessionTime;
	}
    /**设置头像*/
	public void setAvatar(String avatar){
		this.avatar=avatar;
	}
	/**获取头像*/
	public String getAvatar(){
		return this.avatar;
	}
    /**设置星星数*/
	public void setStars(Integer stars){
		this.stars=stars;
	}
	/**获取星星数*/
	public Integer getStars(){
		return this.stars;
	}
    /**设置上课次数*/
	public void setSumDone(Integer sumDone){
		this.sumDone=sumDone;
	}
	/**获取上课次数*/
	public Integer getSumDone(){
		return this.sumDone;
	}
    /**设置开课次数*/
	public void setSumCreate(Integer sumCreate){
		this.sumCreate=sumCreate;
	}
	/**获取开课次数*/
	public Integer getSumCreate(){
		return this.sumCreate;
	}


}
