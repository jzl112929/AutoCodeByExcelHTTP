package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【老师详情信息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class TeacherInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//姓名
	private String name;
	//图像地址
	private String avatar;
	//性别
	private Integer sex;
	//手机号码
	private String mobile;
	//教龄
	private Integer expricence;
	//个人简介
	private String description;
	//是否通过教师证认证    0、未通过  1、通过
	private Integer isVertifyCertificate;
	//是否通过身份证认证    张浩:  0、未通过  1、通过
	private Integer isVertifyIDCard;
	//昵称
	private String nickname;
	//擅长学科
	private String subjects;
	//是否已购买    0、未购买  1、已购买
	private Integer isPurchase;
	//辅导课Id
	private Integer tutorialId;
	//是否已收藏    0、未收藏  1、已收藏
	private Integer isFavorite;
	//是否已评论    0、未评论  1、已评论
	private Integer isComment;
	//辅导类型    1、一对一  2、群组
	private Integer tutorialType;
	//用户IM号
	private String imuserId;


    /**设置姓名*/
	public void setName(String name){
		this.name=name;
	}
	/**获取姓名*/
	public String getName(){
		return this.name;
	}
    /**设置图像地址*/
	public void setAvatar(String avatar){
		this.avatar=avatar;
	}
	/**获取图像地址*/
	public String getAvatar(){
		return this.avatar;
	}
    /**设置性别*/
	public void setSex(Integer sex){
		this.sex=sex;
	}
	/**获取性别*/
	public Integer getSex(){
		return this.sex;
	}
    /**设置手机号码*/
	public void setMobile(String mobile){
		this.mobile=mobile;
	}
	/**获取手机号码*/
	public String getMobile(){
		return this.mobile;
	}
    /**设置教龄*/
	public void setExpricence(Integer expricence){
		this.expricence=expricence;
	}
	/**获取教龄*/
	public Integer getExpricence(){
		return this.expricence;
	}
    /**设置个人简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取个人简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置是否通过教师证认证    0、未通过  1、通过*/
	public void setIsVertifyCertificate(Integer isVertifyCertificate){
		this.isVertifyCertificate=isVertifyCertificate;
	}
	/**获取是否通过教师证认证    0、未通过  1、通过*/
	public Integer getIsVertifyCertificate(){
		return this.isVertifyCertificate;
	}
    /**设置是否通过身份证认证    张浩:  0、未通过  1、通过*/
	public void setIsVertifyIDCard(Integer isVertifyIDCard){
		this.isVertifyIDCard=isVertifyIDCard;
	}
	/**获取是否通过身份证认证    张浩:  0、未通过  1、通过*/
	public Integer getIsVertifyIDCard(){
		return this.isVertifyIDCard;
	}
    /**设置昵称*/
	public void setNickname(String nickname){
		this.nickname=nickname;
	}
	/**获取昵称*/
	public String getNickname(){
		return this.nickname;
	}
    /**设置擅长学科*/
	public void setSubjects(String subjects){
		this.subjects=subjects;
	}
	/**获取擅长学科*/
	public String getSubjects(){
		return this.subjects;
	}
    /**设置是否已购买    0、未购买  1、已购买*/
	public void setIsPurchase(Integer isPurchase){
		this.isPurchase=isPurchase;
	}
	/**获取是否已购买    0、未购买  1、已购买*/
	public Integer getIsPurchase(){
		return this.isPurchase;
	}
    /**设置辅导课Id*/
	public void setTutorialId(Integer tutorialId){
		this.tutorialId=tutorialId;
	}
	/**获取辅导课Id*/
	public Integer getTutorialId(){
		return this.tutorialId;
	}
    /**设置是否已收藏    0、未收藏  1、已收藏*/
	public void setIsFavorite(Integer isFavorite){
		this.isFavorite=isFavorite;
	}
	/**获取是否已收藏    0、未收藏  1、已收藏*/
	public Integer getIsFavorite(){
		return this.isFavorite;
	}
    /**设置是否已评论    0、未评论  1、已评论*/
	public void setIsComment(Integer isComment){
		this.isComment=isComment;
	}
	/**获取是否已评论    0、未评论  1、已评论*/
	public Integer getIsComment(){
		return this.isComment;
	}
    /**设置辅导类型    1、一对一  2、群组*/
	public void setTutorialType(Integer tutorialType){
		this.tutorialType=tutorialType;
	}
	/**获取辅导类型    1、一对一  2、群组*/
	public Integer getTutorialType(){
		return this.tutorialType;
	}
    /**设置用户IM号*/
	public void setImuserId(String imuserId){
		this.imuserId=imuserId;
	}
	/**获取用户IM号*/
	public String getImuserId(){
		return this.imuserId;
	}


}
