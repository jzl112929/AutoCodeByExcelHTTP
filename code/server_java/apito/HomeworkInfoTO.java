package com.jzl.to.api;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【作业信息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class HomeworkInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//作业Id
	private Integer homeworkId;
	//作业内容
	private String content;
	//图像地址
	private String pic;
	//创建时间
	private String ctime;
	//修改时间
	private String mtime;
	//教师简称
	private String teacherName;
	//已完成作业学生列表
	private List<HomeworkBaseTO> doneList = new ArrayList<HomeworkBaseTO>();
	//未完成作业列表
	private List<HomeworkBaseTO> undoList = new ArrayList<HomeworkBaseTO>();
	//作业文件url
    private List<String> fileUrls = new ArrayList<String>();
	//昵称
	private String nickname;
	//科目
	private Integer subjectId;


    /**设置作业Id*/
	public void setHomeworkId(Integer homeworkId){
		this.homeworkId=homeworkId;
	}
	/**获取作业Id*/
	public Integer getHomeworkId(){
		return this.homeworkId;
	}
    /**设置作业内容*/
	public void setContent(String content){
		this.content=content;
	}
	/**获取作业内容*/
	public String getContent(){
		return this.content;
	}
    /**设置图像地址*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取图像地址*/
	public String getPic(){
		return this.pic;
	}
    /**设置创建时间*/
	public void setCtime(String ctime){
		this.ctime=ctime;
	}
	/**获取创建时间*/
	public String getCtime(){
		return this.ctime;
	}
    /**设置修改时间*/
	public void setMtime(String mtime){
		this.mtime=mtime;
	}
	/**获取修改时间*/
	public String getMtime(){
		return this.mtime;
	}
    /**设置教师简称*/
	public void setTeacherName(String teacherName){
		this.teacherName=teacherName;
	}
	/**获取教师简称*/
	public String getTeacherName(){
		return this.teacherName;
	}
    /**设置已完成作业学生列表*/
	public void setDoneList(List<HomeworkBaseTO> doneList){
		this.doneList=doneList;
	}
	/**获取已完成作业学生列表*/
	public List<HomeworkBaseTO> getDoneList(){
		return this.doneList;
	}
    /**设置未完成作业列表*/
	public void setUndoList(List<HomeworkBaseTO> undoList){
		this.undoList=undoList;
	}
	/**获取未完成作业列表*/
	public List<HomeworkBaseTO> getUndoList(){
		return this.undoList;
	}
    /**设置作业文件url*/
	public void setFileUrls(List<String> fileUrls){
		this.fileUrls=fileUrls;
	}
	/**获取作业文件url*/
	public List<String> getFileUrls(){
		return this.fileUrls;
	}
    /**设置昵称*/
	public void setNickname(String nickname){
		this.nickname=nickname;
	}
	/**获取昵称*/
	public String getNickname(){
		return this.nickname;
	}
    /**设置科目*/
	public void setSubjectId(Integer subjectId){
		this.subjectId=subjectId;
	}
	/**获取科目*/
	public Integer getSubjectId(){
		return this.subjectId;
	}


}
