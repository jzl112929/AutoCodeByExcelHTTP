package com.jzl.to.api.live;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【注销直播间】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class UnregisterRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//直播间记录Id
	private Integer id;


    /**设置直播间记录Id*/
	public void setId(Integer id){
		this.id=id;
	}
	/**获取直播间记录Id*/
	public Integer getId(){
		return this.id;
	}


}
