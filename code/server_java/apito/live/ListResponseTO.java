package com.jzl.to.api.live;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.LiveRoomTO;

/**
 * 【查询直播间列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//直播间用户
    private List<LiveRoomTO> infoList = new ArrayList<LiveRoomTO>();

    public ListResponseTO(){
        this.returnCode = 1;
    }

    /**设置直播间用户*/
	public void setInfoList(List<LiveRoomTO> infoList){
		this.infoList=infoList;
	}
	/**获取直播间用户*/
	public List<LiveRoomTO> getInfoList(){
		return this.infoList;
	}


}
