package com.jzl.to.api.live;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.LiveRoomTO;

/**
 * 【进入直播课堂】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class EnterLiveCourseResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//直播间用户
	private LiveRoomTO info = new LiveRoomTO();

    public EnterLiveCourseResponseTO(){
        this.returnCode = 1;
    }

    /**设置直播间用户*/
	public void setInfo(LiveRoomTO info){
		this.info=info;
	}
	/**获取直播间用户*/
	public LiveRoomTO getInfo(){
		return this.info;
	}


}
