package com.jzl.to.api.live;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.LiveRoomTO;

/**
 * 【注册直播间】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class RegisterResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//直播间用户
	private LiveRoomTO info = new LiveRoomTO();

    public RegisterResponseTO(){
        this.returnCode = 1;
    }

    /**设置直播间用户*/
	public void setInfo(LiveRoomTO info){
		this.info=info;
	}
	/**获取直播间用户*/
	public LiveRoomTO getInfo(){
		return this.info;
	}


}
