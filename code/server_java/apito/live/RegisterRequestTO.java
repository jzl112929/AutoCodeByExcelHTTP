package com.jzl.to.api.live;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【注册直播间】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class RegisterRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//IM群的ID
	private String chatRoomId;
	//课堂的ID
	private Integer courseDetailId;
	//课堂类型    1、专题课  2、辅导课
	private Integer courseType;


    /**设置IM群的ID*/
	public void setChatRoomId(String chatRoomId){
		this.chatRoomId=chatRoomId;
	}
	/**获取IM群的ID*/
	public String getChatRoomId(){
		return this.chatRoomId;
	}
    /**设置课堂的ID*/
	public void setCourseDetailId(Integer courseDetailId){
		this.courseDetailId=courseDetailId;
	}
	/**获取课堂的ID*/
	public Integer getCourseDetailId(){
		return this.courseDetailId;
	}
    /**设置课堂类型    1、专题课  2、辅导课*/
	public void setCourseType(Integer courseType){
		this.courseType=courseType;
	}
	/**获取课堂类型    1、专题课  2、辅导课*/
	public Integer getCourseType(){
		return this.courseType;
	}


}
