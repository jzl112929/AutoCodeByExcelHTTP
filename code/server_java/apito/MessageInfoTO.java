package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【消息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class MessageInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//消息Id
	private Integer id;
	//标题
	private String title;
	//消息类型    1:专题课程购买通知  2:专题课程开课通知  3:作业提醒  4:买老师通知   5:老师开课通知
	private Integer type;
	//内容
	private String content;
	//创建时间
	private String ctime;
	//目标名称
	private String targetName;
	//目标Id
	private Integer targetId;
	//讲师昵称
	private String nickname;


    /**设置消息Id*/
	public void setId(Integer id){
		this.id=id;
	}
	/**获取消息Id*/
	public Integer getId(){
		return this.id;
	}
    /**设置标题*/
	public void setTitle(String title){
		this.title=title;
	}
	/**获取标题*/
	public String getTitle(){
		return this.title;
	}
    /**设置消息类型    1:专题课程购买通知  2:专题课程开课通知  3:作业提醒  4:买老师通知   5:老师开课通知*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取消息类型    1:专题课程购买通知  2:专题课程开课通知  3:作业提醒  4:买老师通知   5:老师开课通知*/
	public Integer getType(){
		return this.type;
	}
    /**设置内容*/
	public void setContent(String content){
		this.content=content;
	}
	/**获取内容*/
	public String getContent(){
		return this.content;
	}
    /**设置创建时间*/
	public void setCtime(String ctime){
		this.ctime=ctime;
	}
	/**获取创建时间*/
	public String getCtime(){
		return this.ctime;
	}
    /**设置目标名称*/
	public void setTargetName(String targetName){
		this.targetName=targetName;
	}
	/**获取目标名称*/
	public String getTargetName(){
		return this.targetName;
	}
    /**设置目标Id*/
	public void setTargetId(Integer targetId){
		this.targetId=targetId;
	}
	/**获取目标Id*/
	public Integer getTargetId(){
		return this.targetId;
	}
    /**设置讲师昵称*/
	public void setNickname(String nickname){
		this.nickname=nickname;
	}
	/**获取讲师昵称*/
	public String getNickname(){
		return this.nickname;
	}


}
