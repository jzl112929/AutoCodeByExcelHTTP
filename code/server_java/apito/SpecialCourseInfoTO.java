package com.jzl.to.api;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【专题课程信息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class SpecialCourseInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//课程ID
	private Integer courseId;
	//课程名称
	private String courseName;
	//课程价格
	private Integer price;
	//课程简介
	private String description;
	//课程开始时间
	private String startTime;
	//课程结束时间
	private String endTime;
	//课程安排
	private List<CourseScheduleTO> courseScheduleList = new ArrayList<CourseScheduleTO>();
	//老师简介
	private String teacherDescription;
	//昵称
	private String nickname;
	//图片
	private String pic;
	//服务器当前时间
	private String currentTime;
	//是否购买过    0、未购买  1、已购买
	private Integer isPurchase;


    /**设置课程ID*/
	public void setCourseId(Integer courseId){
		this.courseId=courseId;
	}
	/**获取课程ID*/
	public Integer getCourseId(){
		return this.courseId;
	}
    /**设置课程名称*/
	public void setCourseName(String courseName){
		this.courseName=courseName;
	}
	/**获取课程名称*/
	public String getCourseName(){
		return this.courseName;
	}
    /**设置课程价格*/
	public void setPrice(Integer price){
		this.price=price;
	}
	/**获取课程价格*/
	public Integer getPrice(){
		return this.price;
	}
    /**设置课程简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取课程简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置课程开始时间*/
	public void setStartTime(String startTime){
		this.startTime=startTime;
	}
	/**获取课程开始时间*/
	public String getStartTime(){
		return this.startTime;
	}
    /**设置课程结束时间*/
	public void setEndTime(String endTime){
		this.endTime=endTime;
	}
	/**获取课程结束时间*/
	public String getEndTime(){
		return this.endTime;
	}
    /**设置课程安排*/
	public void setCourseScheduleList(List<CourseScheduleTO> courseScheduleList){
		this.courseScheduleList=courseScheduleList;
	}
	/**获取课程安排*/
	public List<CourseScheduleTO> getCourseScheduleList(){
		return this.courseScheduleList;
	}
    /**设置老师简介*/
	public void setTeacherDescription(String teacherDescription){
		this.teacherDescription=teacherDescription;
	}
	/**获取老师简介*/
	public String getTeacherDescription(){
		return this.teacherDescription;
	}
    /**设置昵称*/
	public void setNickname(String nickname){
		this.nickname=nickname;
	}
	/**获取昵称*/
	public String getNickname(){
		return this.nickname;
	}
    /**设置图片*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取图片*/
	public String getPic(){
		return this.pic;
	}
    /**设置服务器当前时间*/
	public void setCurrentTime(String currentTime){
		this.currentTime=currentTime;
	}
	/**获取服务器当前时间*/
	public String getCurrentTime(){
		return this.currentTime;
	}
    /**设置是否购买过    0、未购买  1、已购买*/
	public void setIsPurchase(Integer isPurchase){
		this.isPurchase=isPurchase;
	}
	/**获取是否购买过    0、未购买  1、已购买*/
	public Integer getIsPurchase(){
		return this.isPurchase;
	}


}
