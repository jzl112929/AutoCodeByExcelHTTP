package com.jzl.to.api.courseware;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【下载课件】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class DownloadCoursewareRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课件Id
	private Integer coursewareId;
	//下载密码
	private String password;


    /**设置课件Id*/
	public void setCoursewareId(Integer coursewareId){
		this.coursewareId=coursewareId;
	}
	/**获取课件Id*/
	public Integer getCoursewareId(){
		return this.coursewareId;
	}
    /**设置下载密码*/
	public void setPassword(String password){
		this.password=password;
	}
	/**获取下载密码*/
	public String getPassword(){
		return this.password;
	}


}
