package com.jzl.to.api.courseware;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获取课件列表】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCoursewareListRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//分页页码
	private Integer pageIndex;
	//群组Id
	private Integer groupId;
	//搜索关键词
	private String searchStr;


    /**设置分页页码*/
	public void setPageIndex(Integer pageIndex){
		this.pageIndex=pageIndex;
	}
	/**获取分页页码*/
	public Integer getPageIndex(){
		return this.pageIndex;
	}
    /**设置群组Id*/
	public void setGroupId(Integer groupId){
		this.groupId=groupId;
	}
	/**获取群组Id*/
	public Integer getGroupId(){
		return this.groupId;
	}
    /**设置搜索关键词*/
	public void setSearchStr(String searchStr){
		this.searchStr=searchStr;
	}
	/**获取搜索关键词*/
	public String getSearchStr(){
		return this.searchStr;
	}


}
