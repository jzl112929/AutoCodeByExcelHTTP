package com.jzl.to.api.courseware;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【创建课件】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateCoursewareResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//下载地址
	private String filePath;

    public CreateCoursewareResponseTO(){
        this.returnCode = 1;
    }

    /**设置下载地址*/
	public void setFilePath(String filePath){
		this.filePath=filePath;
	}
	/**获取下载地址*/
	public String getFilePath(){
		return this.filePath;
	}


}
