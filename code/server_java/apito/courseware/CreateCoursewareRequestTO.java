package com.jzl.to.api.courseware;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【创建课件】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CreateCoursewareRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课件附件
    private List<Object[]TO> files = new ArrayList<Object[]TO>();
	//课件名称
	private String name;
	//简介
	private String description;
	//课件密码
	private String password;


    /**设置课件附件*/
	public void setFiles(List<Object[]TO> files){
		this.files=files;
	}
	/**获取课件附件*/
	public List<Object[]TO> getFiles(){
		return this.files;
	}
    /**设置课件名称*/
	public void setName(String name){
		this.name=name;
	}
	/**获取课件名称*/
	public String getName(){
		return this.name;
	}
    /**设置简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置课件密码*/
	public void setPassword(String password){
		this.password=password;
	}
	/**获取课件密码*/
	public String getPassword(){
		return this.password;
	}


}
