package com.jzl.to.api.courseware;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【下载课件】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class DownloadCoursewareResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public DownloadCoursewareResponseTO(){
        this.returnCode = 1;
    }



}
