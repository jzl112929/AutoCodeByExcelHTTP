package com.jzl.to.api.courseware;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.CoursewareBaseTO;

/**
 * 【获取课件列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetCoursewareListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课件列表
    private List<CoursewareBaseTO> coursewareList = new ArrayList<CoursewareBaseTO>();

    public GetCoursewareListResponseTO(){
        this.returnCode = 1;
    }

    /**设置课件列表*/
	public void setCoursewareList(List<CoursewareBaseTO> coursewareList){
		this.coursewareList=coursewareList;
	}
	/**获取课件列表*/
	public List<CoursewareBaseTO> getCoursewareList(){
		return this.coursewareList;
	}


}
