package com.jzl.to.api;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【作 业】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class HomeworkBaseTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//学生作业Id
	private Integer studentWorkId;
	//作业标题
	private String title;
	//作业内容
	private String content;
	//图像地址
	private String pic;
	//创建时间
	private String ctime;
	//修改时间
	private String mtime;
	//作业文件url
    private List<String> fileUrls = new ArrayList<String>();
	//科目
	private Integer subjectId;
	//作业状态    0、未发布  1、已发布  
	private Integer status;
	//老师作业Id
	private Integer teacherWorkId;
	//昵称
	private String nickname;


    /**设置学生作业Id*/
	public void setStudentWorkId(Integer studentWorkId){
		this.studentWorkId=studentWorkId;
	}
	/**获取学生作业Id*/
	public Integer getStudentWorkId(){
		return this.studentWorkId;
	}
    /**设置作业标题*/
	public void setTitle(String title){
		this.title=title;
	}
	/**获取作业标题*/
	public String getTitle(){
		return this.title;
	}
    /**设置作业内容*/
	public void setContent(String content){
		this.content=content;
	}
	/**获取作业内容*/
	public String getContent(){
		return this.content;
	}
    /**设置图像地址*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取图像地址*/
	public String getPic(){
		return this.pic;
	}
    /**设置创建时间*/
	public void setCtime(String ctime){
		this.ctime=ctime;
	}
	/**获取创建时间*/
	public String getCtime(){
		return this.ctime;
	}
    /**设置修改时间*/
	public void setMtime(String mtime){
		this.mtime=mtime;
	}
	/**获取修改时间*/
	public String getMtime(){
		return this.mtime;
	}
    /**设置作业文件url*/
	public void setFileUrls(List<String> fileUrls){
		this.fileUrls=fileUrls;
	}
	/**获取作业文件url*/
	public List<String> getFileUrls(){
		return this.fileUrls;
	}
    /**设置科目*/
	public void setSubjectId(Integer subjectId){
		this.subjectId=subjectId;
	}
	/**获取科目*/
	public Integer getSubjectId(){
		return this.subjectId;
	}
    /**设置作业状态    0、未发布  1、已发布  */
	public void setStatus(Integer status){
		this.status=status;
	}
	/**获取作业状态    0、未发布  1、已发布  */
	public Integer getStatus(){
		return this.status;
	}
    /**设置老师作业Id*/
	public void setTeacherWorkId(Integer teacherWorkId){
		this.teacherWorkId=teacherWorkId;
	}
	/**获取老师作业Id*/
	public Integer getTeacherWorkId(){
		return this.teacherWorkId;
	}
    /**设置昵称*/
	public void setNickname(String nickname){
		this.nickname=nickname;
	}
	/**获取昵称*/
	public String getNickname(){
		return this.nickname;
	}


}
