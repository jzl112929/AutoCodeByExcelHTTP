package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【一对一】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class StudentTutorialTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//名称
	private String studentName;
	//学生Id
	private Integer studentId;
	//辅导课Id
	private Integer tutorialId;
	//学校级别
	private Integer schoolType;
	//科目类别
	private Integer subjectType;
	//年级学科
	private String gradeSubjectName;


    /**设置名称*/
	public void setStudentName(String studentName){
		this.studentName=studentName;
	}
	/**获取名称*/
	public String getStudentName(){
		return this.studentName;
	}
    /**设置学生Id*/
	public void setStudentId(Integer studentId){
		this.studentId=studentId;
	}
	/**获取学生Id*/
	public Integer getStudentId(){
		return this.studentId;
	}
    /**设置辅导课Id*/
	public void setTutorialId(Integer tutorialId){
		this.tutorialId=tutorialId;
	}
	/**获取辅导课Id*/
	public Integer getTutorialId(){
		return this.tutorialId;
	}
    /**设置学校级别*/
	public void setSchoolType(Integer schoolType){
		this.schoolType=schoolType;
	}
	/**获取学校级别*/
	public Integer getSchoolType(){
		return this.schoolType;
	}
    /**设置科目类别*/
	public void setSubjectType(Integer subjectType){
		this.subjectType=subjectType;
	}
	/**获取科目类别*/
	public Integer getSubjectType(){
		return this.subjectType;
	}
    /**设置年级学科*/
	public void setGradeSubjectName(String gradeSubjectName){
		this.gradeSubjectName=gradeSubjectName;
	}
	/**获取年级学科*/
	public String getGradeSubjectName(){
		return this.gradeSubjectName;
	}


}
