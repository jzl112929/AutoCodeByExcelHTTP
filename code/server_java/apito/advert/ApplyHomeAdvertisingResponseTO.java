package com.jzl.to.api.advert;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【申请首页推广】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ApplyHomeAdvertisingResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;


    public ApplyHomeAdvertisingResponseTO(){
        this.returnCode = 1;
    }



}
