package com.jzl.to.api.advert;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

/**
 * 【获得首页banner轮播图】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetAdvertBannerListRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//app应用类型    app类型  1、学生端  2、老师端  3、web端
	private Integer appType;


    /**设置app应用类型    app类型  1、学生端  2、老师端  3、web端*/
	public void setAppType(Integer appType){
		this.appType=appType;
	}
	/**获取app应用类型    app类型  1、学生端  2、老师端  3、web端*/
	public Integer getAppType(){
		return this.appType;
	}


}
