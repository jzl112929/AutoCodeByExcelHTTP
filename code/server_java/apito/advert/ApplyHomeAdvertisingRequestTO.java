package com.jzl.to.api.advert;

import java.util.ArrayList;
import java.util.List;

import java.util.Date;

import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.Object[]TO;

/**
 * 【申请首页推广】Request
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class ApplyHomeAdvertisingRequestTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//课程ID
	private Integer courseId;
	//推广开始时间
	private Date advertBeginTime;
	//推广结束
	private Date advertEndTime;
	//推广类型
	private Integer advertCostId;
	//支付金额
	private Integer payAmount;
	//链接详情类型    1、专题课  2、平台广告
	private Integer linkType;
	//广告应用类型    1、学生端  2、教师端  3、web端
	private Integer appType;
	//广告图片
    private List<Object[]TO> files = new ArrayList<Object[]TO>();


    /**设置课程ID*/
	public void setCourseId(Integer courseId){
		this.courseId=courseId;
	}
	/**获取课程ID*/
	public Integer getCourseId(){
		return this.courseId;
	}
    /**设置推广开始时间*/
	public void setAdvertBeginTime(Date advertBeginTime){
		this.advertBeginTime=advertBeginTime;
	}
	/**获取推广开始时间*/
	public Date getAdvertBeginTime(){
		return this.advertBeginTime;
	}
    /**设置推广结束*/
	public void setAdvertEndTime(Date advertEndTime){
		this.advertEndTime=advertEndTime;
	}
	/**获取推广结束*/
	public Date getAdvertEndTime(){
		return this.advertEndTime;
	}
    /**设置推广类型*/
	public void setAdvertCostId(Integer advertCostId){
		this.advertCostId=advertCostId;
	}
	/**获取推广类型*/
	public Integer getAdvertCostId(){
		return this.advertCostId;
	}
    /**设置支付金额*/
	public void setPayAmount(Integer payAmount){
		this.payAmount=payAmount;
	}
	/**获取支付金额*/
	public Integer getPayAmount(){
		return this.payAmount;
	}
    /**设置链接详情类型    1、专题课  2、平台广告*/
	public void setLinkType(Integer linkType){
		this.linkType=linkType;
	}
	/**获取链接详情类型    1、专题课  2、平台广告*/
	public Integer getLinkType(){
		return this.linkType;
	}
    /**设置广告应用类型    1、学生端  2、教师端  3、web端*/
	public void setAppType(Integer appType){
		this.appType=appType;
	}
	/**获取广告应用类型    1、学生端  2、教师端  3、web端*/
	public Integer getAppType(){
		return this.appType;
	}
    /**设置广告图片*/
	public void setFiles(List<Object[]TO> files){
		this.files=files;
	}
	/**获取广告图片*/
	public List<Object[]TO> getFiles(){
		return this.files;
	}


}
