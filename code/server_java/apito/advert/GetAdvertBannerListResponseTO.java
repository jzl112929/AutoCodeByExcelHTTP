package com.jzl.to.api.advert;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.AdvertBannerTO;

/**
 * 【获得首页banner轮播图】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetAdvertBannerListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//广告列表
    private List<AdvertBannerTO> bannerList = new ArrayList<AdvertBannerTO>();

    public GetAdvertBannerListResponseTO(){
        this.returnCode = 1;
    }

    /**设置广告列表*/
	public void setBannerList(List<AdvertBannerTO> bannerList){
		this.bannerList=bannerList;
	}
	/**获取广告列表*/
	public List<AdvertBannerTO> getBannerList(){
		return this.bannerList;
	}


}
