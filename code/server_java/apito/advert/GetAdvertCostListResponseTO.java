package com.jzl.to.api.advert;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.jzl.to.api.AdvertBannerCostTO;

/**
 * 【获取广告推广类型列表】Response
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GetAdvertCostListResponseTO extends AbstractObject {

	private static final long serialVersionUID = 1L;

	//广告价目表
    private List<AdvertBannerCostTO> advertCostList = new ArrayList<AdvertBannerCostTO>();

    public GetAdvertCostListResponseTO(){
        this.returnCode = 1;
    }

    /**设置广告价目表*/
	public void setAdvertCostList(List<AdvertBannerCostTO> advertCostList){
		this.advertCostList=advertCostList;
	}
	/**获取广告价目表*/
	public List<AdvertBannerCostTO> getAdvertCostList(){
		return this.advertCostList;
	}


}
