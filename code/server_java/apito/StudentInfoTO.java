package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【学生信息详情】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class StudentInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//学生Id
	private Integer studentId;
	//学生名字
	private String name;
	//年龄
	private Integer age;
	//学生名称
	private String schoolName;
	//年级
	private String grade;
	//是否已评价    0、未评价  1、已评价
	private Integer isComment;
	//电话
	private String phone;


    /**设置学生Id*/
	public void setStudentId(Integer studentId){
		this.studentId=studentId;
	}
	/**获取学生Id*/
	public Integer getStudentId(){
		return this.studentId;
	}
    /**设置学生名字*/
	public void setName(String name){
		this.name=name;
	}
	/**获取学生名字*/
	public String getName(){
		return this.name;
	}
    /**设置年龄*/
	public void setAge(Integer age){
		this.age=age;
	}
	/**获取年龄*/
	public Integer getAge(){
		return this.age;
	}
    /**设置学生名称*/
	public void setSchoolName(String schoolName){
		this.schoolName=schoolName;
	}
	/**获取学生名称*/
	public String getSchoolName(){
		return this.schoolName;
	}
    /**设置年级*/
	public void setGrade(String grade){
		this.grade=grade;
	}
	/**获取年级*/
	public String getGrade(){
		return this.grade;
	}
    /**设置是否已评价    0、未评价  1、已评价*/
	public void setIsComment(Integer isComment){
		this.isComment=isComment;
	}
	/**获取是否已评价    0、未评价  1、已评价*/
	public Integer getIsComment(){
		return this.isComment;
	}
    /**设置电话*/
	public void setPhone(String phone){
		this.phone=phone;
	}
	/**获取电话*/
	public String getPhone(){
		return this.phone;
	}


}
