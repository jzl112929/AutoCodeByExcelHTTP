package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【地区】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class RegionTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//地区Id
	private Integer regionId;
	//地区名称
	private String regionName;
	//父节点Id
	private Integer parentId;
	//英文名首字母
	private String initial;


    /**设置地区Id*/
	public void setRegionId(Integer regionId){
		this.regionId=regionId;
	}
	/**获取地区Id*/
	public Integer getRegionId(){
		return this.regionId;
	}
    /**设置地区名称*/
	public void setRegionName(String regionName){
		this.regionName=regionName;
	}
	/**获取地区名称*/
	public String getRegionName(){
		return this.regionName;
	}
    /**设置父节点Id*/
	public void setParentId(Integer parentId){
		this.parentId=parentId;
	}
	/**获取父节点Id*/
	public Integer getParentId(){
		return this.parentId;
	}
    /**设置英文名首字母*/
	public void setInitial(String initial){
		this.initial=initial;
	}
	/**获取英文名首字母*/
	public String getInitial(){
		return this.initial;
	}


}
