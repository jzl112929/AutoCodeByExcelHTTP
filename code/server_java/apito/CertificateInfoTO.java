package com.jzl.to.api;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【资质信息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CertificateInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//资质名称
	private String certificateName;
	//资质类型
	private Integer type;
	//分组资质
	private List<CertificateBaseTO> certificateGroup = new ArrayList<CertificateBaseTO>();


    /**设置资质名称*/
	public void setCertificateName(String certificateName){
		this.certificateName=certificateName;
	}
	/**获取资质名称*/
	public String getCertificateName(){
		return this.certificateName;
	}
    /**设置资质类型*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取资质类型*/
	public Integer getType(){
		return this.type;
	}
    /**设置分组资质*/
	public void setCertificateGroup(List<CertificateBaseTO> certificateGroup){
		this.certificateGroup=certificateGroup;
	}
	/**获取分组资质*/
	public List<CertificateBaseTO> getCertificateGroup(){
		return this.certificateGroup;
	}


}
