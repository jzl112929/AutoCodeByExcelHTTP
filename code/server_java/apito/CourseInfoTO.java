package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【课程信息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CourseInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//课程ID
	private Integer courseId;
	//课程名称
	private String name;
	//老师图像地址
	private String avart;
	//老师昵称
	private String nickname;
	//课程价格    若果停售显示停售
	private Integer price;
	//课程开始时间
	private String startTime;
	//课程结束时间
	private String endTime;
	//购买人数
	private Integer purchaseNum;
	//老师Id
	private Integer teacherUserId;
	//上架状态    0下架  1上架
	private Integer status;


    /**设置课程ID*/
	public void setCourseId(Integer courseId){
		this.courseId=courseId;
	}
	/**获取课程ID*/
	public Integer getCourseId(){
		return this.courseId;
	}
    /**设置课程名称*/
	public void setName(String name){
		this.name=name;
	}
	/**获取课程名称*/
	public String getName(){
		return this.name;
	}
    /**设置老师图像地址*/
	public void setAvart(String avart){
		this.avart=avart;
	}
	/**获取老师图像地址*/
	public String getAvart(){
		return this.avart;
	}
    /**设置老师昵称*/
	public void setNickname(String nickname){
		this.nickname=nickname;
	}
	/**获取老师昵称*/
	public String getNickname(){
		return this.nickname;
	}
    /**设置课程价格    若果停售显示停售*/
	public void setPrice(Integer price){
		this.price=price;
	}
	/**获取课程价格    若果停售显示停售*/
	public Integer getPrice(){
		return this.price;
	}
    /**设置课程开始时间*/
	public void setStartTime(String startTime){
		this.startTime=startTime;
	}
	/**获取课程开始时间*/
	public String getStartTime(){
		return this.startTime;
	}
    /**设置课程结束时间*/
	public void setEndTime(String endTime){
		this.endTime=endTime;
	}
	/**获取课程结束时间*/
	public String getEndTime(){
		return this.endTime;
	}
    /**设置购买人数*/
	public void setPurchaseNum(Integer purchaseNum){
		this.purchaseNum=purchaseNum;
	}
	/**获取购买人数*/
	public Integer getPurchaseNum(){
		return this.purchaseNum;
	}
    /**设置老师Id*/
	public void setTeacherUserId(Integer teacherUserId){
		this.teacherUserId=teacherUserId;
	}
	/**获取老师Id*/
	public Integer getTeacherUserId(){
		return this.teacherUserId;
	}
    /**设置上架状态    0下架  1上架*/
	public void setStatus(Integer status){
		this.status=status;
	}
	/**获取上架状态    0下架  1上架*/
	public Integer getStatus(){
		return this.status;
	}


}
