package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【提现学习豆信息】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CoinInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//可提现额度
	private Long coinCanCashed;
	//今日收入
	private Long coinIncomeToday;
	//总收入
	private Long sumCoinIncome;


    /**设置可提现额度*/
	public void setCoinCanCashed(Long coinCanCashed){
		this.coinCanCashed=coinCanCashed;
	}
	/**获取可提现额度*/
	public Long getCoinCanCashed(){
		return this.coinCanCashed;
	}
    /**设置今日收入*/
	public void setCoinIncomeToday(Long coinIncomeToday){
		this.coinIncomeToday=coinIncomeToday;
	}
	/**获取今日收入*/
	public Long getCoinIncomeToday(){
		return this.coinIncomeToday;
	}
    /**设置总收入*/
	public void setSumCoinIncome(Long sumCoinIncome){
		this.sumCoinIncome=sumCoinIncome;
	}
	/**获取总收入*/
	public Long getSumCoinIncome(){
		return this.sumCoinIncome;
	}


}
