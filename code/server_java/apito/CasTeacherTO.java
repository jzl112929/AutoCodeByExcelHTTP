package com.jzl.to.api;

import java.util.ArrayList;
import java.util.List;


import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【老师用户】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CasTeacherTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//昵称
	private String nickName;
	//图像地址
	private String avatar;
	//性别
	private Integer sex;
	//手机号码
	private String phone;
	//教龄
	private String beginSignTime;
	//擅长学科
    private List<Integer> subjects = new ArrayList<Integer>();
	//试听课Id
	private Integer previewCourseId;
	//简介
	private Integer description;
	//试听课缩略图
	private String preCoursePic;


    /**设置昵称*/
	public void setNickName(String nickName){
		this.nickName=nickName;
	}
	/**获取昵称*/
	public String getNickName(){
		return this.nickName;
	}
    /**设置图像地址*/
	public void setAvatar(String avatar){
		this.avatar=avatar;
	}
	/**获取图像地址*/
	public String getAvatar(){
		return this.avatar;
	}
    /**设置性别*/
	public void setSex(Integer sex){
		this.sex=sex;
	}
	/**获取性别*/
	public Integer getSex(){
		return this.sex;
	}
    /**设置手机号码*/
	public void setPhone(String phone){
		this.phone=phone;
	}
	/**获取手机号码*/
	public String getPhone(){
		return this.phone;
	}
    /**设置教龄*/
	public void setBeginSignTime(String beginSignTime){
		this.beginSignTime=beginSignTime;
	}
	/**获取教龄*/
	public String getBeginSignTime(){
		return this.beginSignTime;
	}
    /**设置擅长学科*/
	public void setSubjects(List<Integer> subjects){
		this.subjects=subjects;
	}
	/**获取擅长学科*/
	public List<Integer> getSubjects(){
		return this.subjects;
	}
    /**设置试听课Id*/
	public void setPreviewCourseId(Integer previewCourseId){
		this.previewCourseId=previewCourseId;
	}
	/**获取试听课Id*/
	public Integer getPreviewCourseId(){
		return this.previewCourseId;
	}
    /**设置简介*/
	public void setDescription(Integer description){
		this.description=description;
	}
	/**获取简介*/
	public Integer getDescription(){
		return this.description;
	}
    /**设置试听课缩略图*/
	public void setPreCoursePic(String preCoursePic){
		this.preCoursePic=preCoursePic;
	}
	/**获取试听课缩略图*/
	public String getPreCoursePic(){
		return this.preCoursePic;
	}


}
