package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【版本】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class VersionTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//版本号
	private Float version;
	//文件地址
	private String filePath;
	//二维码
	private String filePathTdc;
	//版本内容
	private String content;


    /**设置版本号*/
	public void setVersion(Float version){
		this.version=version;
	}
	/**获取版本号*/
	public Float getVersion(){
		return this.version;
	}
    /**设置文件地址*/
	public void setFilePath(String filePath){
		this.filePath=filePath;
	}
	/**获取文件地址*/
	public String getFilePath(){
		return this.filePath;
	}
    /**设置二维码*/
	public void setFilePathTdc(String filePathTdc){
		this.filePathTdc=filePathTdc;
	}
	/**获取二维码*/
	public String getFilePathTdc(){
		return this.filePathTdc;
	}
    /**设置版本内容*/
	public void setContent(String content){
		this.content=content;
	}
	/**获取版本内容*/
	public String getContent(){
		return this.content;
	}


}
