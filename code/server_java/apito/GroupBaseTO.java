package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【群组】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class GroupBaseTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//群组Id
	private Integer groupId;
	//群组名称
	private String groupName;
	//图像地址
	private String pic;
	//简介
	private String description;
	//创建时间
	private String ctime;
	//群组人数
	private Integer memberNumber;
	//用户IM群号
	private String imGroupId;


    /**设置群组Id*/
	public void setGroupId(Integer groupId){
		this.groupId=groupId;
	}
	/**获取群组Id*/
	public Integer getGroupId(){
		return this.groupId;
	}
    /**设置群组名称*/
	public void setGroupName(String groupName){
		this.groupName=groupName;
	}
	/**获取群组名称*/
	public String getGroupName(){
		return this.groupName;
	}
    /**设置图像地址*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取图像地址*/
	public String getPic(){
		return this.pic;
	}
    /**设置简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取简介*/
	public String getDescription(){
		return this.description;
	}
    /**设置创建时间*/
	public void setCtime(String ctime){
		this.ctime=ctime;
	}
	/**获取创建时间*/
	public String getCtime(){
		return this.ctime;
	}
    /**设置群组人数*/
	public void setMemberNumber(Integer memberNumber){
		this.memberNumber=memberNumber;
	}
	/**获取群组人数*/
	public Integer getMemberNumber(){
		return this.memberNumber;
	}
    /**设置用户IM群号*/
	public void setImGroupId(String imGroupId){
		this.imGroupId=imGroupId;
	}
	/**获取用户IM群号*/
	public String getImGroupId(){
		return this.imGroupId;
	}


}
