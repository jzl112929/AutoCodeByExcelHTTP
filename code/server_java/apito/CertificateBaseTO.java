package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【用户资质】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class CertificateBaseTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//资质Id
	private Integer certificateId;
	//资质类型
	private Integer type;
	//文件地址
	private String pic;


    /**设置资质Id*/
	public void setCertificateId(Integer certificateId){
		this.certificateId=certificateId;
	}
	/**获取资质Id*/
	public Integer getCertificateId(){
		return this.certificateId;
	}
    /**设置资质类型*/
	public void setType(Integer type){
		this.type=type;
	}
	/**获取资质类型*/
	public Integer getType(){
		return this.type;
	}
    /**设置文件地址*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取文件地址*/
	public String getPic(){
		return this.pic;
	}


}
