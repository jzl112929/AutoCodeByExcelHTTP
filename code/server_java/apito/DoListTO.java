package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【作业列表名单】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class DoListTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//学生作业Id
	private Integer studentWorkId;
	//姓名
	private String studentName;
	//头像
	private String avatar;
	//状态    1、未提交  2、提交待点评  3、已点评
	private Integer status;
	//完成时间
	private String time;


    /**设置学生作业Id*/
	public void setStudentWorkId(Integer studentWorkId){
		this.studentWorkId=studentWorkId;
	}
	/**获取学生作业Id*/
	public Integer getStudentWorkId(){
		return this.studentWorkId;
	}
    /**设置姓名*/
	public void setStudentName(String studentName){
		this.studentName=studentName;
	}
	/**获取姓名*/
	public String getStudentName(){
		return this.studentName;
	}
    /**设置头像*/
	public void setAvatar(String avatar){
		this.avatar=avatar;
	}
	/**获取头像*/
	public String getAvatar(){
		return this.avatar;
	}
    /**设置状态    1、未提交  2、提交待点评  3、已点评*/
	public void setStatus(Integer status){
		this.status=status;
	}
	/**获取状态    1、未提交  2、提交待点评  3、已点评*/
	public Integer getStatus(){
		return this.status;
	}
    /**设置完成时间*/
	public void setTime(String time){
		this.time=time;
	}
	/**获取完成时间*/
	public String getTime(){
		return this.time;
	}


}
