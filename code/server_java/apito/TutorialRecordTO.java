package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【辅导课开课记录】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class TutorialRecordTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//课程Id
	private Integer tutorialRecordId;
	//课程时间
	private String courseTime;
	//课程名称
	private String courseName;
	//图像地址
	private String pic;
	//上课学生姓名
	private String studentName;
	//课时类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟
	private Integer timeType;
	//课程简介
	private String description;


    /**设置课程Id*/
	public void setTutorialRecordId(Integer tutorialRecordId){
		this.tutorialRecordId=tutorialRecordId;
	}
	/**获取课程Id*/
	public Integer getTutorialRecordId(){
		return this.tutorialRecordId;
	}
    /**设置课程时间*/
	public void setCourseTime(String courseTime){
		this.courseTime=courseTime;
	}
	/**获取课程时间*/
	public String getCourseTime(){
		return this.courseTime;
	}
    /**设置课程名称*/
	public void setCourseName(String courseName){
		this.courseName=courseName;
	}
	/**获取课程名称*/
	public String getCourseName(){
		return this.courseName;
	}
    /**设置图像地址*/
	public void setPic(String pic){
		this.pic=pic;
	}
	/**获取图像地址*/
	public String getPic(){
		return this.pic;
	}
    /**设置上课学生姓名*/
	public void setStudentName(String studentName){
		this.studentName=studentName;
	}
	/**获取上课学生姓名*/
	public String getStudentName(){
		return this.studentName;
	}
    /**设置课时类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟*/
	public void setTimeType(Integer timeType){
		this.timeType=timeType;
	}
	/**获取课时类型    1、45分钟  2、60分钟  3、90分钟  4、120分钟*/
	public Integer getTimeType(){
		return this.timeType;
	}
    /**设置课程简介*/
	public void setDescription(String description){
		this.description=description;
	}
	/**获取课程简介*/
	public String getDescription(){
		return this.description;
	}


}
