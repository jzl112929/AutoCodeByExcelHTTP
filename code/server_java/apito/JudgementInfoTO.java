package com.jzl.to.api;



import com.appcore.model.AbstractObject;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
/**
 * 【评价记录】
 * @author AutoCode 1129290218@qq.com
 */
@JsonIgnoreProperties
public class JudgementInfoTO extends AbstractObject {

    private static final long serialVersionUID = 1L;

	//评价ID
	private Integer id;
	//评价人
	private String creator;
	//综合分数
	private Integer score;
	//评价时间
	private String time;
	//评价内容
	private String content;
	//被评人
	private Integer studentId;
	//评价总数
	private Integer judgeAmount;


    /**设置评价ID*/
	public void setId(Integer id){
		this.id=id;
	}
	/**获取评价ID*/
	public Integer getId(){
		return this.id;
	}
    /**设置评价人*/
	public void setCreator(String creator){
		this.creator=creator;
	}
	/**获取评价人*/
	public String getCreator(){
		return this.creator;
	}
    /**设置综合分数*/
	public void setScore(Integer score){
		this.score=score;
	}
	/**获取综合分数*/
	public Integer getScore(){
		return this.score;
	}
    /**设置评价时间*/
	public void setTime(String time){
		this.time=time;
	}
	/**获取评价时间*/
	public String getTime(){
		return this.time;
	}
    /**设置评价内容*/
	public void setContent(String content){
		this.content=content;
	}
	/**获取评价内容*/
	public String getContent(){
		return this.content;
	}
    /**设置被评人*/
	public void setStudentId(Integer studentId){
		this.studentId=studentId;
	}
	/**获取被评人*/
	public Integer getStudentId(){
		return this.studentId;
	}
    /**设置评价总数*/
	public void setJudgeAmount(Integer judgeAmount){
		this.judgeAmount=judgeAmount;
	}
	/**获取评价总数*/
	public Integer getJudgeAmount(){
		return this.judgeAmount;
	}


}
